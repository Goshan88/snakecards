using Leopotam.Ecs;
using UnityEngine;
using System.Collections.Generic;
using System;

[EcsInject]
public class FieldProcessing : IEcsInitSystem, IEcsRunSystem {
	
    float _nextFieldUpdate = Const.TimeBegin;
    static float size =1f;
    EcsWorld _world = null;
    EcsFilter<Field> _fieldFilter = null;
    EcsFilter<Card> _cardFilter = null;
    Card viewed_card = new Card();
    private Field close_viewed_card = new Field();

    void IEcsInitSystem.Initialize () {
        
    }

    void IEcsInitSystem.Destroy () {
        for (var i = 0; i < _fieldFilter.EntitiesCount; i++)
        {
            _fieldFilter.Components1[i].Object = null;
            _world.RemoveEntity(_fieldFilter.Entities[i]);
        }
        for (var i = 0; i < _cardFilter.EntitiesCount; i++)
        {
            _cardFilter.Components1[i].Fields.Clear();
            _world.RemoveEntity(_cardFilter.Entities[i]);
        }
        Const.Game_Fields.Clear();
        Const.User_usedCard.Clear();
        Const.Opponent_usedCard.Clear();
    }

    void IEcsRunSystem.Run() {
       
        if (!Const.initGame)
        {
            Const.initGame = true;
            initField();
        }
            //Debug.Log("Update view: " + _nextUpdateTime.ToString() + "x" + Time.time.ToString());

            //view update
            if ((_nextFieldUpdate > Time.time)|| (!Const.initGame))
            {
                //Debug.Log("Update view: " + _nextUpdateTime.ToString() + "x" + Time.time.ToString());
                return;
            }
            else
            {
                _nextFieldUpdate = Time.time + Const.game_update;

            // Debug.Log("Update FieldProcessing next_update: " + _nextFieldUpdate.ToString() + "x" + Time.time.ToString());
            //get all fields and check update
            //update field game
            for (var i = 0; i < Const.Game_Fields.Count; i++)
            {
                //delete explosion
                if (Const.Game_Fields[i].State == Const.FIELD_STATE.EXPLOSION) Const.Game_Fields[i].newState = Const.FIELD_STATE.EMPTY;
            
                updateField(Const.Game_Fields[i]);
            }

            //update viewed card
            for (var i = 0; i < _cardFilter.EntitiesCount; i++)
            {
                var tmp =_cardFilter.Components1[i];
                if (tmp.type == Const.CARD_TYPE.VIEWED)
                {
                    for (var j = 0; j < tmp.Fields.Count; j++)
                    {
                        updateCardField(tmp.Fields[j]);
                    }
                        //Debug.Log("Numb: " + _cardFilter.Components1[i].type.ToString());
                        //showCard(_cardFilter.Components1[i],"view card", Const.VIEW_TYPE.STATE);
                }
            }
            //Debug.Log("Cnt: " + _cardFilter.EntitiesCount.ToString());

            //check close  viewed card
            if(Const.game_step_mode != (!close_viewed_card.Object.activeSelf))
            {
                if (close_viewed_card.Object.activeSelf)
                {
                    close_viewed_card.Object.SetActive(false);
                }
                else
                {
                    close_viewed_card.Object.SetActive(true);
                }
            }
        }
        
 
    }

    public void showCard(Card _card, string name, Const.VIEW_TYPE type)
    {
        int cnt = 0;
        string row = name + " 0 ";
        //change state in Card
        for (int j = 0; j < _card.Fields.Count; j++)
        {
            switch (type)
            {
                case Const.VIEW_TYPE.STATE:
                    row += _card.Fields[j].State.ToString() + " ";
                    break;
                case Const.VIEW_TYPE.NEW_STATE:
                    row += _card.Fields[j].newState.ToString() + " ";
                    break;
                case Const.VIEW_TYPE.COORD:
                    row += _card.Fields[j].Coords.X.ToString() + "x" + _card.Fields[j].Coords.Y.ToString() + " ";
                    break;
                default: break;
            }
            if (((++cnt) % Const.CardSize == 0))
            {
                Debug.Log(row);
                row = name + " " + (cnt / 6).ToString() + " ";
            }
        }
        Debug.Log("Count card field: " + _card.Fields.Count.ToString());
    }

    private void initField()
    {
        //need init cardcontrol
        Debug.Log("initField()");
        Const.initCardsControl = false;
        Const.screen_width = Screen.width;
        Const.screen_height = Screen.height;
        size = Const.screen_width / (Const.FIELD_SIZE + 1);
        float x_begin = -(Screen.width / 2) + (size / 2);
        float y_begin = (Screen.height / 2) - (size / 2);
        float x = 0f, y = 0f;
        Const.FIELD_STATE state = Const.FIELD_STATE.EMPTY;

        for (int i = 0; i <= Const.FIELD_SIZE; i++)
        {
            y = y_begin - i * size;
            for (int j = 0; j <= Const.FIELD_SIZE; j++)
            {
                x = x_begin + j * size;
                //choice image
                state = Const.FIELD_STATE.EMPTY;
                if ((j == 0) || (j == Const.FIELD_SIZE) || (i == 0) || (i == Const.FIELD_SIZE)) state = Const.FIELD_STATE.WALL;
                var tmp = field_Field(x, y, Const.Z_COORD, size, j, i, state);
                Const.Game_Fields.Add(tmp);
            }
        }

        state = Const.FIELD_STATE.VOID;
        y_begin -= (Const.CARD_OFFSET_Y + Const.FIELD_SIZE) * size;
        //x_begin += (Const.FIELD_SIZE-Const.CardSize+ Const.CARD_OFFSET_X) *size;

        //viewed card
        //Debug.Log("Cnt Field: " + _fieldFilter.EntitiesCount.ToString());
        viewed_card = _world.CreateEntityWith<Card>();
        int k = 0, m = 0;
        Field tmp_field = new Field();
        for (int i = 0; i < (Const.CardSize * Const.CardSize); i++)
        {
            y = y_begin - k * size;
            x = x_begin + m * size;

            tmp_field = field_Field(x, y, Const.Z_ZERO, size, k, m, state);

            viewed_card.Fields.Add(tmp_field);
            if (++m >= Const.CardSize)
            {
                m = 0;
                k++;
            }
        }
        viewed_card.type = Const.CARD_TYPE.VIEWED;
        viewed_card = null;
        //Debug.Log("Cnt Field+Card: " + _fieldFilter.EntitiesCount.ToString());

        //field for close viewed card
        int koef = Const.CardSize / 2;
        y = y_begin - koef * size;
        x = x_begin + koef * size;
        close_viewed_card = field_Field(x, y, Const.Z_COORD, (size * Const.CardSize), 0, 0, Const.FIELD_STATE.VOID);
        close_viewed_card.Object.SetActive(false);

        //used card
        koef = (Const.FIELD_SIZE / 2);
        x_begin = -(Screen.width / 2) + (size / 2)+size* (koef + Const.USER_USED_CARD_SHIFT_X);
        y_begin = (Screen.height / 2) - (size / 2) - size * (koef + Const.USER_USED_CARD_SHIFT_Y);
        int id_snake = PreferencyHelper.getActiveSnake();
        createUsedCardView(size, x_begin, y_begin, id_snake, Const.User_usedCard);//User

        y_begin -=  size *  Const.OPPONENT_USED_CARD_SHIFT_Y;
        id_snake = (id_snake == 0) ? 1 : 0;
        createUsedCardView(size, x_begin, y_begin, id_snake, Const.Opponent_usedCard);//Opponent
    }

    public void createUsedCardView(float size, float x_begin, float y_begin, int snake_id, List<Field> _List)
    {
        float x = 0, y = 0;
        int k = 0, m = 0;
        Field tmp_field;
        int used_card = PreferencyHelper.getUsedCard(snake_id);
        //Debug.Log("Used cards: " + used_card.ToString());
        Const.FIELD_STATE state = Const.FIELD_STATE.VOID;
        for (int i = 0; i < Const.PackSize; i++)
        {
            y = y_begin - k * size;
            x = x_begin + m * size;
            state = ((used_card-1) >= i) ? Const.FIELD_STATE.EMPTY : Const.FIELD_STATE.EMPTY_SYMBOL;
            tmp_field = field_Field(x, y, Const.Z_COORD_BUTTON, size, k, m, state);
            tmp_field.Object.SetActive(false);
            _List.Add(tmp_field);
            if (++m >= 3)//make square 3x3
            {
                m = 0;
                k++;
            }
        }
    }


    private void updateField(int _entitas)
    {
        if (_fieldFilter.Components1[_entitas] == null) return;
        if (_fieldFilter.Components1[_entitas].newState != _fieldFilter.Components1[_entitas].State)
        {
            //change sprite or rotate sprite
            _fieldFilter.Components1[_entitas].Object.GetComponent<SpriteRenderer>().sprite = getFieldSprite(_fieldFilter.Components1[_entitas].newState);
            _fieldFilter.Components1[_entitas].Object.transform.localScale = new Vector3(size, size, 0);
            _fieldFilter.Components1[_entitas].State = _fieldFilter.Components1[_entitas].newState;

            float cur_ang = _fieldFilter.Components1[_entitas].Object.transform.localEulerAngles.z; 
            cur_ang = _fieldFilter.Components1[_entitas].Rotate - cur_ang;
            _fieldFilter.Components1[_entitas].Object.transform.Rotate(0, 0, cur_ang);
            
        }
    }

    private void updateField(Field _field)
    {
        if (_field == null) return;
        if (_field.newState != _field.State)
        {
            //change sprite or rotate sprite
            _field.Object.GetComponent<SpriteRenderer>().sprite = getFieldSprite(_field.newState);
            _field.Object.transform.localScale = new Vector3(size, size, 0);
            _field.State = _field.newState;

            float cur_ang = _field.Object.transform.localEulerAngles.z;
            cur_ang = _field.Rotate - cur_ang;
            _field.Object.transform.Rotate(0, 0, cur_ang);

        }
    }


    private void updateCardField(int _entitas)
    {
        //check change sprite
        if (_fieldFilter.Components1[_entitas] == null) return;
        if (_fieldFilter.Components1[_entitas].newState != _fieldFilter.Components1[_entitas].State)
        {
            //change sprite or rotate sprite
            _fieldFilter.Components1[_entitas].Object.GetComponent<SpriteRenderer>().sprite = getSymbolSprite(_fieldFilter.Components1[_entitas].newState);
            _fieldFilter.Components1[_entitas].Object.transform.localScale = new Vector3(size, size, 0);
            _fieldFilter.Components1[_entitas].State = _fieldFilter.Components1[_entitas].newState;

            float cur_ang = _fieldFilter.Components1[_entitas].Object.transform.localEulerAngles.z;
            //Debug.Log("Rotate: " + cur_ang.ToString() + "x" + _fieldFilter.Components1[_entitas].Rotate.ToString());
            cur_ang = _fieldFilter.Components1[_entitas].Rotate - cur_ang;
            _fieldFilter.Components1[_entitas].Object.transform.Rotate(0, 0, cur_ang);

        }
    }

    private void updateCardField(Field _field)
    {
        //check change sprite
        if (_field == null) return;
        if (_field.Object == null) return;
        if ((_field.newState != _field.State)|| (_field.newRotate != _field.Rotate))
        {
            //change sprite or rotate sprite
            try
            {
                _field.Object.GetComponent<SpriteRenderer>().sprite = getSymbolSprite(_field.newState);
                _field.Object.transform.localScale = new Vector3(size, size, 0);
                _field.State = _field.newState;

                float cur_ang = _field.Object.transform.localEulerAngles.z;
                //Debug.Log("Rotate: " + cur_ang.ToString() + "x" + _fieldFilter.Components1[_entitas].Rotate.ToString());
                _field.Rotate = _field.newRotate;
                cur_ang = _field.Rotate - cur_ang;
                _field.Object.transform.Rotate(0, 0, cur_ang);
             }
            catch(NullReferenceException e)
            {
                Debug.Log("I can't find object for field:" + e.ToString());
            }
        }
    }


    Sprite getFieldSprite(Const.FIELD_STATE _state)
    {
        Sprite mySprite = Resources.Load("Coin_2", typeof(Sprite)) as Sprite;
        switch (_state)
        {
            case Const.FIELD_STATE.VOID:
            case Const.FIELD_STATE.EMPTY:
                mySprite = Resources.Load("empty", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.WALL:
                mySprite = Resources.Load("wall_32", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.HEAD:
                mySprite = Resources.Load("head_red_9", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.BODY:
                mySprite = Resources.Load("body_red_6", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.CURVE:
                mySprite = Resources.Load("curve_red_4", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.TAIL:
                mySprite = Resources.Load("tail_red_6", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_HEAD:
                mySprite = Resources.Load("head_blue_9", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_BODY:
                mySprite = Resources.Load("body_blue_6", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_CURVE:
                mySprite = Resources.Load("curve_blue_4", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_TAIL:
                mySprite = Resources.Load("tail_blue_6", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.EXPLOSION:
                mySprite = Resources.Load("exploation", typeof(Sprite)) as Sprite;
                break;
            default:
                break;
        }
        return mySprite;
    }


    Sprite getSymbolSprite(Const.FIELD_STATE _state)
    {
        Sprite mySprite = Resources.Load("Coin_2", typeof(Sprite)) as Sprite;
        switch (_state)
        {
            case Const.FIELD_STATE.NOTHING:
            case Const.FIELD_STATE.VOID:
                mySprite = Resources.Load("empty", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.EMPTY:
            case Const.FIELD_STATE.EMPTY_SYMBOL:
                mySprite = Resources.Load("empty_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.WALL:
                mySprite = Resources.Load("wall_32", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.HEAD:
                mySprite = Resources.Load("my_head_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.BODY:
                mySprite = Resources.Load("my_body_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.TAIL:
                mySprite = Resources.Load("my_tail_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_HEAD:
                mySprite = Resources.Load("enemy_head_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_BODY:
                mySprite = Resources.Load("enemy_body_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_TAIL:
                mySprite = Resources.Load("enemy_tail_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.EXPLOSION:
                mySprite = Resources.Load("exploation", typeof(Sprite)) as Sprite;
                break;
            default:
                break;
        }
        return mySprite;
    }


    private int Field(float x, float y, float size, int _x, int _y, Const.FIELD_STATE _state)
    {
        UnityEngine.Object pPrefab = Resources.Load("prf_Coin"); // note: not .prefab!
        switch (_state)
        {
            case Const.FIELD_STATE.VOID:
            case Const.FIELD_STATE.EMPTY:
                pPrefab = Resources.Load("prf_empty");
                break;
            case Const.FIELD_STATE.WALL:
                pPrefab = Resources.Load("prf_wall");
                break;
            case Const.FIELD_STATE.HEAD:
                pPrefab = Resources.Load("prf_head");
                break;
            case Const.FIELD_STATE.BODY:
                pPrefab = Resources.Load("prf_body");
                break;
            case Const.FIELD_STATE.CURVE:
                pPrefab = Resources.Load("prf_curve");
                break;
            case Const.FIELD_STATE.TAIL:
                pPrefab = Resources.Load("prf_tail");
                break;
            case Const.FIELD_STATE.ENEMY_HEAD:
                pPrefab = Resources.Load("prf_enemy_head");
                break;
            case Const.FIELD_STATE.ENEMY_BODY:
                pPrefab = Resources.Load("prf_enemy_body");
                break;
            case Const.FIELD_STATE.ENEMY_CURVE:
                pPrefab = Resources.Load("prf_enemy_curve");
                break;
            case Const.FIELD_STATE.ENEMY_TAIL:
                pPrefab = Resources.Load("prf_enemy_tail");
                break;
            case Const.FIELD_STATE.USED_CARD:
                pPrefab = Resources.Load("prf_used_card");
                break;
            case Const.FIELD_STATE.ANUSED_CARD:
                pPrefab = Resources.Load("prf_anused_card");
                break;
            case Const.FIELD_STATE.EMPTY_SYMBOL:
            pPrefab = Resources.Load("prf_empty_symbol");
                break;
            default:
                break;
        }

        GameObject goField = (GameObject)GameObject.Instantiate(pPrefab, new Vector3(x, y, Const.Z_COORD), Quaternion.identity);
        goField.transform.localScale = new Vector3(size, size, 0);
        Field field = new Field();
        int iField = _world.CreateEntityWith<Field>(out field);
        field.Object = goField;
        field.State = _state;
        field.newState = _state;
        field.Coords.X = _x;
        field.Coords.Y = _y;
        //Debug.Log("Coord: " + field.X.ToString() + "x" + field.Y.ToString());
        field.Rotate = 0f;
        return iField;
    }

    private Field field_Field(float x, float y, float z, float size, int _x, int _y, Const.FIELD_STATE _state)
    {
        UnityEngine.Object pPrefab = Resources.Load("prf_Coin"); // note: not .prefab!
        switch (_state)
        {
            case Const.FIELD_STATE.VOID:
            case Const.FIELD_STATE.EMPTY:
                pPrefab = Resources.Load("prf_empty");
                break;
            case Const.FIELD_STATE.WALL:
                pPrefab = Resources.Load("prf_wall");
                break;
            case Const.FIELD_STATE.HEAD:
                pPrefab = Resources.Load("prf_head");
                break;
            case Const.FIELD_STATE.BODY:
                pPrefab = Resources.Load("prf_body");
                break;
            case Const.FIELD_STATE.CURVE:
                pPrefab = Resources.Load("prf_curve");
                break;
            case Const.FIELD_STATE.TAIL:
                pPrefab = Resources.Load("prf_tail");
                break;
            case Const.FIELD_STATE.USED_CARD:
                pPrefab = Resources.Load("prf_used_card");
                break;
            case Const.FIELD_STATE.ANUSED_CARD:
                pPrefab = Resources.Load("prf_anused_card");
                break;
            case Const.FIELD_STATE.EMPTY_SYMBOL:
                pPrefab = Resources.Load("prf_empty_symbol");
                break;
            default:
                break;
        }

        GameObject goField = (GameObject)GameObject.Instantiate(pPrefab, new Vector3(x, y, z), Quaternion.identity);
        goField.transform.localScale = new Vector3(size, size, 0);
        Field field = new Field();
        int iField = _world.CreateEntityWith<Field>(out field);
        field.Object = goField;
        field.State = _state;
        field.newState = _state;
        field.Coords.X = _x;
        field.Coords.Y = _y;
        //Debug.Log("Coord: " + field.X.ToString() + "x" + field.Y.ToString());
        field.Rotate = 0f;
        return field;
    }
}