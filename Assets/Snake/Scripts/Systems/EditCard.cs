using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;


[EcsInject]
public class EditCard : IEcsInitSystem, IEcsRunSystem {
	float _nextEditCardUpdate = Const.TimeBegin;
    EcsFilter<Card> _cardFilter = null;
	EcsWorld _world = null;
    EcsFilter<Field> _fieldFilter = null;
    static float size = 0f;
    Card button = new Card();
    Card symbol = new Card();
    Card card = new Card();
    Field toggle;
    
    //Field[] teach_field= new Field[Const.size_field_ind];

    void IEcsInitSystem.Initialize () {
        //update time change snake
        PreferencyHelper.saveChangeDT(Const.PRF_DT_SNAKE);
        Const.sfs_state = Const.CONNECT_STATE.NOTHING;
    }

    void IEcsInitSystem.Destroy()
    {
        deleteAll();
    }

    
    void IEcsRunSystem.Run() {
      
        if (!Const.initEditCards)
        {
            Const.initEditCards = true;
            Const.EDIT_SAVE_CARD = false;
            Const.EDIT_CANCEL_CARD = false;
            Const.change_state = Const.FIELD_STATE.NOTHING;
            initCards();
        }
        if (_nextEditCardUpdate > Time.time)
        {
            return;
        }
        else
        {
            _nextEditCardUpdate = Time.time + Const.game_update;
            //Debug.Log("Update view: " + Time.time.ToString());
            //Debug.Log("Pack size: " + _packcardFilter.Components1[0].Pack.Count.ToString());
            //check touch
			if(!Const.touch_pos.processed){
                Const.touch_pos.processed = true;
                if (!PreferencyHelper.getUserTrening())//check trening condition
                {
                    switch (Const.StateTeach)
                    {
                       case Const.TeachState.ClickCard_7:
                            Const.StateTeach = Const.TeachState.ClickCard_8;
                            return;
                        case Const.TeachState.ClickCard_8:
                            toggle.Object.transform.localPosition = new Vector3(Const.CoordTailSymbol.x, Const.CoordTailSymbol.y, Const.Z_COORD);
                            break;
                        case Const.TeachState.ClickCard_9:
                            BorderOff();
                            break;
                        case Const.TeachState.ClickCard_10:
                            Const.StateTeach = Const.TeachState.ClickCard_11;
                            break;
                        case Const.TeachState.ClickCard_11:
                            Const.StateTeach = Const.TeachState.ClickCard_12;
                            return;
                        case Const.TeachState.ClickCard_14:
                            Const.StateTeach = Const.TeachState.ClickCard_15;
                            return;
                        case Const.TeachState.ClickCard_15:
                            Const.StateTeach = Const.TeachState.ClickCard_16;
                            toggle.Object.transform.localPosition = new Vector3(Const.CoordEmptySymbol.x, Const.CoordEmptySymbol.y, Const.Z_COORD);
                            return;
                        case Const.TeachState.ClickCard_16:
                            break;
                        case Const.TeachState.ClickCard_17:
                            BorderOff();
                            break;
                        case Const.TeachState.ClickCard_18:
                            break;
                        default:
                            return;
                    }                
            }
                click_symbol_btn();
                click_field();
            }
            if (!PreferencyHelper.getUserTrening())//check trening condition
            {
                showBorder();
            }
        }

        for (var j = 0; j < _fieldFilter.EntitiesCount; j++)
        {
            updateCardField(_fieldFilter.Entities[j]);
        }

        click_btn_ui();

        if (!PreferencyHelper.getUserTrening())//check trening condition
        {
            if (Const.StateTeach == Const.TeachState.ClickCard_17)
            {
                //check compleated click on 4 cell
                int res = 0;
                Field[] field = card.Fields.ToArray();
                for (int i = 0; i < field.Length; i++)
                {
                    if (field[i].State == Const.FIELD_STATE.BUTTON_SLC) res++;
                }
                if (res == 0) Const.StateTeach = Const.TeachState.ClickCard_18;
            }
        }
    }

    private void updateCardField(int _entitas)
    {
        //check change sprite
        //Debug.Log("Cnt Field+Card: " + _fieldFilter.EntitiesCount.ToString());0
        if (_fieldFilter.Components1[_entitas] == null)
        {
            //Debug.Log("return 0: " + _fieldFilter.Components1[_entitas].newState.ToString());
            return;
        }
        if (_fieldFilter.Components1[_entitas].newState != _fieldFilter.Components1[_entitas].State)
        {
            //Debug.Log("Update state: " + _fieldFilter.Components1[_entitas].newState.ToString());
            //change sprite or rotate sprite
            float _size = _fieldFilter.Components1[_entitas].Object.transform.localScale.x;
            _fieldFilter.Components1[_entitas].Object.GetComponent<SpriteRenderer>().sprite = getSymbolSprite(_fieldFilter.Components1[_entitas].newState);
            _fieldFilter.Components1[_entitas].Object.transform.localScale = new Vector3(_size, _size, 0);
            _fieldFilter.Components1[_entitas].State = _fieldFilter.Components1[_entitas].newState;

            float cur_ang = _fieldFilter.Components1[_entitas].Object.transform.localEulerAngles.z;
            //Debug.Log("Rotate: " + cur_ang.ToString() + "x" + _fieldFilter.Components1[_entitas].Rotate.ToString());
            cur_ang = _fieldFilter.Components1[_entitas].Rotate - cur_ang;
            _fieldFilter.Components1[_entitas].Object.transform.Rotate(0, 0, cur_ang);

        }
    }

    void initCards()
    {
        //create Cards
        Card tmp_card = new Card();
        Const.screen_width = Screen.width;
        Const.screen_height = Screen.height;
        size = Const.screen_width / (Const.EDIT_SIZE + 1);
        float _x = 0f, _y = 0f;
        float y_begin = (Screen.height / Const.PACK_OFF_Y) - (size / 2);
        float x_begin = -(Screen.width / Const.EDIT_OFF_X) + (size / 2);
        int cnt_x = 0, cnt_y = 0;
        //Card card = new Card();
        for (byte i = 0; i < Const.EditCardSize; i++)
            {
                card = _world.CreateEntityWith<Card>();
				int x=0, y=0;
                var x_begin_ = x_begin +(Const.CardSize * size+size) * cnt_x;
                var y_begin_ = y_begin - (Const.CardSize * size + size) * cnt_y;
                
            if (++cnt_x >= Const.RowCards)
            {
                cnt_x = 0;
                cnt_y++;
            }

            //Debug.Log("Begin: " + x_begin.ToString() + "x" + y_begin.ToString());
            for (int j = 0; j < (Const.CardSize * Const.CardSize + Const.OFF_CARD_EDIT); j++)
            {
                Field tmp_field;
                if (j < Const.OFF_CARD_EDIT)
                {
                    tmp_field = Field(0, 0, Const.Z_COORD, 0, 0, 0, Const.FIELD_STATE.VOID);
                }
                else
                {
                    _x = x_begin_ + x * size;
                    _y = y_begin_ - y * size;
                    tmp_field = Field(_x, _y, Const.Z_COORD, size, x, y, Const.FIELD_STATE.VOID);
                    card.Fields.Add(tmp_field);
                    tmp_card.Fields.Add(tmp_field);
                    if (++x >= Const.CardSize)
                    {
                        y++;
                        x = 0;
                    }
                    }
                }
			}

        loadCard();

        //add field choise
        _x = card.Fields[(card.Fields.Count-1)].Object.transform.localPosition.x + Const.EDIT_OFF_SYMB_X*size;
        var y_choice = card.Fields[(card.Fields.Count-1)].Object.transform.localPosition.x + Const.CardSize *size;
        y_choice = y_choice - Const.EDIT_OFF_SYMB_Y * size;
        
        button = _world.CreateEntityWith<Card>();
        symbol = _world.CreateEntityWith<Card>();
        var state_but = Const.FIELD_STATE.BUTTON_EMP;
        var state_smb = Const.FIELD_STATE.HEAD;
        for (int i = 0; i < Const.CNT_TYPE_SYMB; i++)
        {

            if (i < Const.OFF_TYPE_SYMB)//another don't work touch on 1 button
            {
                Field tmp_field = Field(0, 0, Const.Z_COORD, 0, 0, 0, state_but);
            }
            else
            {
                _y = y_choice - (Const.EDIT_BTW_SYMB_Y * (i- Const.OFF_TYPE_SYMB)) * size;
                Field tmp_field = Field(_x, _y, Const.Z_COORD, Const.edit_but_scale * size, 0, 0, state_but);
                button.Fields.Add(tmp_field);
                tmp_field = Field(_x, _y, Const.Z_SMB_COORD, size, 0, 0, state_smb++);
                symbol.Fields.Add(tmp_field);
            }
        }

        //hide Head symbol
        button.Fields[0].Object.SetActive(false);
        symbol.Fields[0].Object.SetActive(false);

        //get coord enemy tail
        Const.CoordTailSymbol = button.Fields[Const.numbEnemyTail].Object.transform.localPosition;
        //Const.CoordTailSymbol.x -= button.Fields[Const.numbEnemyTail].Object.transform.localScale.x;
        Const.CoordEmptySymbol = button.Fields[Const.numbEmpty].Object.transform.localPosition;
        //Const.CoordEmptySymbol.x -= button.Fields[Const.numbEmpty].Object.transform.localScale.x;

        Field[] tmp = card.Fields.ToArray();
        Const.CoordField_1 = tmp[((Const.teach_feild_ind[0]-1)-Const.CardSize)].Object.transform.localPosition;
        Const.CoordField_2 = tmp[((Const.teach_feild_ind[2] - 1) - Const.CardSize)].Object.transform.localPosition;

        //field for show card and other click place
        toggle = Field(Const.CoordTailSymbol.x, Const.CoordTailSymbol.y, Const.Z_COORD, (Const.edit_but_scale * size*Const.toggle_size_multi), (int)Const.CoordField_1.x, (int)Const.CoordField_1.y, Const.FIELD_STATE.BUTTON_SLC);
        toggle.Object.SetActive(false);
    }
       
    Sprite getSymbolSprite(Const.FIELD_STATE _state)
    {
        Sprite mySprite = Resources.Load("Coin_2", typeof(Sprite)) as Sprite;
        switch (_state)
        {
            case Const.FIELD_STATE.NOTHING:
            case Const.FIELD_STATE.DELETE_SMB:
            case Const.FIELD_STATE.VOID:
                mySprite = Resources.Load("empty", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.EMPTY_SYMBOL:
            case Const.FIELD_STATE.EMPTY:
                mySprite = Resources.Load("empty_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.WALL:
                mySprite = Resources.Load("wall_32", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.HEAD:
                mySprite = Resources.Load("my_head_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.BODY:
                mySprite = Resources.Load("my_body_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.TAIL:
                mySprite = Resources.Load("my_tail_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_HEAD:
                mySprite = Resources.Load("enemy_head_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_BODY:
                mySprite = Resources.Load("enemy_body_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_TAIL:
                mySprite = Resources.Load("enemy_tail_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.BUTTON_EMP:
                mySprite = Resources.Load("button_empty", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.BUTTON_SLC:
                mySprite = Resources.Load("button_selected", typeof(Sprite)) as Sprite;
                break;
            default:
                break;
        }
        return mySprite;
    }

    private Field Field(float x, float y, float z, float size, int _x, int _y, Const.FIELD_STATE _state)
    {
        UnityEngine.Object pPrefab = Resources.Load("prf_Coin"); // note: not .prefab!
        switch (_state)
        {
            case Const.FIELD_STATE.NOTHING:
            case Const.FIELD_STATE.DELETE_SMB:
            case Const.FIELD_STATE.VOID:
                pPrefab = Resources.Load("prf_empty");
                break;
            case Const.FIELD_STATE.EMPTY_SYMBOL:
            case Const.FIELD_STATE.EMPTY:
                pPrefab = Resources.Load("prf_empty_symbol");
                break;
            case Const.FIELD_STATE.WALL:
                pPrefab = Resources.Load("prf_wall");
                break;
            case Const.FIELD_STATE.HEAD:
                pPrefab = Resources.Load("prf_my_head_symbol");
                break;
            case Const.FIELD_STATE.BODY:
                pPrefab = Resources.Load("prf_my_body_symbol");
                break;
            case Const.FIELD_STATE.TAIL:
                pPrefab = Resources.Load("prf_my_tail_symbol");
                break;
            case Const.FIELD_STATE.ENEMY_HEAD:
                pPrefab = Resources.Load("prf_enemy_head_symbol");
                break;
            case Const.FIELD_STATE.ENEMY_BODY:
                pPrefab = Resources.Load("prf_enemy_body_symbol");
                break;
            case Const.FIELD_STATE.ENEMY_TAIL:
                pPrefab = Resources.Load("prf_enemy_tail_symbol");
                break;
            case Const.FIELD_STATE.BUTTON_EMP:
                pPrefab = Resources.Load("prf_btn_emp");
                break;
            case Const.FIELD_STATE.BUTTON_SLC:
                pPrefab = Resources.Load("prf_btn_slc");
                break;
            default:
                pPrefab = Resources.Load("prf_Coin"); // note: not .prefab!
                break;
        }

        GameObject goField = (GameObject)GameObject.Instantiate(pPrefab, new Vector3(x, y, z), Quaternion.identity);
        goField.transform.localScale = new Vector3(size, size, 0);
        Field field = new Field();
        int iField = _world.CreateEntityWith<Field>(out field);
        field.Object = goField;
        field.State = _state;
        field.newState = _state;
        field.Coords.X = _x;
        field.Coords.Y = _y;
        //Debug.Log("Coord: " + field.Coords.X.ToString() + "x" + field.Coords.Y.ToString());
        field.Rotate = 0f;
        return field;
    }

    //check click on button
    private void click_symbol_btn()
    {
        for (var i = 0; i < button.Fields.Count; i++)
        {
            //if button is not active tnan next
            if (!button.Fields[i].Object.active) continue;

            var tmp = button.Fields[i].Object.transform.localPosition.x;
            var x_min = tmp - ((Const.edit_but_scale * size) / 2);
            var x_max = tmp + ((Const.edit_but_scale * size) / 2);

            tmp = button.Fields[i].Object.transform.localPosition.y;
            var y_min = tmp - ((Const.edit_but_scale * size) / 2);
            var y_max = tmp + ((Const.edit_but_scale * size) / 2);

            //Debug.Log("X: " + x_min.ToString() + " " + x_max.ToString());
            //Debug.Log("Y: " + y_min.ToString() + " " + y_max.ToString());


            //Debug.Log("X get: " + x_max.ToString() +" " + Const.touch_pos.x.ToString());
            if ((x_min <= Const.touch_pos.x) && (x_max >= Const.touch_pos.x))
            {
                //Debug.Log("X get: ");
                if ((y_min <= Const.touch_pos.y) && (y_max >= Const.touch_pos.y))
                {
                    if (!PreferencyHelper.getUserTrening())
                    {
                        switch (Const.StateTeach)
                        {
                            case Const.TeachState.ClickCard_9:
                                return;
                            case Const.TeachState.ClickCard_8:
                                if (i != Const.numbEnemyTail)
                                {
                                    continue;
                                }
                                else
                                {
                                    Const.StateTeach = Const.TeachState.ClickCard_9;
                                     _fieldFilter.Components1[Const.teach_feild_ind[0]].newState = Const.FIELD_STATE.BUTTON_SLC;
                                }
                                break;
                            case Const.TeachState.ClickCard_16:
                                if (i != Const.numbEmpty)
                                {
                                    continue;
                                }
                                else
                                {
                                    Const.StateTeach = Const.TeachState.ClickCard_17;
                                    for (int k = 0; k < Const.teach_feild_ind.Length; k++) {
                                        _fieldFilter.Components1[Const.teach_feild_ind[k]].newState = Const.FIELD_STATE.BUTTON_SLC;
                                    }
                                }
                                break;
                            case Const.TeachState.ClickCard_17:
                                return;
                        }
                    }
                    //Debug.Log("Choice field is: " + i.ToString());
                    //Debug.Log("button.Fields.newState is: " + button.Fields[i].newState.ToString());
                    button.Fields[i].newState = Const.FIELD_STATE.BUTTON_SLC;
                    //Debug.Log("button.Fields.newState is: " + button.Fields[i].newState.ToString());
                        //search last selected button and deselect
                        if (button.Fields[i].State != Const.FIELD_STATE.BUTTON_SLC)
                    {//if make change button
                        if (symbol.Fields[i].State == Const.FIELD_STATE.DELETE_SMB)
                        {
                            Const.change_state = Const.FIELD_STATE.VOID;
                        }
                        else
                        {
                            if (symbol.Fields[i].State == Const.FIELD_STATE.EMPTY_SYMBOL)
                            {
                                Const.change_state = Const.FIELD_STATE.EMPTY;
                            }
                            else
                            {
                                Const.change_state = symbol.Fields[i].State;
                            }
                        }
                    }
                    else
                    {
                        Const.change_state = Const.FIELD_STATE.NOTHING;
                    }
                    Debug.Log("symbol.Fields.newState is: " + Const.change_state.ToString());
                    for (var j = 0; j < button.Fields.Count; j++)
                    {
                        if (button.Fields[j].State == Const.FIELD_STATE.BUTTON_SLC)
                        {
                            button.Fields[j].newState = Const.FIELD_STATE.BUTTON_EMP;
                            break;
                        }
                    }


                    break;
                }
            }
        }
    }

    private void click_field()
    {

        //search field that need change state
        for (var i = 0; i < card.Fields.Count; i++)
        {
            var tmp = card.Fields[i].Object.transform.localPosition.x;
            var x_min = tmp - ((Const.edit_but_scale * size) / 2);
            var x_max = tmp + ((Const.edit_but_scale * size) / 2);

            tmp = card.Fields[i].Object.transform.localPosition.y;
            var y_min = tmp - ((Const.edit_but_scale * size) / 2);
            var y_max = tmp + ((Const.edit_but_scale * size) / 2);

            //Debug.Log("X: " + x_min.ToString() + " " + x_max.ToString());
            //Debug.Log("Y: " + y_min.ToString() + " " + y_max.ToString());
            
            //Debug.Log("X get: " + x_max.ToString() +" " + Const.touch_pos.x.ToString());
                if ((x_min <= Const.touch_pos.x) && (x_max >= Const.touch_pos.x))
            {
                //Debug.Log("X get: ");
                if ((y_min <= Const.touch_pos.y) && (y_max >= Const.touch_pos.y))
                {
                    //���� ����� ��������
                    if (!PreferencyHelper.getUserTrening())
                    {
                        switch (Const.StateTeach)
                        {
                            case Const.TeachState.ClickCard_9:
                                if (card.Fields[i].State != Const.FIELD_STATE.BUTTON_SLC)
                                {
                                    return;
                                }
                                Const.StateTeach = Const.TeachState.ClickCard_10;
                                break;
                            case Const.TeachState.ClickCard_17:
                                if (card.Fields[i].State != Const.FIELD_STATE.BUTTON_SLC)
                                {
                                    return;
                                }
                                //Const.StateTeach = Const.TeachState.ClickCard_18;
                                break;
                            default:
                                return;
                        }
                    }

                        if (Const.change_state != Const.FIELD_STATE.NOTHING)
                    {
                        if (card.Fields[i].newState == Const.FIELD_STATE.HEAD)
                        {
                            Const.delete_head = true;
                            Debug.Log("You can't delete head in card!");
                        }
                        else
                        {
                            card.Fields[i].newState = Const.change_state;
                        }
                    }
                    Debug.Log("Selected field id: " + i.ToString());
                    break;
                }
            }
        }
    }

    //method for work with memory
    private void saveCard()
    {
        int[] tmp = new int[card.Fields.Count];
        for (var i = 0; i < card.Fields.Count; i++)
        {
            tmp[i] =(int) card.Fields[i].newState;
        }
        showCard(card, "save_card:", Const.VIEW_TYPE.NEW_STATE);
        PreferencyHelper.setCardValue(Const.snake_active, Const.card_active, tmp);
    }

    //method for work with memory
    private void loadCard()
    {
        int[] tmp = PreferencyHelper.getCardValue(Const.snake_active, Const.card_active);
        for (var i = 0; i < card.Fields.Count; i++)
        {
             card.Fields[i].newState =(Const.FIELD_STATE) tmp[i];
        }
        
    }
    
    private void loadCard(Card _card)
    {
        int[] tmp = PreferencyHelper.getCardValue(Const.snake_active, Const.card_active);
        for (var i = 0; i < _card.Fields.Count; i++)
        {
            _card.Fields[i].newState = (Const.FIELD_STATE)tmp[i];
        }
    }

    private void click_btn_ui()
    {
        if (Const.EDIT_SAVE_CARD) {
            if (!PreferencyHelper.getUserTrening())
            {
                if (Const.StateTeach == Const.TeachState.ClickCard_11) Const.StateTeach = Const.TeachState.ClickCard_12;
                if (Const.StateTeach == Const.TeachState.ClickCard_18) Const.StateTeach = Const.TeachState.ClickCard_19;
            }
            Const.EDIT_SAVE_CARD = false;
            Debug.Log("Save card load main scene: ");
            saveCard();
            deleteAll();
            Const.initShowCards = false;
            SceneManager.LoadScene("Main");
        }

        if (Const.EDIT_CANCEL_CARD)
        {
            Const.EDIT_CANCEL_CARD = false;
            Debug.Log("without save card load main scene: ");
            deleteAll();
            Const.initShowCards = false;
            SceneManager.LoadScene("Main");
        }

        if (Const.clear_edit_card)
        {
            Const.clear_edit_card = false;
            for (var i = 0; i < card.Fields.Count; i++)
            {
                card.Fields[i].newState = Const.FIELD_STATE.VOID;
            }
            card.Fields[(card.Fields.Count/2)].newState = Const.FIELD_STATE.HEAD;
        }
    }

    void deleteAll()
    {
        for (int i = 0; i < _fieldFilter.EntitiesCount; i++)
        {
            if (_fieldFilter.Components1[i].Object != null) _fieldFilter.Components1[i].Object = null;
            _world.RemoveEntity(_fieldFilter.Entities[i]);
        }
        for (var i = 0; i < _cardFilter.EntitiesCount; i++)
        {
            //_cardFilter.Components1[i].Fields.Clear();
            /*for (var j = 0; j < _cardFilter.Components1[i].Fields.Count; j++)
            {
                _cardFilter.Components1[i].Fields[j].Object = null;
            }*/
            _cardFilter.Components1[i].Fields.Clear();
            _world.RemoveEntity(_cardFilter.Entities[i]);
        }                
    }

    public void showCard(Card _card, string name, Const.VIEW_TYPE type)
    {
        int cnt = 0;
        string row = name + " 0 ";
        //change state in Card
        for (int j = 0; j < _card.Fields.Count; j++)
        {
            switch (type)
            {
                case Const.VIEW_TYPE.STATE:
                    row += _card.Fields[j].State.ToString() + " ";
                    break;
                case Const.VIEW_TYPE.NEW_STATE:
                    row += _card.Fields[j].newState.ToString() + " ";
                    break;
                case Const.VIEW_TYPE.COORD:
                    row += _card.Fields[j].Coords.X.ToString() + "x" + _card.Fields[j].Coords.Y.ToString() + " ";
                    break;
                default: break;
            }
            if (((++cnt) % Const.CardSize == 0))
            {
                //Debug.Log(row);
                row = name + " " + (cnt / 6).ToString() + " ";
            }
        }
        //Debug.Log("Count card field: " + _card.Fields.Count.ToString());
    }
        private void showBorder()
        {
            switch (Const.StateTeach)
            {
                case Const.TeachState.ClickCard_7:
                case Const.TeachState.ClickCard_10:
                case Const.TeachState.ClickCard_14:
                case Const.TeachState.ClickCard_15:
                    break;
                case Const.TeachState.ClickCard_8:
                if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                {
                    Const.CntUpdateBorder = 0;
                    toggleBorder();
                }
                break;
                case Const.TeachState.ClickCard_9:
                break;
                case Const.TeachState.ClickCard_18:
                case Const.TeachState.ClickCard_11:
                 
                    break;
                case Const.TeachState.ClickCard_16:
                if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                {
                    Const.CntUpdateBorder = 0;
                    toggleBorder();
                }
                break;
                case Const.TeachState.ClickCard_17:
                break;

                default:
                    break;
            }
        }

    private void toggleBorder()
    {
        if (toggle.Object.activeSelf)
        {
            toggle.Object.SetActive(false);
        }
        else
        {
            toggle.Object.SetActive(true);
        }
    }

    private void BorderOff()
    {
        toggle.Object.SetActive(false);
    }
}
