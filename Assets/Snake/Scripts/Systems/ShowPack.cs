using Leopotam.Ecs;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

[EcsInject]
public class showPack : IEcsInitSystem, IEcsRunSystem {
	float _nextCardsUpdate = Const.TimeBegin;
    EcsFilter<Card> _cardFilter = null;
	EcsFilter<PackCard> _packcardFilter = null;
    EcsWorld _world = null;
    EcsFilter<Field> _fieldFilter = null;
    static float size = 0f;
    PackCard pack = new PackCard();
    int newUsed_Card=10;
    Card close;
    static int old_snake_active = 0;
    Field toggle;

    void IEcsInitSystem.Initialize () {
        
    }

    void IEcsInitSystem.Destroy () {
        deleteAll();
    }

    void deleteAll()
    {
        for (int i = 0; i < _fieldFilter.EntitiesCount; i++)
        {
            if (_fieldFilter.Components1[i].Object != null) _fieldFilter.Components1[i].Object = null;
            _world.RemoveEntity(_fieldFilter.Entities[i]);
        }
        for (var i = 0; i < _cardFilter.EntitiesCount; i++)
        {
            //_cardFilter.Components1[i].Fields.Clear();
            for (var j = 0; j < _cardFilter.Components1[i].Fields.Count; j++)
            {
                _cardFilter.Components1[i].Fields[j].Object = null;
            }
            _cardFilter.Components1[i].Fields.Clear();
            _world.RemoveEntity(_cardFilter.Entities[i]);
        }
        for (var i = 0; i < _packcardFilter.EntitiesCount; i++)
        {
            var pack = _packcardFilter.Components1[i];
            if (pack != null)
            {
                pack.Pack.Clear();
                _world.RemoveEntity(_packcardFilter.Entities[i]);
            }
        }
    }

 
    void IEcsRunSystem.Run() {
      
        if (!Const.initShowCards)
        {
            Const.initShowCards = true;
            initCards();
        }

        if (_nextCardsUpdate > Time.time)
        {
            return;
        }
        else
        {
            _nextCardsUpdate = Time.time + Const.game_update;
            //Debug.Log("Update view: " + Time.time.ToString());
            //Debug.Log("Pack size: " + _packcardFilter.Components1[0].Pack.Count.ToString());
            for (var i = 0; i < _fieldFilter.EntitiesCount; i++)
            {
                updateCardField(_fieldFilter.Entities[i]);
            }

            if(Const.battle_type == Const.BATTLE_TYPE.DEBUG)
            {
                Const.initGame = false;
                Const.initShowCards = false;
                deleteAll();
                SceneManager.LoadScene("Game");
                //Const.battle_type = Const.BATTLE_TYPE.NOTHING;
            }
            else
            {
                if (Const.battle_type == Const.BATTLE_TYPE.BATTLE)
                {
                    Const.initShowCards = false;
                    Const.initShowCards = false;
                    deleteAll();
                    SceneManager.LoadScene("Game");
                    //Const.battle_type = Const.BATTLE_TYPE.NOTHING;
                }
            }

           
            if (Const.snake_used_card != newUsed_Card)
            {
                newUsed_Card = Const.snake_used_card;
                //Debug.Log("Used_Card: "+ Used_Card.ToString());
                for (int i=0 ; i < close.Fields.Count; i++)
                {
                    if(i >= newUsed_Card)
                    {
                        showField(close.Fields[i]);
                    }
                    else
                    {
                        hideField(close.Fields[i]);
                    }
                }
            }

            //if change active card in teach mode
            if (!PreferencyHelper.getUserTrening())
            {
                if (Const.needClickCard != Const.needClickCardOut)
                {
                    Const.needClickCardOut = Const.needClickCard;
                    Field[] tmp = close.Fields.ToArray();
                    tmp[(Const.needClickCardOut-1)].newState = Const.FIELD_STATE.VOID;
                    setCoordToggle(tmp[(Const.needClickCardOut - 1)].Object.transform.position);
                    toggleToggle();
                    /*Const.firstCard = tmp[(Const.needClickCardOut - 1)].Object.transform.position;
                    var size = tmp[(Const.needClickCardOut - 1)].Object.transform.localScale.y;
                    Const.firstCardTop = new Vector3(Const.firstCard.x, (Const.firstCard.y+ (size/2)), Const.firstCard.z);
                    newUsed_Card = 10;*/
                }
                if(Const.StateTeach == Const.TeachState.ClickCard_19)
                {
                    Field[] tmp = close.Fields.ToArray();
                    tmp[0].newState = Const.FIELD_STATE.VOID;
                    Const.firstCard = tmp[0].Object.transform.localPosition;
                    var size = tmp[0].Object.transform.localScale.y;
                    Const.firstCardTop = new Vector3(Const.firstCard.x, (Const.firstCard.y + (size / 2)), Const.firstCard.z);
                }
            }

            checkToggle();

            //check touch
            if (!Const.touch_pos.processed){
                Const.touch_pos.processed = true;

                if (!PreferencyHelper.getUserTrening()){
                    switch (Const.StateTeach) {
                        case Const.TeachState.BEGIN:
                            Const.StateTeach = Const.TeachState.ClickCard_1;
                            Const.needClickCard = 1;
                        return;
                        case Const.TeachState.ClickCard_6:
                        case Const.TeachState.ClickCard_1:
                            break;
                        case Const.TeachState.ClickCard_2:
                            Const.StateTeach = Const.TeachState.ClickCard_3;
                            return;
                        case Const.TeachState.ClickCard_3:
                            Const.StateTeach = Const.TeachState.ClickCard_4;
                            return;
                        case Const.TeachState.ClickCard_4:
                            Const.StateTeach = Const.TeachState.ClickCard_5;
                            return;
                        case Const.TeachState.ClickCard_5:
                            Const.StateTeach = Const.TeachState.ClickCard_6;
                            return;
                        case Const.TeachState.ClickCard_12:
                            //Const.StateTeach = Const.TeachState.ClickCard_13;
                            break;
                        case Const.TeachState.ClickCard_13:
                            //Const.StateTeach = Const.TeachState.ClickCard_14;
                            break;
                        case Const.TeachState.ClickCard_19:
                            //Const.StateTeach = Const.TeachState.ClickCard_20;
                            return;
                        case Const.TeachState.ClickCard_20:
                            //Const.StateTeach = Const.TeachState.ClickCard_21;
                            return;
                        default:
                            return;
                    }
                }

				for (var i = 0; i < pack.Pack.Count; i++)
                {
                    float x_min = 0f, x_max = 0f, y_max = 0f, y_min = 0f;
                    try {
                        Card _card = pack.Pack[i];
                        x_min = _card.Fields[0].Object.transform.localPosition.x;
                        x_max = _card.Fields[(_card.Fields.Count - 1)].Object.transform.localPosition.x;
                        y_min = _card.Fields[(_card.Fields.Count - 1)].Object.transform.localPosition.y;
                        y_max = _card.Fields[0].Object.transform.localPosition.y;
                    }catch(NullReferenceException e)
                    {
                        //Debug.Log("Error object not fond: " + i.ToString());
                        Debug.Log("Error log: " + e.ToString());
                    }
                     if ((x_min <=Const.touch_pos.x)&&(x_max >= Const.touch_pos.x)){
                        if ((y_min <= Const.touch_pos.y)&&(y_max >= Const.touch_pos.y)){
                            Debug.Log("Choice card is: " + i.ToString());

                            if (!PreferencyHelper.getUserTrening())
                            {
                                switch (Const.StateTeach)
                                {
                                    case Const.TeachState.ClickCard_6:
                                    case Const.TeachState.ClickCard_1:
                                        if(i == 0)//if choise 1st card
                                        {
                                           if(Const.StateTeach== Const.TeachState.ClickCard_1) Const.StateTeach = Const.TeachState.ClickCard_2;
                                           if (Const.StateTeach == Const.TeachState.ClickCard_6) Const.StateTeach = Const.TeachState.ClickCard_7;
                                        }
                                        else
                                        {
                                            return;
                                        }
                                        break;
                                    case Const.TeachState.ClickCard_13:
                                    case Const.TeachState.ClickCard_12:
                                        if (i == 1)//if choise 2nd card
                                        {
                                            if (Const.StateTeach == Const.TeachState.ClickCard_13) Const.StateTeach = Const.TeachState.ClickCard_14;
                                            if (Const.StateTeach == Const.TeachState.ClickCard_12) Const.StateTeach = Const.TeachState.ClickCard_13;
                                        }
                                        else
                                        {
                                            return;
                                        }
                                        break;
                                    default:
                                        return;
                                }
                            }

                            if ((i >= Const.snake_used_card) &&(!Const.long_click))
                            {
                                if (++Const.snake_used_card > Const.PackSize) Const.snake_used_card = Const.PackSize;
                                PreferencyHelper.setUsedCard(Const.USER_ID, Const.snake_used_card);
                                break; //if click on close card
                            }

                            if ((i <= Const.snake_used_card) && (Const.long_click))
                            {
                                if (--Const.snake_used_card < Const.MinUsedCard) Const.snake_used_card = Const.MinUsedCard;
                                PreferencyHelper.setUsedCard(Const.USER_ID, Const.snake_used_card);
                                break; //if click on close card
                            }

                             deleteAll();
                             Const.card_active = i;
                             Const.initEditCards = false;
                             //PreferencyHelper.saveChangeDT(Const.PRF_DT_SNAKE);
                             if(Const.panel == Const.Type_panel.NOTHING) SceneManager.LoadScene("EditCard");
                          }
                      }
                 }
            }
        }

      
    }

    private void checkToggle()
    {
        if (!PreferencyHelper.getUserTrening())
        {
            switch (Const.StateTeach)
            {
                case Const.TeachState.BEGIN:
                    return;
                case Const.TeachState.ClickCard_6:
                case Const.TeachState.ClickCard_1:
                    if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                    {
                        Const.CntUpdateBorder = 0;
                        toggleToggle();
                    }
                    return;
                case Const.TeachState.ClickCard_2:
                    offToggle();
                    return;
                case Const.TeachState.ClickCard_3:
                    return;
                case Const.TeachState.ClickCard_4:
                    return;
                case Const.TeachState.ClickCard_5:
                    return;
                case Const.TeachState.ClickCard_12:
                    if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                    {
                        Const.CntUpdateBorder = 0;
                        toggleToggle();
                    }
                    break;
                case Const.TeachState.ClickCard_13:
                    if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                    {
                        Const.CntUpdateBorder = 0;
                        toggleToggle();
                    }
                    break;
                case Const.TeachState.ClickCard_19:
                    return;
                default:
                    return;
            }
        }
    }

    private void updateCardField(int _entitas)
    {
        //check change sprite
        //Debug.Log("Cnt Field+Card: " + _fieldFilter.EntitiesCount.ToString());0
        if (_fieldFilter.Components1[_entitas] == null)
        {
            //Debug.Log("Update state: " + _fieldFilter.Components1[_entitas].newState.ToString());
            return;
        }
        if (_fieldFilter.Components1[_entitas].newState != _fieldFilter.Components1[_entitas].State)
        {
            //get size
            float tmp_size = _fieldFilter.Components1[_entitas].Object.transform.localScale.y;

            //Debug.Log("Update state: " + _fieldFilter.Components1[_entitas].newState.ToString());
            //change sprite or rotate sprite
            _fieldFilter.Components1[_entitas].Object.GetComponent<SpriteRenderer>().sprite = getSymbolSprite(_fieldFilter.Components1[_entitas].newState);
            _fieldFilter.Components1[_entitas].Object.transform.localScale = new Vector3(tmp_size, tmp_size, 0);
            _fieldFilter.Components1[_entitas].State = _fieldFilter.Components1[_entitas].newState;

            float cur_ang = _fieldFilter.Components1[_entitas].Object.transform.localEulerAngles.z;
            //Debug.Log("Rotate: " + cur_ang.ToString() + "x" + _fieldFilter.Components1[_entitas].Rotate.ToString());
            cur_ang = _fieldFilter.Components1[_entitas].Rotate - cur_ang;
            _fieldFilter.Components1[_entitas].Object.transform.Rotate(0, 0, cur_ang);

        }
    }

    private void initCards()
    {
       //Debug.Log("Init showPack: ");
        Const.screen_width = Screen.width;
        Const.screen_height = Screen.height;
        size = Const.screen_width / (Const.PACK_SIZE + 1);
        float _x = 0f, _y = 0f;
        float x_begin = -(Const.PACK_OFF_X * Const.CardSize*size);
        float y_begin = (Screen.height / Const.PACK_OFF_Y) - (size / 2);
        pack = _world.CreateEntityWith<PackCard>();
        int cnt_x = 0, cnt_y = 0;
        Field[] tmp_deb = new Field[(Const.PackSize + 1)];//i have crash field in all card
        tmp_deb[0] = Field(0, 0, Const.Z_COORD, 0, 0, 0, Const.FIELD_STATE.VOID);//sad
        for (byte i = 0; i < Const.PackSize; i++)
            {
                //Debug.Log("Const.PackSize: " + i.ToString());
            Card card = _world.CreateEntityWith<Card>();
            Field(0, 0, Const.Z_COORD, 0, 0, 0, Const.FIELD_STATE.VOID);//sad
            int x=0, y=0;
            var x_begin_ = x_begin +(Const.CardSize * size+size) * cnt_x;
            var y_begin_ = y_begin - (Const.CardSize * size + size) * cnt_y;
       
            if (++cnt_x >= Const.RowCards)
            {
                cnt_x = 0;
                cnt_y++;
            }

            //Debug.Log("Begin: " + x_begin.ToString() + "x" + y_begin.ToString());
            if (card.Fields.Count == 0)//check if card have
            {
                for (int j = 0; j < (Const.CardSize * Const.CardSize); j++)
                {

                    _x = x_begin_ + x * size;
                    _y = y_begin_ - y * size;
                    if (j == i) tmp_deb[(i + 1)] = Field(0, 0,0, 0, 0, 0, Const.FIELD_STATE.VOID);//sad
                    Field tmp_field = Field(_x, _y, Const.Z_COORD, size, x, y, Const.FIELD_STATE.VOID);
                    card.Fields.Add(tmp_field);
                    if (++x >= Const.CardSize)
                    {
                        y++;
                        x = 0;
                    }
                }
            }
            else
            {

                Debug.Log("Warning card is full: ");
            }
				pack.Pack.Add(card);
			}
			
        //field for close anused card
        close = _world.CreateEntityWith<Card>();
        for (int j = 0; j < pack.Pack.Count; j++)
        {
            float[] begin = new float[2];
            float[] end = new float[2];
            //search begin and end of card
            for (int i = 0; i < pack.Pack[0].Fields.Count; i++)//search first element
            {
                if (pack.Pack[j].Fields[i].Object.transform.localPosition.x != 0)
                {
                    begin[0] = pack.Pack[j].Fields[i].Object.transform.localPosition.x;
                    begin[1] = pack.Pack[j].Fields[i].Object.transform.localPosition.y;
                    break;
                }
            }
            end[0] = pack.Pack[j].Fields[(pack.Pack[j].Fields.Count - 1)].Object.transform.localPosition.x;
            end[1] = pack.Pack[j].Fields[(pack.Pack[j].Fields.Count - 1)].Object.transform.localPosition.y;


            float close_size = end[0] - begin[0] + size;
            //Debug.Log("Close field begin: " + begin[0].ToString() + "x" + begin[1].ToString() + " end: " + end[0].ToString() + "x" + end[1].ToString() + " size: " + close_size.ToString());
            int close_x = 0;
            int close_y = 0;
            Field close_field;
            
            //if in trening mode than
            if (!PreferencyHelper.getUserTrening())
            {
                close_field = Field(((end[0] + begin[0]) / 2), ((end[1] + begin[1]) / 2), Const.Z_HIDE_CLOSE, close_size, close_x, close_y, Const.FIELD_STATE.CLOSE_CARD_NOTINTERACT);
            }
            else
            {
                close_field = Field(((end[0] + begin[0]) / 2), ((end[1] + begin[1]) / 2), Const.Z_HIDE_CLOSE, close_size, close_x, close_y, Const.FIELD_STATE.VOID);
            }
            hideField(close_field);
            close.Fields.Add(close_field); 
           }

        //create field for toggle
        var toogle_x = (float)close.Fields[(close.Fields.Count-1)].Coords.X;
        var toogle_y =  (float)close.Fields[(close.Fields.Count - 1)].Coords.Y;
        var toogle_size = close.Fields[(close.Fields.Count - 1)].Object.transform.localScale.y;
        var first_size = toogle_size;
        toogle_size = toogle_size * Const.toggle_size_multi;
        toogle_x += first_size*(Const.toggle_offset-1);
        toogle_y -= first_size * (Const.toggle_offset - 1);

        toggle = Field(toogle_x, toogle_y, Const.FIELD_BORDER, toogle_size, (int)toogle_x, (int)toogle_y, Const.FIELD_STATE.BUTTON_SLC);
        toggle.Object.SetActive(false);

        //load usedcard
        Const.snake_used_card = PreferencyHelper.getUsedCard(Const.USER_ID);
       
        loadPack();

        //if second enter in main scene
        if (!PreferencyHelper.getUserTrening())
        {
            if (Const.StateTeach == Const.TeachState.ClickCard_12) Const.needClickCard = 2;
        }
    }

    private void setCoordToggle(Vector3 coord)
    {
        var size = close.Fields[0].Object.transform.localScale.y;
        var y = coord.y /*+ size * (Const.toggle_offset - 1)*/;
        var x = coord.x /*+ size * (Const.toggle_offset - 1)*/;
        var z = toggle.Object.transform.localPosition.z;
        toggle.Object.transform.localPosition = new Vector3(x,y,z);
    }

    private void toggleToggle()
    {
        if (toggle.Object.activeSelf) {
            toggle.Object.SetActive(false);
        }
        else
        {
            toggle.Object.SetActive(true);
        }
    }

    private void offToggle()
    {
        if (toggle.Object.activeSelf)
        {
            toggle.Object.SetActive(false);
        }
    }

            private void loadPack()
    {

        //check head in card 
        checkCards();

        int[] tmp = PreferencyHelper.getPackValue(Const.USER_ID);
        int state_count = 0;
        state_count = 0;
        //Debug.Log("count field" + state_count.ToString());
        for (var i = 0; i < pack.Pack.Count; i++)
        {
            Card card_tmp = pack.Pack[i];
            //Debug.Log("card_tmp size" + card_tmp.Fields.Count.ToString());
            for (var j = 0; j < card_tmp.Fields.Count; j++)
            {
                try
                {
                    card_tmp.Fields[j].newState = (Const.FIELD_STATE)tmp[state_count++];
                }catch(IndexOutOfRangeException e)
                {
                    //Debug.Log("NullReferenceException" + e.ToString());
                    //Debug.Log("card_tmp.Fields: " + j.ToString() + " tmp: " + state_count.ToString());
                }
            }
                //_card.Fields[i].newState = (Const.FIELD_STATE)tmp[i];
        }
        //Debug.Log("count field" + state_count.ToString());
        //Debug.Log("pack.Pack.Count" + pack.Pack.Count.ToString());
    }

    private void checkCards()
    {
        int[] tmp = PreferencyHelper.getCardValue(Const.USER_ID,0);
   
            for (var i = 0; i < pack.Pack.Count; i++)
        {
            tmp = PreferencyHelper.getCardValue(Const.USER_ID, i);
            if(tmp[((Const.CardSize* Const.CardSize) / 2)] != (int)Const.FIELD_STATE.HEAD)
            {
                Debug.Log("No head in card: " + i.ToString());
                tmp[((Const.CardSize * Const.CardSize) / 2)] = (int)Const.FIELD_STATE.HEAD;
                PreferencyHelper.setCardValue(Const.USER_ID, i, tmp);
            }
        }
   
    }


    Sprite getSymbolSprite(Const.FIELD_STATE _state)
    {
        Sprite mySprite = Resources.Load("Coin_2", typeof(Sprite)) as Sprite;
        switch (_state)
        {
            case Const.FIELD_STATE.NOTHING:
            case Const.FIELD_STATE.VOID:
                mySprite = Resources.Load("empty", typeof(Sprite)) as Sprite;
                //mySprite = Resources.Load("empty_without_active", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.EMPTY:
            case Const.FIELD_STATE.EMPTY_SYMBOL:
                mySprite = Resources.Load("empty_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.WALL:
                mySprite = Resources.Load("wall_32", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.HEAD:
                mySprite = Resources.Load("my_head_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.BODY:
                mySprite = Resources.Load("my_body_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.TAIL:
                mySprite = Resources.Load("my_tail_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_HEAD:
                mySprite = Resources.Load("enemy_head_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_BODY:
                mySprite = Resources.Load("enemy_body_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.ENEMY_TAIL:
                mySprite = Resources.Load("enemy_tail_symbol", typeof(Sprite)) as Sprite;
                break;
            case Const.FIELD_STATE.BUTTON_SLC:
                mySprite = Resources.Load("button_selected", typeof(Sprite)) as Sprite;
                break;
            default:
                break;
        }
        return mySprite;
    }

    private Field Field(float x, float y, float z, float size, int _x, int _y, Const.FIELD_STATE _state)
    {
        UnityEngine.Object pPrefab = Resources.Load("prf_Coin"); // note: not .prefab!
        switch (_state)
        {
            case Const.FIELD_STATE.NOTHING:
            case Const.FIELD_STATE.VOID:
                pPrefab = Resources.Load("prf_empty");
                break;
            case Const.FIELD_STATE.EMPTY_SYMBOL:
            case Const.FIELD_STATE.EMPTY:
                pPrefab = Resources.Load("prf_empty_symbol");
                break;
            /*case Const.FIELD_STATE.EMPTY:
                pPrefab = Resources.Load("prf_empty");
                break;*/
            case Const.FIELD_STATE.WALL:
                pPrefab = Resources.Load("prf_wall");
                break;
            case Const.FIELD_STATE.HEAD:
                pPrefab = Resources.Load("prf_my_head_symbol");
                break;
            case Const.FIELD_STATE.BODY:
                pPrefab = Resources.Load("prf_my_body_symbol");
                break;
            case Const.FIELD_STATE.TAIL:
                pPrefab = Resources.Load("prf_my_tail_symbol");
                break;
            case Const.FIELD_STATE.ENEMY_HEAD:
                pPrefab = Resources.Load("prf_enemy_head_symbol");
                break;
            case Const.FIELD_STATE.ENEMY_BODY:
                pPrefab = Resources.Load("prf_enemy_body_symbol");
                break;
            case Const.FIELD_STATE.ENEMY_TAIL:
                pPrefab = Resources.Load("prf_enemy_tail_symbol");
                break;
            case Const.FIELD_STATE.CLOSE_CARD_NOTINTERACT:
                pPrefab = Resources.Load("prf_empty_without_active");
                break;
            case Const.FIELD_STATE.BUTTON_SLC:
                pPrefab = Resources.Load("prf_btn_slc");
                break;
            default:
                pPrefab = Resources.Load("prf_Coin"); // note: not .prefab!
                break;
        }

        GameObject goField = (GameObject)GameObject.Instantiate(pPrefab, new Vector3(x, y, z), Quaternion.identity);
        goField.transform.localScale = new Vector3(size, size, 0);
        Field field = new Field();
        int iField = _world.CreateEntityWith<Field>(out field);
        field.Object = goField;
        field.State = _state;
        field.newState = _state;
        field.Coords.X = _x;
        field.Coords.Y = _y;
        //Debug.Log("Coord: " + field.Coords.X.ToString() + "x" + field.Coords.Y.ToString());
        field.Rotate = 0f;
        return field;
    }

    private void  hideField(Field _field)
    {
        float x =_field.Object.transform.localPosition.x;
        float y = _field.Object.transform.localPosition.y;
        _field.Object.transform.localPosition = new Vector3(x,y,Const.Z_HIDE_CLOSE);
    }

    private void showField(Field _field)
    {
        float x = _field.Object.transform.localPosition.x;
        float y = _field.Object.transform.localPosition.y;
        _field.Object.transform.localPosition = new Vector3(x, y, Const.Z_CLOSE);
    }

    //make grey all elements
    private void deactiveAll()
    {
        for (int i = 0; i < _fieldFilter.EntitiesCount; i++)
        {
            //if (_fieldFilter.Components1[i].Object != null) _fieldFilter.Components1[i].Object.
            //interactable
            //_fieldFilter.Components1[i].Object
        }
    }

}