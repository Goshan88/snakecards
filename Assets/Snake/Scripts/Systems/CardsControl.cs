using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
//using static FieldProcessing;

[EcsInject]
public class CardsControl : IEcsInitSystem, IEcsRunSystem {
	float _nextCardsUpdate = Const.TimeBegin;
    EcsWorld _world = null;
    EcsFilter<Card> _cardFilter = null;
	EcsFilter<PackCard> _packcardFilter = null;
	EcsFilter<Field> _fieldFilter = null;
    EcsFilter<Snake> _snakeFilter = null;
    SNAKE_MOVE[] free_move = new SNAKE_MOVE[Const.FREE_MOVE_COORD];
    private int cnt_block_snake = 0;


    public static MatchRotate[] arr_matche = new MatchRotate[Const.PackSize*Const.RotateVariant];
    int cnt_matches = 0;

    Snake snake;
    PackCard pack;
    int[] pack_card= new int[(Const.CardSize* Const.CardSize*Const.PackSize)];

    public class MatchRotate{
			public byte CntMatches;
            public int Rotate;
            public int CardID;
            public bool Active;
            public bool Compleat;//if card full complead with field
		}

		public static readonly List<MatchRotate> Matches = new List<MatchRotate> (Const.PackSize*Const.RotateVariant);
        public static readonly List<MatchRotate> rotateCard = new List<MatchRotate>(Const.PackSize * Const.RotateVariant);

    void IEcsInitSystem.Initialize () {
        
    }

    void IEcsInitSystem.Destroy () {
        deleteAll();
    }


    void deleteAll()
    {
        for (var i = 0; i < _cardFilter.EntitiesCount; i++)
        {
            _cardFilter.Components1[i].Fields.Clear();
            _world.RemoveEntity(_cardFilter.Entities[i]);
        }
        for (var i = 0; i < _packcardFilter.EntitiesCount; i++)
        {
            var pack = _packcardFilter.Components1[i];
            if (pack != null)
            {
                pack.Pack.Clear();
                _world.RemoveEntity(_packcardFilter.Entities[i]);
            }
        }
        for (var i = 0; i < _snakeFilter.EntitiesCount; i++)
        {
            _snakeFilter.Components1[i].pack.Pack.Clear();
            _snakeFilter.Components1[i].Body.Clear();
            _world.RemoveEntity(_cardFilter.Entities[i]);
        }
    }

    void IEcsRunSystem.Run()
    {

        if (!Const.initCardsControl)
        {
            Const.initCardsControl = true;
            initCards();
            Const.init_snake = false;
            cnt_block_snake = 0;
        }

        if ((_nextCardsUpdate > Time.time) || (!Const.initCardsControl))
        {
            //Debug.Log("Update view: " + _nextUpdateTime.ToString() + "x" + Time.time.ToString());
            return;
        }
        else
        {
            _nextCardsUpdate = Time.time + Const.game_update;

            //1)loop for used cards
            //2)get coords head in cards
            //3)get tmp_mass fields around snake head, if fields does not exists than set void
            //4)loop for rotate tmp_mass and count every match and store to the match_massive
            //5)choiсe max match item and save into cards_direct max_matches and rotate of the card
            //6)next card
            //7)choice card with max matches or if have a few card than choice with min numb

            //Debug.Log("Snake entitas: " + _snakeFilter.EntitiesCount.ToString());
            //arr_matche = new MatchRotate[Const.PackSize * Const.RotateVariant];


            //check ded end for all snaks
            if ((cnt_block_snake >= _snakeFilter.EntitiesCount)&&(_snakeFilter.EntitiesCount>0))
            {
                //Debug.Log("All snakes are blocked cnt:" + cnt_block_snake.ToString());
                Const.playGame = false;
                Const.finishGame = true;
            }
            else
            {
                cnt_block_snake = 0;
            }

            for (byte j = 0; j < _snakeFilter.EntitiesCount; j++)
            {
                var snake = _snakeFilter.Components1[j];

                //check snake lose game
                int cnt = (snake.Color == Const.SNAKE_COLOR.RED) ? Const.LoseSizeSnake : Const.LoseSizeSnake;
                if (snake.Body.Count <= cnt)//one of the snake is lost game
                {
                    Debug.Log("snake " + snake.Color.ToString() + " lose game");
                    Const.playGame = false;
                    Const.finishGame = true;
                    snake.next_move = false;
                    if (snake.Color == Const.SNAKE_COLOR.BLUE) _snakeFilter.Components1[0].next_move = false;//anothe red snake make step after win
                }

                if (snake.dead_end)
                {
                    Debug.Log("snake blocked:" + snake.Color.ToString() + "  _snakeFilter.EntitiesCount:" + _snakeFilter.EntitiesCount.ToString());
                    cnt_block_snake++;
                }
                //Debug.Log("snake dir: " + snake.Direction.ToString());
                //if (snake.Direction != SNAKE_MOVE.NOTHING) continue;

                    //Debug.Log("Used card: " + snake.pack.UsedCards.ToString());
                Matches.Clear();
                cnt_matches = 0;
                //var pack =_packcardFilter.Components1[0];
                for (int i = 0; i < snake.pack.UsedCards; i++)
                {
                    //Debug.Log
                    var card = snake.pack.Pack[i];
                    //showCard(card,"card",Const.VIEW_TYPE.STATE);
                    //get coord head in the card
                    Coords head = getHeadInCard(card);
                    //Debug.Log("head card coord: " + head.X.ToString()+"x" + head.Y.ToString());
                    //get size
                    Coords begin = head;//size of begin
                    Coords end = new Coords();//size of end
                    end.X = (Const.CardSize - 1) - begin.X;
                    end.Y = (Const.CardSize - 1) - begin.Y;
                    //Debug.Log("end card coord: " + end.X.ToString() + "x" + end.Y.ToString());
                    //get coord head in the fields
                    head = snake.Body.First.Value.Coords;
                    //Debug.Log("head on field: " + head.X.ToString() + "x" + head.Y.ToString());
                    //get card around head
                    Card card_field = getCardAroundHead(head, begin, end);
                    //showCard(card_field, "field", Const.VIEW_TYPE.STATE);
                    //showCard(card, "card", Const.VIEW_TYPE.STATE);
                    //get max match and rotate
                    getFreeMoveDir(head,snake);
                    //showFreeMoveDir(j);
                    getMatches(card_field, card,i,snake);
                }
                //showMatches(Matches);
                //Debug.Log("snake "+j.ToString()+ " dir: " + snake.Direction.ToString());

                if ((snake.Direction == SNAKE_MOVE.NOTHING)&&(!snake.dead_end))
                {
                    selectCard(snake);
                    Debug.Log("snake id: " + j.ToString() + " active snake: " + Const.snake_active.ToString());
                    Const.SNAKE_COLOR color = (Const.showOpponentCard) ? Const.SNAKE_COLOR.BLUE : Const.SNAKE_COLOR.RED;
                    if (snake.Color == color)
                    {
                        showActiveCard(snake);//show card active snake in battle
                    }
                    for (int i = 0; i < snake.pack.Pack.Count; i++)
                    {
                        if (snake.pack.Pack[i].type == Const.CARD_TYPE.ACTIVE)
                        {
                            snake.Direction = calcDirection(snake.pack.Pack[i].angle);//Matches.Rotate;
                            break;
                        }
                    }

                }
                
                if (!Const.playGame)//if game finish
                {
                    snake.next_move = false;
                    continue;
                }

                if ((Const.game_step_mode) && (!Const.game_next_step))//if have no next step
                {
                    //snake.Direction = SNAKE_MOVE.NOTHING;
                    snake.next_move = false;
                    continue;
                }

                if ((Const.game_next_step) && (j == (_snakeFilter.EntitiesCount - 1)))//all snake make move
                {
                    Debug.Log("CardsControl snake " + snake.Color.ToString() + " size:" + snake.Body.Count.ToString());
                    Const.game_next_step = false;
                }

                if ((Const.cnt_move < Const.min_move)||(!Const.playGame))//check finished move
                    {
                        Debug.Log("Move finished");
                        Const.playGame = false;
                        Const.finishGame = true;
                        snake.next_move = false;
                        continue;
                    }else{
                       snake.next_move = true;
                    }


                int result = 0;
                if ((j == (_snakeFilter.EntitiesCount - 1))&&(_snakeFilter.EntitiesCount > 1))
                {
                    result = (--Const.cnt_move) * 100;
                    result = result / Const.max_move;
                    GameSceneControl.setMovesProgr(result);//debug 
                }
                else
                {
                    if (_snakeFilter.EntitiesCount == 1)
                    {
                        result = (--Const.cnt_move) * 100;
                        result = result / Const.max_move;
                        GameSceneControl.setMovesProgr(result);//debug
                    }
                }
                
            }

        }
    }

    private SNAKE_MOVE calcDirection(int _angle)
    {
        switch (_angle)
        {
            case 0:
                return SNAKE_MOVE.UP;
            case 90:
                return SNAKE_MOVE.LEFT;
            case 180:
                return SNAKE_MOVE.DOWN;
            case 270:
                return SNAKE_MOVE.RIGHT;
            default:
                return SNAKE_MOVE.UP;
        }
    }

    private int getAngle(SNAKE_MOVE _dir)
    {
        switch (_dir)
        {
            case SNAKE_MOVE.UP:
                return 0;
            case SNAKE_MOVE.LEFT:
                return 90;
            case SNAKE_MOVE.DOWN:
                return 180;
            case SNAKE_MOVE.RIGHT:
                return 270;
            default:
                return 0;
        }
    }
    

    private void showActiveCard(Snake _snake)
    {
        int id_card = 0;
       
        for(var i=0; i<_snake.pack.Pack.Count; i++)
        {
            if (_snake.pack.Pack[i].type == Const.CARD_TYPE.ACTIVE)
            {
                id_card = i;
                break;
            }
        }
        //Debug.Log("Viewed Card id: " + id_card.ToString());
        //get rotated card
        Card tmp_card = getRotateCard(_snake.pack.Pack[id_card], _snake.pack.Pack[id_card].angle);
        tmp_card.angle = _snake.pack.Pack[id_card].angle;
        //showCard(tmp_card, "state card:", Const.VIEW_TYPE.STATE);
        //view active card
        for (var i = 0; i < _cardFilter.EntitiesCount; i++)
        {
            if (_cardFilter.Components1[i].type == Const.CARD_TYPE.VIEWED) /*showCard(_cardFilter.Components1[i], "view");*///view_card = _cardFilter.Components1[i];
            {
                for (var j = 0; j < tmp_card.Fields.Count; j++)
                {
                    _cardFilter.Components1[i].Fields[j].newState = tmp_card.Fields[j].State;
                    if ((tmp_card.Fields[j].State == Const.FIELD_STATE.BODY) ||
                        (tmp_card.Fields[j].State == Const.FIELD_STATE.ENEMY_BODY) ||
                        (tmp_card.Fields[j].State == Const.FIELD_STATE.ENEMY_HEAD) ||
                        (tmp_card.Fields[j].State == Const.FIELD_STATE.ENEMY_TAIL) ||
                        (tmp_card.Fields[j].State == Const.FIELD_STATE.HEAD) ||
                        (tmp_card.Fields[j].State == Const.FIELD_STATE.TAIL))
                    {
                        _cardFilter.Components1[i].Fields[j].newRotate = (float)tmp_card.angle;
                    }
                }
                //showCard(_cardFilter.Components1[i],"state card:", Const.VIEW_TYPE.STATE);
                //showCard(_cardFilter.Components1[i], "newstate card:", Const.VIEW_TYPE.NEW_STATE);
                break;
            }
        }
        
        tmp_card.Fields.Clear();
    }
    
    //return
    private bool getMatche(Field _field, Field _card, Const.SNAKE_COLOR _color)
    {
        if (_color == Const.SNAKE_COLOR.BLUE)
        {//for enemy
            if ((_field.State == Const.FIELD_STATE.ENEMY_HEAD) && (_card.State == Const.FIELD_STATE.HEAD)) return true;
            if ((_field.State == Const.FIELD_STATE.ENEMY_BODY) && (_card.State == Const.FIELD_STATE.BODY)) return true;
            if ((_field.State == Const.FIELD_STATE.ENEMY_CURVE) && (_card.State == Const.FIELD_STATE.BODY)) return true;
            if ((_field.State == Const.FIELD_STATE.ENEMY_TAIL) && (_card.State == Const.FIELD_STATE.TAIL)) return true;
            if ((_field.State == Const.FIELD_STATE.TAIL) && (_card.State == Const.FIELD_STATE.ENEMY_TAIL)) return true;
            if ((_field.State == Const.FIELD_STATE.WALL) && (_card.State == Const.FIELD_STATE.WALL)) return true;
            if ((_field.State == Const.FIELD_STATE.EMPTY) && (_card.State == Const.FIELD_STATE.EMPTY)) return true;
        }
        else
        {//for user
            if (_field.State == _card.State) return true;
            if ((_field.State == Const.FIELD_STATE.CURVE) && (_card.State == Const.FIELD_STATE.BODY)) return true;
        }
        return false;
    }


    private void getMatches(Card _field, Card _card, int _id, Snake _snake){

            //calc element in card
            int elem_cnt = 0;
            for (int i = 0; i < _card.Fields.Count; i++)
            {
                if (_card.Fields[i].State != Const.FIELD_STATE.VOID) elem_cnt++;
            }
            //Debug.Log("element cnt: " + elem_cnt.ToString());

            if (arr_matche[cnt_matches]==null) arr_matche[cnt_matches] = new MatchRotate();
            arr_matche[cnt_matches].Rotate = 0;
            arr_matche[cnt_matches].CntMatches = 0;
            arr_matche[cnt_matches].CardID = _id;
            arr_matche[cnt_matches].Compleat = false;
            for (int i=0;i<_field.Fields.Count;i++){
                if (getMatche(_field.Fields[i],_card.Fields[i], _snake.Color))  arr_matche[cnt_matches].CntMatches++;
                //Debug.Log("Card_0: " + arr_matche[cnt_matches].ToString());
                }
            if (arr_matche[cnt_matches].CntMatches == elem_cnt) arr_matche[cnt_matches].Compleat = true;
        /*if (_id == 1)
        {
            showCard(_card, "card", Const.VIEW_TYPE.STATE);
            showCard(_field, "field", Const.VIEW_TYPE.STATE);
        }*/
            Matches.Add(arr_matche[cnt_matches++]);
                
        if (arr_matche[cnt_matches] == null) arr_matche[cnt_matches] = new MatchRotate();
        arr_matche[cnt_matches].Rotate = 90;
        arr_matche[cnt_matches].CntMatches = 0;
        arr_matche[cnt_matches].CardID = _id;
        arr_matche[cnt_matches].Compleat = false;
        Card tmp_card = getRotateCard(_card, 90);
        //showCard(_card, "card", Const.VIEW_TYPE.STATE);
        //showCard(_field, "field", Const.VIEW_TYPE.STATE);
        for (int i = 0; i < _field.Fields.Count; i++)
        {
           if (getMatche(_field.Fields[i], tmp_card.Fields[i], _snake.Color)) arr_matche[cnt_matches].CntMatches++;
        }
        if (arr_matche[cnt_matches].CntMatches == elem_cnt) arr_matche[cnt_matches].Compleat = true;
        Matches.Add(arr_matche[cnt_matches++]);

        //MatchRotate tmp3 = new MatchRotate();
        if (arr_matche[cnt_matches] == null) arr_matche[cnt_matches] = new MatchRotate();
        arr_matche[cnt_matches].Rotate = 180;
        arr_matche[cnt_matches].CardID = _id;
        arr_matche[cnt_matches].CntMatches = 0;
        arr_matche[cnt_matches].Compleat = false;
        Card tmp_card2 = getRotateCard(_card, 180);
        for (int i = 0; i < _field.Fields.Count; i++)
        {
            if (getMatche(_field.Fields[i], tmp_card2.Fields[i], _snake.Color))  arr_matche[cnt_matches].CntMatches++;
        }
        if (arr_matche[cnt_matches].CntMatches == elem_cnt) arr_matche[cnt_matches].Compleat = true;
        Matches.Add(arr_matche[cnt_matches++]);

        if (arr_matche[cnt_matches] == null) arr_matche[cnt_matches] = new MatchRotate();
        arr_matche[cnt_matches].Rotate = 270;
        arr_matche[cnt_matches].CntMatches = 0;
        arr_matche[cnt_matches].CardID = _id;
        arr_matche[cnt_matches].Compleat = false;
        Card tmp_card3 = getRotateCard(_card, 270);
        for (int i = 0; i < _field.Fields.Count; i++)
        {
            if (getMatche(_field.Fields[i], tmp_card3.Fields[i], _snake.Color)) arr_matche[cnt_matches].CntMatches++;
        }
        if (arr_matche[cnt_matches].CntMatches == elem_cnt) arr_matche[cnt_matches].Compleat = true;
        Matches.Add(arr_matche[cnt_matches++]);
    }

        Card getCardAroundHead(Coords _head,Coords _begin,Coords _end){

        Card card_around = getFilledCard();
			int x1 = _head.X-_begin.X;
			int y1 = _head.Y-_begin.Y;
			int x2 = _head.X+_end.X;
			int y2 = _head.Y+_end.Y;

        //Debug.Log("_head :" + _head.X.ToString() + "x" + _head.Y.ToString());
        //Debug.Log("_begin :" + _begin.X.ToString() + "x" + _begin.Y.ToString());
        //Debug.Log("_end :" + _end.X.ToString() + "x" + _end.Y.ToString());
        //Debug.Log("x1 " + x1.ToString() + " x2 " + x2.ToString() + " y1 " + y1.ToString() + " y2 " + y2.ToString());
        //Debug.Log("card_around cnt :" + card_around.Fields.Count.ToString());
        //showCard(card_around, "tmp", Const.VIEW_TYPE.STATE);
        Field tmp = new Field();
        //int cnt_ent = 0;
        for (var i = 0; i < Const.Game_Fields.Count; i++)
        {
            if(((Const.Game_Fields[i].Coords.X)>=x1)&&((Const.Game_Fields[i].Coords.X)<=x2)){
                if(((Const.Game_Fields[i].Coords.Y)>=y1)&&((Const.Game_Fields[i].Coords.Y)<=y2)){
                    //if enter into diapozone, to get coordinate in card minus coords x1 and y1
                    tmp.State= Const.Game_Fields[i].State;
                    tmp.Coords.X = Const.Game_Fields[i].Coords.X - x1;
                    tmp.Coords.Y = Const.Game_Fields[i].Coords.Y - y1;
                    //cnt_ent++;
                    //Debug.Log("Coord tmp: "+ tmp.Coords.X.ToString() + "x" + tmp.Coords.Y.ToString() + "x" + tmp.State.ToString());
                    //card_around.Fields.Add(_fieldFilter.Components1[i]);
                    for (var j=0; j < card_around.Fields.Count; j++)
                    {
                        if ((card_around.Fields[j].Coords.X == tmp.Coords.X) && (card_around.Fields[j].Coords.Y == tmp.Coords.Y))
                        {
                            card_around.Fields[j].State = tmp.State;
                            //Debug.Log("Field tmp: " + tmp.Coords.X.ToString() + "x" + tmp.Coords.Y.ToString() + " " + tmp.State.ToString());
                            //Debug.Log("Field card_around: " + card_around.Fields[j].Coords.X.ToString() + "x" + card_around.Fields[j].Coords.Y.ToString() + " " + card_around.Fields[j].State.ToString());
                        }
                    }
                }
            }
        }
        
        return card_around;
	}


    private void getFreeMoveDir(Coords _head, Snake _snake)
    {
        int count = 0;
        Const.FIELD_STATE snake_type = Const.FIELD_STATE.ENEMY_TAIL;//for user snake
        snake_type = (_snake.Color == Const.SNAKE_COLOR.BLUE) ? Const.FIELD_STATE.TAIL : Const.FIELD_STATE.ENEMY_TAIL;

        for (var i = 0; i < free_move.Length; i++){
            free_move[i] = SNAKE_MOVE.NOTHING;
        }

        bool have_ban = false;
        Coords[] ban_coords = new Coords[(_snakeFilter.EntitiesCount-1)];//becouse need ban coord only opponent
        //get ban coord
        if (Const.battle_type == Const.BATTLE_TYPE.BATTLE)
        {
            int cnt = 0;
            for (int i = 0; i < _snakeFilter.EntitiesCount; i++)
            {
                if (_snake.Color != _snakeFilter.Components1[i].Color)
                {
                    ban_coords[cnt++] = _snakeFilter.Components1[i].next_coords;
                }
            }
            //Debug.Log("ban coord: " + ban_coords[0].X+"x" + ban_coords[0].Y /*+" "+ ban_coords[1].X + "x" + ban_coords[1].Y*/);
        }

        for (var i = 0; i < Const.Game_Fields.Count; i++)
        {
            //cheack ban coords
            //if in next move two snake have plan move to that coord
            have_ban = false;
            for (int j = 0; j < ban_coords.Length; j++)
            {
                if ((Const.Game_Fields[i].Coords.X == ban_coords[j].X) && (Const.Game_Fields[i].Coords.Y == ban_coords[j].Y))
                {
                    have_ban = true;
                    continue;
                }
            }
            if (have_ban) continue;
            

           if (((Const.Game_Fields[i].State == Const.FIELD_STATE.EMPTY) || (Const.Game_Fields[i].State == snake_type))/*&&(Const.Game_Fields[i].newState == Const.FIELD_STATE.EMPTY)*/)
            {
                if (Const.Game_Fields[i].Coords.X == _head.X)
                {
                    if (Const.Game_Fields[i].Coords.Y == (_head.Y - 1))
                    {
                        free_move[count++] = SNAKE_MOVE.UP;
                    }
                    if (Const.Game_Fields[i].Coords.Y == (_head.Y + 1))
                    {
                        free_move[count++] = SNAKE_MOVE.DOWN;
                    }
                }
                if (Const.Game_Fields[i].Coords.Y == _head.Y)
                {
                    if (Const.Game_Fields[i].Coords.X == (_head.X - 1))
                    {
                        free_move[count++] = SNAKE_MOVE.LEFT;
                    }
                    if (Const.Game_Fields[i].Coords.X == (_head.X + 1))
                    {
                        free_move[count++] = SNAKE_MOVE.RIGHT;
                    }
                }
            }
        }
        //Debug.Log("Count move: " + count.ToString());
        //showFreeMoveDir(0);
        //if we don't have empty field for move
        if (count == 0)
        {
            _snake.dead_end = true;
        }
        else
        {
            _snake.dead_end = false;
        }
            
    }

    private void showFreeMoveDir(int _id)
    {
        string tmp = "snake id: " + _id.ToString() + " showFreeMoveDir: ";
        for (var i = 0; i < free_move.Length; i++)
        {
            tmp += free_move[i].ToString() + " ";
        }
        Debug.Log(tmp);
    }
    
    	Coords getHeadInCard(Card _card){
			for (int i=0;i<_card.Fields.Count;i++){
				if (_card.Fields[i].State == Const.FIELD_STATE.HEAD)
            {
						return _card.Fields[i].Coords;
				}
			}
			
			Debug.Log("Cant find head in the Card.");
			Coords def = new Coords();
            def.X = Const.DEF_RET_COORD;
            def.Y = Const.DEF_RET_COORD;
            return def;
		}


    void initCards()
    {

        //check type battle
        if (Const.battle_type == Const.BATTLE_TYPE.DEBUG)
        {
            //one snake
            createSnake(Const.snake_active);
        }
        else
        {
            //two snake
            //active snake
            createSnake(Const.USER_ID);
            GameSceneControl.setNameUser(PreferencyHelper.getNameSnake(Const.USER_ID));
            //other snake
            /*int opponent_id = 0;
            if (Const.snake_active == 0)
            {
                opponent_id = 1;
            }
            else
            {
                opponent_id = 0;
                
            }*/
            createSnake(Const.OPPONENT_ID);
            GameSceneControl.setNameOpponent(PreferencyHelper.getNameSnake(Const.OPPONENT_ID));
        }

        //Const.battle_type = Const.BATTLE_TYPE.NOTHING;
        Const.cnt_move = Const.max_move;
        GameSceneControl.setMovesProgr(100);//debug

        //Debug.Log("active snake id: " + Const.snake_active.ToString());
    }


        private void createSnake(int _id)
       {
        pack_card = PreferencyHelper.getPackValue(_id);
        int cnt_field = 0;
        //create Cards
        var snake = _world.CreateEntityWith<Snake>();
        snake.pack = _world.CreateEntityWith<PackCard>();
        pack = snake.pack;
        for (byte i = 0; i < Const.PackSize; i++)
        {
            //Debug.Log("Const.PackSize: " + i.ToString());
            Card card = _world.CreateEntityWith<Card>();
            int x = 0, y = 0;
            for (int j = 0; j < (Const.CardSize * Const.CardSize); j++)
            {
                Field tmp_field = new Field();
                tmp_field.Coords.X = x++;
                tmp_field.Coords.Y = y;
                tmp_field.State = (Const.FIELD_STATE)pack_card[cnt_field++];
                card.Fields.Add(tmp_field);
                //Debug.Log("Fields coord: " + pack.Pack[0].Fields[j].Coords.X + "x" + pack.Pack[0].Fields[j].Coords.Y);
                if (x >= Const.CardSize)
                {
                    y++;
                    x = 0;
                }
            }
            pack.Pack.Add(card);
        }
        //just for debug
        pack.UsedCards = PreferencyHelper.getUsedCard(_id);
        Card tmp = pack.Pack[0];
        //add empty card for random move
        Card empty_card = _world.CreateEntityWith<Card>();
        empty_card = getFilledCard();
        empty_card.Fields[empty_card.Fields.Count / 2].State = Const.FIELD_STATE.HEAD;
        pack.Pack.Add(empty_card);
        snake.Direction = SNAKE_MOVE.NOTHING;
        snake.Name = PreferencyHelper.getNameSnake(_id);
        snake.dead_end = false;
    }



        Card getFilledCard(){
		    Card _card = new Card();
            int x = 0, y = 0;
            for (int i=0;i<(Const.CardSize*Const.CardSize);i++){
                            Field tmp = new Field();
                            tmp.State = Const.FIELD_STATE.NOTHING;
                            tmp.Coords.X = x++;
					        tmp.Coords.Y = y;
                            if (x >= Const.CardSize) { y++; x = 0; }
                            _card.Fields.Add(tmp);
				    }
            //showNumbField(_card, "getFilledCard");
            return _card;
	    }


    private void showCard(Card _card, string name, Const.VIEW_TYPE type)
    {
        int cnt = 0;
        string row = name +" 0 ";
                     //change state in Card
                    for (int j = 0; j < _card.Fields.Count; j++)
                    {
                        switch(type){
                            case Const.VIEW_TYPE.STATE: row += _card.Fields[j].State.ToString() + " ";
                            break;
                            case Const.VIEW_TYPE.NEW_STATE:
                            row += _card.Fields[j].newState.ToString() + " ";
                            break;
                            case Const.VIEW_TYPE.COORD:
                            row += _card.Fields[j].Coords.X.ToString()+"x" + _card.Fields[j].Coords.Y.ToString() + " ";
                            break;
                            default: break;
            }
                        if (((++cnt)% Const.CardSize==0))
                        {
                            Debug.Log(row);
                            row = name + " " + (cnt / Const.CardSize).ToString() + " ";
                        } 
                    }
        //Debug.Log("Count card field: " + _card.Fields.Count.ToString());
    }

    private void showSnakeCntCards(Snake _snake)
    {
            if (_snake.pack.Pack != null) Debug.Log("snake cards: " + _snake.pack.Pack.Count.ToString());
     }

        private void showMatches(List<MatchRotate> _tmp)
    {
        //change state in Card
        //string row = "matches ";
        
        Debug.Log("Cnt matches: " + _tmp.Count.ToString());
        /*for (int j = 0; j < Matches.Count; j++)
        {
            //row = rotateCard[j].Rotate.ToString() + "x" + rotateCard[j].CntMatches.ToString();
            Debug.Log("j: " +j.ToString()+" "+ Matches[j].Rotate.ToString() + "x" + Matches[j].CntMatches.ToString());
        }*/
        for (int j = 0; j < _tmp.Count; j++)
        {
            //row = rotateCard[j].Rotate.ToString() + "x" + rotateCard[j].CntMatches.ToString();
            Debug.Log("j: " + j.ToString() + " card_id: " + _tmp[j].CardID.ToString() + " " + _tmp[j].Rotate.ToString() + "x" + _tmp[j].CntMatches.ToString() + 
                " Compleate: " + _tmp[j].Compleat.ToString());
        }
    }


    private void selectCard(Snake _snake)
    {
        int move_select = 0;
        //Debug.Log("Select card");
        List<MatchRotate> tmp_matches = new List<MatchRotate>(Matches.Count);
        List<MatchRotate> ready_matches = new List<MatchRotate>(Matches.Count);
        //forming new list get rotated only if they true
        for (int i = 0; i < Matches.Count; i++)
        {
            Matches[i].Active = false;
            //Matches[i].Compleat = false;
            for (var j = 0; j < free_move.Length; j++)
            {
                if (free_move[j] == calcDirection(Matches[i].Rotate)) tmp_matches.Add(Matches[i]);
            }
        }

        //showMatches(tmp_matches);

        for (int i=0; i< _snake.pack.Pack.Count; i++)
        {
            _snake.pack.Pack[i].type = Const.CARD_TYPE.NOTHING;
        }
        //Debug.Log("tmp_matches size: " + tmp_matches.Count.ToString());

        //if in the list we have not more 1 matche than we can use  random move
        bool random_move = true;
        for (int i = 0; i < tmp_matches.Count; i++)
        {
            //if (tmp_matches[i].CntMatches > Const.MIN_MATCHES_FOR_RND)//search max matches
            if (tmp_matches[i].Compleat)
            {
                random_move = false;
                break;
            }
        }

        if (random_move)
        {
            Debug.Log("Use random move");
            //if we have make moves than return
            
            int cnt = 0, angle=0;
            for (var i = 0; i < free_move.Length; i++)
            {
                if (free_move[i] != SNAKE_MOVE.NOTHING) cnt++;
            }
            if(cnt == 0)
            {
                //Debug.Log("No free move");
                return;
            }
            else
            {
                //if (_snake.Direction != SNAKE_MOVE.NOTHING) return;
                //false all card
                CardsActiveFalse(_snake);
                move_select =Random.Range(0, cnt);
                angle = getAngle(free_move[move_select]);
                //Debug.Log("Random max: " + cnt.ToString() + " value: " + move_select.ToString() + " angle: " + angle.ToString());
                setActiveCard(_snake, (_snake.pack.Pack.Count-1), angle);//last card with random angle
                setNextCoord(_snake, angle);
                return;
            }
        }

        //prepare ready compleate mathes list
        for (int i = 0; i < tmp_matches.Count; i++)
        {
            if (tmp_matches[i].Compleat) ready_matches.Add(tmp_matches[i]);//search max matches
        }
        //showMatches(tmp_matches);
        //Debug.Log("ready_matches");
        //showMatches(ready_matches);

        //search active card
        int local_cnt_matches = 0;
        int[] border = new int[2];
        border[0] = 0;
        border[1] = ready_matches.Count - 1;
        int id_card = ready_matches[0].CardID;
        //bool found_begin=false;
        for (int i = 0; i < ready_matches.Count; i++)
        {
                if(id_card != ready_matches[i].CardID)
                {
                    border[1] = (i-1);
                    break;
                }
            local_cnt_matches++;
        }

        //get random move
        if (local_cnt_matches > 1)
        {
            move_select = Random.Range(border[0], (border[1]+1));//+1 because max value not in the rnd
            ready_matches[move_select].Active = true;
            //Debug.Log("Move cnt: " + local_cnt_matches.ToString() + " id_card: " + id_card.ToString() + " Border: " + border[0].ToString() + "x" + border[1].ToString() + " " +
            //"Rnd: " + move_select.ToString());
        }
        else
        {
            //Debug.Log("Only one move: " + border[0].ToString());
            ready_matches[border[0]].Active = true;
        }

        
        //Debug.Log("max matches: " + result.CardID.ToString() + " " + result.Rotate.ToString() + " " + result.CntMatches.ToString());
        //Debug.Log("cnt matches: " + ready_matches.Count.ToString() + " card[0] id: " + ready_matches[0].CardID.ToString());
        for (int i = 0; i < ready_matches.Count; i++)
        {
            //if ((result.CardID == Matches[i].CardID) && (result.Rotate == Matches[i].Rotate))
            if (ready_matches[i].Active)
            {
                //Debug.Log("result: " + ready_matches[i].CardID.ToString() + " " + ready_matches[i].Rotate.ToString() + " " + ready_matches[i].CntMatches.ToString());
                setActiveCard(_snake, ready_matches[i].CardID, ready_matches[i].Rotate);
                setNextCoord(_snake, ready_matches[i].Rotate);
                break;
            }
        }
    }

    private void CardsActiveFalse(Snake _snake)
    {
        for (int i=0; i< _snake.pack.Pack.Count;i++)
        {
            _snake.pack.Pack[i].type = Const.CARD_TYPE.NOTHING;
            _snake.pack.Pack[i].angle = 0;
        }
    }

    private void setActiveCard(Snake _snake,int _cnt,int _angle)
    {
            _snake.pack.Pack[_cnt].type = Const.CARD_TYPE.ACTIVE;
            _snake.pack.Pack[_cnt].angle = _angle;
    }

    private void setNextCoord(Snake _snake, int _angle)
    {
        if (_snake == null) return;
        Coords tmp = _snake.Body.First.Value.Coords;
        switch (_angle)
        {
            case 0:
                tmp.Y--;
                break;
            case 90:
                tmp.X--;
                break;
            case 180:
                tmp.Y++;
                break;
            case 270:
                tmp.X++;
                break;
            default:
                break;
        }
        _snake.next_coords.X = tmp.X;
        _snake.next_coords.Y = tmp.Y;
        //Debug.Log("Snake " + _snake.Color.ToString() + " coords:" + _snake.next_coords.X.ToString() +"x"+ _snake.next_coords.Y.ToString());
    }

        Card getRotateCard(Card _card, int _angle)
    {
        Card result = getFilledCard();
        //Card active = _snake.pack.Pack[_id];
        int x = 0, y = 0;
        int a = (Const.CardSize - 1), b = Const.CardSize;

        switch (_angle)
        {
            case 0:
                for (int i = 0; i < _card.Fields.Count; i++)
                {
                    result.Fields[i].State = _card.Fields[i].State;
                }
                break;
            case 90:
                a = (Const.CardSize - 1);
                b = Const.CardSize;
                x = 0; y = 0;
                for (int i = 0; i < _card.Fields.Count; i++)
                {
                    y = a + b * x;
                    result.Fields[i].State = _card.Fields[y].State;
                    if (x++ >= (Const.CardSize - 1))
                    {
                        x = 0;
                        a--;
                    }
                }
                break;
            case 180:
                a = (Const.CardSize * Const.CardSize) - 1;
                for (int i = 0; i < _card.Fields.Count; i++)
                {
                    result.Fields[i].State = _card.Fields[a - i].State;
                }
                break;
            case 270:
                x = 0; y = 0;
                a = ((Const.CardSize - 1) * Const.CardSize);
                b = -Const.CardSize;
                for (int i = 0; i < _card.Fields.Count; i++)
                {
                    y = a + b * x;
                    result.Fields[i].State = _card.Fields[y].State;
                    if (x++ >= (Const.CardSize - 1))
                    {
                        x = 0;
                        a++;
                    }
                }
                break;
        }
        return result;
    }
}