using Leopotam.Ecs;
using UnityEngine;
using System.Collections.Generic;

public struct Coords
{
    public int X;
    public int Y;
}

public enum SNAKE_MOVE
{
    NOTHING,
    UP,
    DOWN,
    LEFT,
    RIGHT
}

[EcsInject]
public class SnakeControl : IEcsInitSystem, IEcsRunSystem {
    bool have_eat_tail = false;
    float _nextSnakeUpdate = Const.TimeBegin;
    Coords posHead = new Coords();
    
    EcsWorld _world = null;
    EcsFilter<Field> _fieldFilter = null;
    EcsFilter<Snake> _snakeFilter = null;


    private static class HeadNext
    {
        public static bool HaveCurve = false;
        public static float Angle = 0f;
    }

    void IEcsInitSystem.Initialize () {
        
    }

    void IEcsInitSystem.Destroy()
    {
        for (int k = 0; k < _snakeFilter.EntitiesCount; k++)
        {
            var snake = _snakeFilter.Components1[k];
            if (snake != null)
            {
                snake.pack.Pack.Clear();
                if (snake.pack != null) snake.pack.Pack.Clear();
                //snake.pack.Pack.Clear();
                snake.Name = null;
                _world.RemoveEntity(_snakeFilter.Entities[0]);
            }
        }
    }

    

    void IEcsRunSystem.Run() {

        if (!Const.init_snake)
        {
            Const.init_snake = true;
            initSnaks();
        }

        //view update
        if ((_nextSnakeUpdate > Time.time)||(!Const.init_snake))
        {
            return;
        }
        else
        {
            _nextSnakeUpdate = Time.time + Const.game_update;
            
			for(int k=0;k<_snakeFilter.EntitiesCount; k++){
                var mSnake = _snakeFilter.Components1[k];//may be use First

                if (!mSnake.next_move) continue;

                //get next head position
                Coords new_pos = getNextPos(mSnake.Body.First.Value.Coords, mSnake.Direction);

                //if new coords = last coord then return
                if (HaveMove(mSnake.Body.First.Value, new_pos)) continue;
			
                //Debug.Log("Next head i=" + k.ToString() + " coord: " + new_pos.X.ToString() + "x" + new_pos.Y.ToString());

                //find new head coord
			    Field newHead = getField(new_pos);
            
			    changeSnakePos(mSnake, newHead);

                //check snake eat somebody tail
                checkSnakeEated(mSnake);

                mSnake.Direction = SNAKE_MOVE.NOTHING;
                mSnake.next_move = false;
            }
        }
        updateProgressBar();
    }


    private void updateProgressBar()
    {
        for (int k = 0; k < _snakeFilter.EntitiesCount; k++)
        {
            var mSnake = _snakeFilter.Components1[k];//may be use First
            int res = ((mSnake.Body.Count-Const.LoseSizeSnake)* 100) / (2*Const.SnakeSize-Const.LoseSizeSnake);//2-couse in battle play two snake
            if (k == 0) {
                GameSceneControl.setUserProgr((float)res);
                Const.user_result = mSnake.Body.Count - Const.LoseSizeSnake;
            }
            if (k == 1)
            {
                GameSceneControl.setOpponentProgr((float)res);
                Const.opponent_result = mSnake.Body.Count - Const.LoseSizeSnake;
            }
            }
    }

    Coords getNextPos(Coords _pos, SNAKE_MOVE _move)
    {
        Coords new_pos= new Coords();
        new_pos = _pos;
        Const.FIELD_STATE new_pos_state = Const.FIELD_STATE.VOID;

        //get next coord
        switch (_move)
        {
            case SNAKE_MOVE.DOWN:
                new_pos.Y++;
                break;
            case SNAKE_MOVE.UP:
                new_pos.Y--;
                break;
            case SNAKE_MOVE.LEFT:
                new_pos.X--;
                break;
            case SNAKE_MOVE.RIGHT:
                new_pos.X++;
                break;
            default:
                return _pos;
               // break;
        }
        
        //Debug.Log("Snake move: " + SNAKE_MOVE.ToString());

        if ((new_pos.X<=Const.FIELD_BORDER)|| (new_pos.X >= (Const.FIELD_SIZE - Const.FIELD_BORDER))){
            return _pos;//nothing change
        }

        if ((new_pos.Y <= Const.FIELD_BORDER) || (new_pos.Y >= (Const.FIELD_SIZE - Const.FIELD_BORDER)))
        {
            return _pos;//nothing change
        }
        
        //check 
        for (var i = 0; i < _fieldFilter.EntitiesCount; i++)
        {
            if ((_fieldFilter.Components1[i].Coords.X == _pos.X)&&(_fieldFilter.Components1[i].Coords.Y == _pos.Y))
            {
                new_pos_state = _fieldFilter.Components1[i].State;
                break;
            }
        }

        if (new_pos_state == Const.FIELD_STATE.EMPTY)
        {
            new_pos = _pos;
        }

        //Debug.Log("Next head coord: " + new_pos.X.ToString() + "x" + new_pos.Y.ToString());
        return new_pos;
    }


    void initSnaks()
    {
        for (int i = 0; i < _snakeFilter.EntitiesCount; i++)
        {
            var Snake = _snakeFilter.Components1[i];//may be use First
                                                    //for (int i=0; i<Const.cntSnake; i++){
            Coords head = new Coords();
            if (i == Const.User_Snake)
            {
                head.X = Const.Head_X;
                head.Y = Const.Head_Y;
                Snake.Color = Const.SNAKE_COLOR.RED;
            }
            else
            {
                head.X = Const.Op_Head_X;
                head.Y = Const.Op_Head_Y;
                Snake.Color = Const.SNAKE_COLOR.BLUE;
            }
            Field tmp = new Field();
            //Debug.Log("Snake init: ");
            for (int j = 0; j < Const.SnakeSize; j++)
            {
                for (var k = 0; k < _fieldFilter.EntitiesCount; k++)
                {
                    if ((_fieldFilter.Components1[k].Coords.X == head.X) && (_fieldFilter.Components1[k].Coords.Y == head.Y))
                    {
                        tmp = _fieldFilter.Components1[k];
                        break;
                    }
                }

                //check user or opponent
                if (i == Const.User_Snake)
                {
                    tmp.newState = Const.FIELD_STATE.BODY;
                    if (j == 0) tmp.newState = Const.FIELD_STATE.HEAD;
                    if (j == (Const.SnakeSize - 1)) tmp.newState = Const.FIELD_STATE.TAIL;
                }
                else
                {
                    tmp.newState = Const.FIELD_STATE.ENEMY_BODY;
                    if (j == 0)
                    {
                        tmp.newState = Const.FIELD_STATE.ENEMY_HEAD;
                        //tmp.Rotate = 180;
                    }
                    if (j == (Const.SnakeSize - 1))
                    {
                        tmp.newState = Const.FIELD_STATE.ENEMY_TAIL;
                        //tmp.Rotate = 180;
                    }
                }
                //just for test rate racing
                //if (i == Const.User_Snake) { 
                    head.Y++;
                /*} else {
                    head.Y--;
                }*/
                Snake.Body.AddLast(tmp);
                Snake.ShouldDecrease = false;
                Snake.ShouldGrow = false;
                Snake.dead_end = false;
            }
        }
	    
    }

    void SnakeView(Snake _snake)
    {
         if (_snake != null)
        {
            Debug.Log("Size of snake: " + _snake.Body.Count.ToString());
            Debug.Log("Head snake name: " + _snake.Body.First.Value.newState.ToString());
            Debug.Log("Tail snake name: " + _snake.Body.Last.Value.newState.ToString());
        }
    }

	        //1)get current direction
            //2)get new direction
            //3)check change direction
            //4)calculate new angle(if no curve than get angle for body)
    private void GetNextBody(Snake _snake, Field _head){
        HeadNext.HaveCurve = false;
        HeadNext.Angle = 0f;

		LinkedListNode<Field> node=_snake.Body.First;
        SNAKE_MOVE curMove =GetDirection(node.Value.Coords,node.Next.Value.Coords);
        SNAKE_MOVE newMove =GetDirection(_head.Coords,node.Value.Coords);

            if (curMove==newMove){
              HeadNext.HaveCurve = false;
                if ((curMove==SNAKE_MOVE.UP)||(curMove==SNAKE_MOVE.DOWN)){
                    HeadNext.Angle = 0f;
				}else{
                    HeadNext.Angle = 90f;
				}
              return;
			}
			
			if (curMove!=newMove){
                HeadNext.HaveCurve = true;
				if((newMove==SNAKE_MOVE.RIGHT)&&(curMove==SNAKE_MOVE.UP)) HeadNext.Angle = 270f;
                if ((newMove == SNAKE_MOVE.RIGHT) && (curMove == SNAKE_MOVE.DOWN)) HeadNext.Angle = 0f;
                if ((newMove==SNAKE_MOVE.UP)&&(curMove==SNAKE_MOVE.LEFT)) HeadNext.Angle = 0f;
                if ((newMove == SNAKE_MOVE.UP) && (curMove == SNAKE_MOVE.RIGHT)) HeadNext.Angle = 90f;
                if ((newMove==SNAKE_MOVE.LEFT)&&(curMove==SNAKE_MOVE.DOWN)) HeadNext.Angle = 90f;
                if ((newMove == SNAKE_MOVE.LEFT) && (curMove == SNAKE_MOVE.UP)) HeadNext.Angle = 180f;
                if ((newMove==SNAKE_MOVE.DOWN)&&(curMove==SNAKE_MOVE.RIGHT)) HeadNext.Angle = 180f;
                if ((newMove == SNAKE_MOVE.DOWN) && (curMove == SNAKE_MOVE.LEFT)) HeadNext.Angle = 270f;
        }
	}
	
	
    //get direction
    private SNAKE_MOVE GetDirection(Coords _new, Coords _cur){
		 SNAKE_MOVE direction = SNAKE_MOVE.NOTHING;
		if(_new.X==_cur.X){
			if(_new.Y<_cur.Y){
				direction = SNAKE_MOVE.UP;
			}else{
				direction = SNAKE_MOVE.DOWN;
			}
		}
		if(_new.Y==_cur.Y){
			if(_new.X<_cur.X){
				direction = SNAKE_MOVE.LEFT;
			}else{
				direction = SNAKE_MOVE.RIGHT;
			}
		}
		return direction;
	}
	
	float getBodyAngle(Field _next, Field _cur){
		float angle = 0f;
		var direct=GetDirection(_next.Coords,_cur.Coords);
		if((direct==SNAKE_MOVE.UP)||(direct==SNAKE_MOVE.DOWN)){
					angle = 0f;
				}else{
					angle = 90f;
				}
		return angle;
	}
	
	float getHeadAngle(Field _next, Field _cur){
		float angle = 0f;
		var direct=GetDirection(_next.Coords,_cur.Coords);
		if(direct==SNAKE_MOVE.UP)	angle = 180f;
		if(direct==SNAKE_MOVE.LEFT)	angle = 270f;
		if(direct==SNAKE_MOVE.DOWN)	angle = 0f;
		if(direct==SNAKE_MOVE.RIGHT)angle = 90f;
		return angle;
	}

    float getTailAngle(Field _next, Field _cur)
    {
        //Debug.Log("_field _cur coord: " + _cur.Coords.X + "x" + _cur.Coords.Y + "_next coord: " + _next.Coords.X + "x" + _next.Coords.Y);
        float angle = 0f;
        var direct = GetDirection(_next.Coords, _cur.Coords);
        if (direct == SNAKE_MOVE.UP) angle = 0f;
        if (direct == SNAKE_MOVE.LEFT) angle = 90f;
        if (direct == SNAKE_MOVE.DOWN) angle = 180f;
        if (direct == SNAKE_MOVE.RIGHT) angle = 270f;
        return angle;
    }

    float getTailAngle(Snake _snake)//method for calc rotate angle tail after was eated
    {
        float angle = 0f;
        LinkedListNode<Field> node;
        Field _next=new Field(), _cur = new Field();
        bool first = true;
        for (node = _snake.Body.Last; node != null; node = node.Previous)
        {
               _cur = node.Value;
               _next = node.Previous.Value;
                break;
        }
        //Debug.Log("snake: " + _snake.Color.ToString() + " _cur coord: " + _cur.Coords.X+"x"+ _cur.Coords.Y + "_next coord: " + _next.Coords.X + "x" + _next.Coords.Y);
        var direct = GetDirection(_next.Coords, _cur.Coords);
        if (direct == SNAKE_MOVE.UP) angle = 0f;
        if (direct == SNAKE_MOVE.LEFT) angle = 90f;
        if (direct == SNAKE_MOVE.DOWN) angle = 180f;
        if (direct == SNAKE_MOVE.RIGHT) angle = 270f;
        return angle;
    }


    bool HaveMove(Field _head, Coords new_pos)
    {
		if ((_head.Coords.X == new_pos.X) && (_head.Coords.Y == new_pos.Y))
            {
                //just for debug
                if (Const.snake_move == SNAKE_MOVE.RIGHT) { Const.snake_move = SNAKE_MOVE.UP; return true; }
                if (Const.snake_move == SNAKE_MOVE.UP){ Const.snake_move = SNAKE_MOVE.LEFT; return true; }
                if (Const.snake_move == SNAKE_MOVE.LEFT){ Const.snake_move = SNAKE_MOVE.DOWN; return true; }
                if (Const.snake_move == SNAKE_MOVE.DOWN){ Const.snake_move = SNAKE_MOVE.RIGHT; return true; }
                return true;
            }
        return false;
    }
	
	Field getField(Coords new_pos){
	            Field newHead = _fieldFilter.Components1[7]; //7-just it is because i can't use empty
        for (var i = 0; i < _fieldFilter.EntitiesCount; i++)
        {
            if ((new_pos.X == _fieldFilter.Components1[i].Coords.X) && (new_pos.Y == _fieldFilter.Components1[i].Coords.Y))
            {
                newHead = _fieldFilter.Components1[i];
                //Debug.Log("newHead: " + newHead.Coords.X.ToString() + "x" + newHead.Coords.Y.ToString());
                break;
            }
        }
		return newHead;
    }
	
	//change position of snake
	void changeSnakePos(Snake mSnake, Field newHead){
            //calc next param of 
			GetNextBody(mSnake,newHead);
            LinkedListNode<Field> node;
            Field last_field = null;

            for (node = mSnake.Body.Last; node != null; node=node.Previous){
                
				if (last_field!=null)
                {
					if((last_field.State == Const.FIELD_STATE.TAIL)|| (last_field.State == Const.FIELD_STATE.ENEMY_TAIL)){
                        node.Value.newState = last_field.State;
						node.Value.Rotate = getTailAngle(node.Previous.Value, node.Value);
                        if (mSnake.Color == Const.SNAKE_COLOR.BLUE)
                        {
                            Debug.Log("Blue tail: newState " + node.Value.newState.ToString() + " State " + node.Value.State.ToString());
                        }
                    //Debug.Log("Check rotate: " + getTailAngle(node.Previous.Value, node.Value).ToString()+"x"+ getTailAngle(mSnake).ToString());
                }
                else{
						if((node.Value.State == Const.FIELD_STATE.CURVE)||(last_field.State == Const.FIELD_STATE.CURVE)||
                        (node.Value.State == Const.FIELD_STATE.ENEMY_CURVE) || (last_field.State == Const.FIELD_STATE.ENEMY_CURVE)){
							//nothing do couse need curve locate at the same position
						}else{
                          node.Value.newState = last_field.State;
                     	}
					}
                //Debug.Log("Snake segment: " + node.Value.newState.ToString());
                }
                else
                {
                    if(((node.Value.State == Const.FIELD_STATE.ENEMY_BODY)||(node.Value.State == Const.FIELD_STATE.ENEMY_CURVE))&&(!mSnake.ShouldGrow))
                    {
                        node.Value.State = Const.FIELD_STATE.ENEMY_TAIL;
                        Debug.Log("Lose enemy tail: "+ node.Value.State.ToString());
                    }
                }
                last_field = node.Value;
                //Debug.Log("Snake segment: " + node.ToString());
            }
			//change snake segment after head
			if(HeadNext.HaveCurve)
            {
                if (mSnake.Color == Const.SNAKE_COLOR.RED)
                {
                    last_field.newState = Const.FIELD_STATE.CURVE;
                }
                else
                {
                    last_field.newState = Const.FIELD_STATE.ENEMY_CURVE;
                }
			}else{
                if (mSnake.Color == Const.SNAKE_COLOR.RED)
                {
                    last_field.newState = Const.FIELD_STATE.BODY;
                }
                else
                {
                    last_field.newState = Const.FIELD_STATE.ENEMY_BODY;
                }
			}
			last_field.Rotate = HeadNext.Angle;
        //Debug.Log("last_field: " + last_field.newState.ToString() + "x" + last_field.Rotate.ToString());
            
            if (mSnake.Color == Const.SNAKE_COLOR.RED)
            {
                newHead.newState = Const.FIELD_STATE.HEAD;
                have_eat_tail = (newHead.State == Const.FIELD_STATE.ENEMY_TAIL) ? true : false; 
            }
            else
            {
                newHead.newState = Const.FIELD_STATE.ENEMY_HEAD;
                have_eat_tail = (newHead.State == Const.FIELD_STATE.TAIL) ? true : false;
            }

            //for detect rate racing
            mSnake.ShouldGrow = have_eat_tail;


            newHead.Rotate = getHeadAngle(last_field, newHead);
            mSnake.Body.AddFirst(newHead);//add new head

        if ((mSnake.Color == Const.SNAKE_COLOR.BLUE) && (mSnake.ShouldDecrease))
        {
            mSnake.Body.Last.Value.newState = Const.FIELD_STATE.EXPLOSION;
            mSnake.ShouldDecrease = false;
        }
        else
        {
            mSnake.Body.Last.Value.newState = Const.FIELD_STATE.EMPTY;
        }
            

            if (!have_eat_tail)
            {
                mSnake.Body.RemoveLast();//Tail is deleted       
            }
            else
            {
                LinkedListNode<Field>  node_tail = mSnake.Body.Last;
                LinkedListNode<Field> node_body = node_tail.Previous;
                LinkedListNode<Field> node_prev_body = node_body.Previous;

            if (!getCurve(node_tail.Value.Coords, node_body.Value.Coords, node_prev_body.Value.Coords))
            {
                Debug.Log("Add body to the snake " + mSnake.Color.ToString());
                node_tail.Value.newState = (mSnake.Color == Const.SNAKE_COLOR.RED) ? Const.FIELD_STATE.TAIL : Const.FIELD_STATE.ENEMY_TAIL;
                node_body.Value.newState = (mSnake.Color == Const.SNAKE_COLOR.RED) ? Const.FIELD_STATE.BODY : Const.FIELD_STATE.ENEMY_BODY;
            }
            else
            {
                Debug.Log("Add curve to the snake " + mSnake.Color.ToString());
                node_tail.Value.newState = (mSnake.Color == Const.SNAKE_COLOR.RED) ? Const.FIELD_STATE.TAIL : Const.FIELD_STATE.ENEMY_TAIL;
                node_body.Value.newState = (mSnake.Color == Const.SNAKE_COLOR.RED) ? Const.FIELD_STATE.CURVE : Const.FIELD_STATE.ENEMY_CURVE;
                node_body.Value.Rotate = HeadNext.Angle;
            }
          }
	}


    private bool getCurve(Coords tail, Coords body, Coords previous_body)
    {
        //bool res = false;
        //Debug.Log("getCurve tail: " + tail.X.ToString() + "x" + tail.Y.ToString() + " body: "+ body.X.ToString()+"x" + body.Y.ToString() + " previous_body: " + previous_body.X.ToString() + "x" + previous_body.Y.ToString());
            if ((tail.X == body.X) && (body.X == previous_body.X)) return false;//in one line
            if ((tail.Y == body.Y) && (body.Y == previous_body.Y)) return false;
            SNAKE_MOVE newMove = GetDirection(previous_body, body);
            SNAKE_MOVE curMove = GetDirection(body,tail);
        
        if ((newMove == SNAKE_MOVE.RIGHT) && (curMove == SNAKE_MOVE.UP)) HeadNext.Angle = 270f;
        if ((newMove == SNAKE_MOVE.RIGHT) && (curMove == SNAKE_MOVE.DOWN)) HeadNext.Angle = 0f;
        if ((newMove == SNAKE_MOVE.UP) && (curMove == SNAKE_MOVE.LEFT)) HeadNext.Angle = 0f;
        if ((newMove == SNAKE_MOVE.UP) && (curMove == SNAKE_MOVE.RIGHT)) HeadNext.Angle = 90f;
        if ((newMove == SNAKE_MOVE.LEFT) && (curMove == SNAKE_MOVE.DOWN)) HeadNext.Angle = 90f;
        if ((newMove == SNAKE_MOVE.LEFT) && (curMove == SNAKE_MOVE.UP)) HeadNext.Angle = 180f;
        if ((newMove == SNAKE_MOVE.DOWN) && (curMove == SNAKE_MOVE.RIGHT)) HeadNext.Angle = 180f;
        if ((newMove == SNAKE_MOVE.DOWN) && (curMove == SNAKE_MOVE.LEFT)) HeadNext.Angle = 270f;

        Debug.Log("getCurve angle: "+ HeadNext.Angle.ToString());
        return true;
    }

    private void checkSnakeEated(Snake _snake)
    {
        if (!have_eat_tail) return;
        //search snake was eated
        Snake tmp_snake= _snake;
        for (int k = 0; k < _snakeFilter.EntitiesCount; k++)
        {
            if (_snakeFilter.Components1[k].Color != _snake.Color)
            {
                tmp_snake = _snakeFilter.Components1[k];
                continue;
            }
        }

        tmp_snake.ShouldDecrease = true;

        if ((tmp_snake.Color == Const.SNAKE_COLOR.RED) && (tmp_snake.dead_end))
        {
            //tmp_snake.Body.Last.Value.newState = Const.FIELD_STATE.EMPTY;//cut the head of blue snake
            LinkedListNode<Field> node_tail = tmp_snake.Body.Last;
            node_tail.Previous.Value.newState = Const.FIELD_STATE.EMPTY;
            Debug.Log("RedSnake in stop");            
        }

        if ((tmp_snake.Color == Const.SNAKE_COLOR.RED) && (!tmp_snake.dead_end))
         {
                //check if we have blue had in this field than nothing do!
                if (tmp_snake.Body.Last.Value.newState != Const.FIELD_STATE.ENEMY_HEAD)
                {
                    tmp_snake.Body.Last.Value.newState = Const.FIELD_STATE.EXPLOSION;//couse first move make red
                }
                else
                {
                    Debug.Log("RedSnake we have Blue head on the tail");
                }
                Debug.Log("RedSnake in move");
         }

        //if blue snake add symbol explosion
        if ((tmp_snake.Color == Const.SNAKE_COLOR.BLUE)&&(tmp_snake.Body.Last.Value.newState != Const.FIELD_STATE.HEAD)) tmp_snake.Body.Last.Value.newState = Const.FIELD_STATE.EXPLOSION;
        if (tmp_snake.Color == Const.SNAKE_COLOR.BLUE){
            Debug.Log("Blue tail state:" + tmp_snake.Body.Last.Value.State.ToString() + " newState: " + tmp_snake.Body.Last.Value.newState.ToString());
        }
        //remove eated tail
        tmp_snake.Body.RemoveLast();
        
        
        if ((tmp_snake.Color == Const.SNAKE_COLOR.RED)/*&&(!tmp_snake.dead_end)*/) tmp_snake.Body.Last.Value.newState = Const.FIELD_STATE.TAIL;//couse first move make red

        if ((tmp_snake.Color == Const.SNAKE_COLOR.BLUE) && (tmp_snake.dead_end))
        {
            tmp_snake.Body.Last.Value.newState = Const.FIELD_STATE.ENEMY_TAIL;//lose tail in normal mode
            //tmp_snake.Body.Last.Value.State = Const.FIELD_STATE.ENEMY_TAIL;
            Debug.Log("add Tail Blue in stop");
        }

        if ((tmp_snake.Color == Const.SNAKE_COLOR.BLUE) &&(!tmp_snake.dead_end))
            {
                if (!tmp_snake.ShouldGrow)
                {
                    //tmp_snake.Body.Last.Value.newState = Const.FIELD_STATE.ENEMY_TAIL;//cut the snake blue tail in normal mode
                    Debug.Log("add Tail Blue in normal mode");
                }
                else
                {
                    tmp_snake.Body.Last.Value.newState = Const.FIELD_STATE.ENEMY_TAIL;//in rate racing mode cut the tail
                    Debug.Log("add Tail Blue with rate racing");
                }
         }
        
        tmp_snake.Body.Last.Value.Rotate = getTailAngle(tmp_snake);

        //Debug.Log("snake " + tmp_snake.Color.ToString() + " tail rotate: " + tmp_snake.Body.Last.Value.Rotate);
        Debug.Log("snake " + tmp_snake.Color.ToString() + " snake size: " + tmp_snake.Body.Count.ToString());
    }
}