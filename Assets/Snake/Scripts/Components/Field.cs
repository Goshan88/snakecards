using UnityEngine;
using Leopotam.Ecs;

public sealed class Field {
    public Coords Coords;
    public Const.FIELD_STATE newState;
    public Const.FIELD_STATE State;
    public float Rotate;
    public float newRotate;
    [EcsIgnoreNullCheck]
    public GameObject Object;
}