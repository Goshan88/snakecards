using UnityEngine;
using Leopotam.Ecs;
using System.Collections.Generic;

public sealed class Card {
    //public Const.Card GameCard;
    //public static Const.FIELD_STATE[] Fields/* = new Const.FIELD_STATE[(Const.CardSize * Const.CardSize)]*/;
	public Const.CARD_TYPE type;
    public int angle;
    [EcsIgnoreNullCheck]
    public readonly List<Field> Fields = new List<Field>((Const.CardSize*Const.CardSize+Const.OFF_CARD_EDIT));
}