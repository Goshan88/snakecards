using System.Collections.Generic;
using Leopotam.Ecs;

sealed class Snake {
    [EcsIgnoreNullCheck]
    public readonly LinkedList<Field> Body = new LinkedList<Field>();
    public SNAKE_MOVE Direction = SNAKE_MOVE.UP;
    public bool next_move;
    public Coords next_coords;
    public bool dead_end = false;
    public bool ShouldGrow = false;//for detect rate racing
    public bool ShouldDecrease=false;//for blue snake that show explosion 
    public int SequencyOfMove;
	public Const.SNAKE_COLOR Color;
    [EcsIgnoreNullCheck]
    public PackCard pack;
    public string Name;
}