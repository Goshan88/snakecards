using System.Collections.Generic;
using Leopotam.Ecs;

sealed class PackCard {
    [EcsIgnoreNullCheck]
    public readonly List<Card> Pack = new List<Card> (9);
	public int UsedCards;
}