﻿using System;
using UnityEngine;

public class CardHelper
{
	public Card getFilledCard(){
		    Card _card = new Card();
            int x = 0, y = 0;
            for (int i=0;i<(Const.CardSize*Const.CardSize);i++){
                            Field tmp = new Field();
                            tmp.State = Const.FIELD_STATE.NOTHING;
                            tmp.Coords.X = x++;
					        tmp.Coords.Y = y;
                            if (x >= Const.CardSize) { y++; x = 0; }
                            _card.Fields.Add(tmp);
				    }
            //showNumbField(_card, "getFilledCard");
            return _card;
	    }

    public void showCard(Card _card, string name, Const.VIEW_TYPE type)
    {
        int cnt = 0;
        string row = name + " 0 ";
        //change state in Card
        for (int j = 0; j < _card.Fields.Count; j++)
        {
            switch (type)
            {
                case Const.VIEW_TYPE.STATE:
                    row += _card.Fields[j].State.ToString() + " ";
                    break;
                case Const.VIEW_TYPE.NEW_STATE:
                    row += _card.Fields[j].newState.ToString() + " ";
                    break;
                case Const.VIEW_TYPE.COORD:
                    row += _card.Fields[j].Coords.X.ToString() + "x" + _card.Fields[j].Coords.Y.ToString() + " ";
                    break;
                default: break;
            }
            //row += _card.Fields[j].newState.ToString() + " ";
            //

            //row += j.ToString() + " ";
            if (((++cnt) % Const.CardSize == 0))
            {
                Debug.Log(row);
                row = name + " " + (cnt / 6).ToString() + " ";
            }
        }
        Debug.Log("Count card field: " + _card.Fields.Count.ToString());
    }

}