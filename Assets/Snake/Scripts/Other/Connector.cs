﻿

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using System.Collections.Generic;

public class Connector : MonoBehaviour {


    private SmartFox sfs;
    private string name_user = "Toxic";
    private RoomSettings settings;
    float _nextSFSupdate = Const.TimeBegin;
    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Start() {
		// Initialize UI
		
		#if !UNITY_WEBGL
		//portInput.text = defaultTcpPort.ToString();
		#else
		portInput.text = defaultWsPort.ToString();
		#endif

	}

    void OnDestroy()
    {
        reset();
    }

    void Update()
    {
        // As Unity is not thread safe, we process the queued up callbacks on every frame
        if (sfs != null) sfs.ProcessEvents();
        if (!PreferencyHelper.getUserTrening()) return;
        
        if (_nextSFSupdate > Time.time)
        {
            return;
        }
        else
        {
            _nextSFSupdate = Time.time + Const.server_update;
            ConnectControl();
        }
    }

    private void ConnectControl()
    {
        //Debug.Log("Connect state: " + Const.sfs_state.ToString() + " cnt_error: " + Const.cnt_sfs_con_err.ToString());

        switch (Const.sfs_state)
        {
            case Const.CONNECT_STATE.NOTHING:
                Const.cnt_sfs_con_err = 0;
                Const.sfs_state = Const.CONNECT_STATE.RESET;
                break;
            case Const.CONNECT_STATE.RESET:
                reset();
                Const.cnt_attempt_con = 0;
                Const.sfs_state = Const.CONNECT_STATE.CONNECT_SFS;
                break;
            case Const.CONNECT_STATE.CONNECT_SFS:
                OnConnect();
                if (Const.cnt_attempt_con++ > Const.MAX_ATTEMPT_CON) Const.sfs_state = Const.CONNECT_STATE.ERROOR_TRY_CON;
                break;
            case Const.CONNECT_STATE.LOGIN:
                requestLogin();
                if (Const.cnt_attempt_con++ > Const.MAX_ATTEMPT_CON) Const.sfs_state = Const.CONNECT_STATE.ERROOR_TRY_CON;
                break;
            case Const.CONNECT_STATE.CREATE_ROOM:
                CreateRoom();
                if (Const.cnt_attempt_con++ > Const.MAX_ATTEMPT_CON) Const.sfs_state = Const.CONNECT_STATE.ERROOR_TRY_CON;
                break;
            case Const.CONNECT_STATE.REQUEST_EXCHANGE:
                if (Const.prepare_request)
                {
                    sendRequest();
                    Const.request_state = Const.REQUEST_TYPE.NOTHING;
                    Const.prepare_request = false;
                }
                break;
            case Const.CONNECT_STATE.DISCONNECT:
                sfs.Send(new LeaveRoomRequest());
                reset();
                Const.sfs_state = Const.CONNECT_STATE.STANDBY;
                break;
            case Const.CONNECT_STATE.ERROOR_CON_FAILED:
            case Const.CONNECT_STATE.ERROOR_CON_LOST:
            case Const.CONNECT_STATE.ERROOR_LOGIN:
            case Const.CONNECT_STATE.ERROOR_TRY_CON:
                Const.sfs_state = Const.CONNECT_STATE.RESET;
                if (Const.cnt_sfs_con_err++ > Const.MAX_ATTEMPT_TO_SFS)
                {
                    Debug.Log("Can't connect to the SFS server");
                    Const.sfs_state = Const.CONNECT_STATE.CANT_CON_SFS;
                    reset();
                }
                break;
            case Const.CONNECT_STATE.CANT_CON_SFS:
                if (Const.cnt_time_delay_error++ > Const.MAX_TIME_DELAY)
                {
                    Const.cnt_time_delay_error = 0;
                    Const.sfs_state = Const.CONNECT_STATE.NOTHING;
                }
                break;
            case Const.CONNECT_STATE.STANDBY:
                break;
            default:
                break;
        }
    }

	void OnApplicationQuit() {
		// Always disconnect before quitting
		if (sfs != null && sfs.IsConnected)
			sfs.Disconnect ();
	}


    public void CreateRoom() {

        //RoomSettings settings = new RoomSettings(sfs.MySelf.Name);
        RoomSettings settings = new RoomSettings(PreferencyHelper.getIdSnakeDataBase(Const.USER_ID));
        if ((settings.Name.Equals(Const.DEF_ID_SNAKE_DB))||(settings.Name.Length<Const.MIN_SIZE_ROOM_NAME))
        {
            float tmp = UnityEngine.Random.Range(0, Const.MaxIdRnd);
            settings.Name = tmp.ToString();
        }
     Debug.Log("CreateRoom: " + settings.Name);

    settings.IsGame = true;
	settings.MaxUsers = 1;
	settings.MaxSpectators = 0;
    settings.Extension = new RoomExtension(Const.RoomID,Const.RoomPaket);
        try
        {
            sfs.Send(new CreateRoomRequest(settings, true, sfs.LastJoinedRoom));
        }
        catch (Exception e)
        {
            Debug.Log("Create Room Error: " + e.ToString());
        }

    }
	
    public void requestLogin()
    {
        //login 
        var snake = PreferencyHelper.getIdSnakeDataBase(Const.USER_ID);
        if (snake.Equals(Const.DEF_ID_SNAKE_DB))
        {
            float tmp = UnityEngine.Random.Range(0, Const.MaxIdRnd);
            snake = tmp.ToString();
        }
        sfs.Send(new Sfs2X.Requests.LoginRequest(snake));
    }

	public void OnConnect() {
		if (sfs == null || !sfs.IsConnected) {
            Debug.Log("Sfs OnConnect");

            // Initialize SFS2X client and add listeners
            // WebGL build uses a different constructor
            #if !UNITY_WEBGL
            sfs = new SmartFox();
			#else
			sfs = new SmartFox(UseWebSocket.WS_BIN);
			#endif

			//connection
			sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
			sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);

            //login
            sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
            sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);

            //room request
            sfs.AddEventListener(SFSEvent.ROOM_ADD, onRoomAdded);
			sfs.AddEventListener(SFSEvent.ROOM_REMOVE, onRoomRemoved);

            //handler request
            sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, onExtensionResponse);

            //need delete
            sfs.AddLogListener(LogLevel.INFO, OnInfoMessage);
			sfs.AddLogListener(LogLevel.WARN, OnWarnMessage);
			sfs.AddLogListener(LogLevel.ERROR, OnErrorMessage);
			
			// Set connection parameters
			ConfigData cfg = new ConfigData();
			cfg.Host = Const.defaultHost;
            cfg.Port = Const.defaultTcpPort;// Convert.ToInt32(portInput.text);
            cfg.Zone = Const.ZoneName;

            // Connect to SFS2X
            try
            {
                sfs.Connect(cfg);
            }catch(Exception e)
            {
                Const.sfs_state = Const.CONNECT_STATE.ERROOR_TRY_CON;
                Debug.Log("sfsConnect error: " + e.ToString());
            }
        }
        else {	
			// Disconnect from SFS2X
			sfs.Disconnect();
		}
	}

	//----------------------------------------------------------
	// Private helper methods
	//----------------------------------------------------------
	
	private void reset() {
        // Remove SFS2X listeners
        if (sfs != null) {
            //if(sfs.Config != null) sfs.Disconnect();

            sfs.RemoveEventListener(SFSEvent.CONNECTION, OnConnection);
            sfs.RemoveEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);

            //login
            sfs.RemoveEventListener(SFSEvent.LOGIN, OnLogin);
            sfs.RemoveEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);

            //room request
            sfs.RemoveEventListener(SFSEvent.ROOM_ADD, onRoomAdded);
            sfs.RemoveEventListener(SFSEvent.ROOM_REMOVE, onRoomRemoved);

            //request from server
            sfs.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, onRoomRemoved);

            sfs.RemoveLogListener(LogLevel.INFO, OnInfoMessage);
            sfs.RemoveLogListener(LogLevel.WARN, OnWarnMessage);
            sfs.RemoveLogListener(LogLevel.ERROR, OnErrorMessage);

            sfs = null;
        }
    }

    public void OnExtensionresponse(BaseEvent evt){
	//processing server answer 
	}
	
	//----------------------------------------------------------
	// SmartFoxServer event listeners
	//----------------------------------------------------------
	
	private void OnConnection(BaseEvent evt) {
		if ((bool)evt.Params["success"]) {
            Debug.Log("Connection established successfully");
            Debug.Log("SFS2X API version: " + sfs.Version);
            Debug.Log("Connection mode is: " + sfs.ConnectionMode);
            Const.sfs_state = Const.CONNECT_STATE.LOGIN;
        }
        else {
            Debug.Log("Connection failed; is the server running at all?");
            Const.sfs_state = Const.CONNECT_STATE.ERROOR_CON_FAILED;
            // Remove SFS2X listeners and re-enable interface
            //reset();
		}
	}
	
	private void OnConnectionLost(BaseEvent evt) {
        //trace("Connection was lost; reason is: " + (string)evt.Params["reason"]);
        Debug.Log("Connection was lost; reason is: " + (string)evt.Params["reason"]);
        Const.sfs_state = Const.CONNECT_STATE.ERROOR_CON_LOST;
        // Remove SFS2X listeners and re-enable interface
        //reset();
    }
	
	private void onRoomAdded(BaseEvent evt){
		Debug.Log("onRoomAdded: " + evt.ToString());
        Const.sfs_state = Const.CONNECT_STATE.REQUEST_EXCHANGE;
    }

    
    public void sendRequest()
    {
        sfs.Send(new ExtensionRequest(Const.cmd_request, Const.object_request, sfs.LastJoinedRoom));
        Debug.Log("sendRequest: "+ Const.cmd_request.ToString());
    }

    public void onExtensionResponse(BaseEvent evt)
    {
        Debug.Log("Server cmd: " + (String)evt.Params[Const.PARAM_CMD]);
        ISFSObject para = (ISFSObject)evt.Params[Const.PARAM_PARAMS];
        //Debug.Log("Server getId: " + para.GetUtfString(Const.PARAM_ID_SNAKE));
        switch ((String)evt.Params[Const.PARAM_CMD])
        {
            case Const.SRV_CMD_GETSNAKE:
                /*if (PreferencyHelper.getIdSnakeDataBase(Const.snake_active).Length < Const.MIN_LEN_ID_STR)
                {*/
                    int raiting_client = PreferencyHelper.getRaiting(Const.USER_ID);
                    int raiting_server = para.GetInt(Const.PARAM_RATING_SNAKE);
                    if (raiting_client!= raiting_server) { PreferencyHelper.setRaiting(Const.USER_ID, raiting_server); }
                    PreferencyHelper.setIdSnakeDataBase(Const.USER_ID, para.GetUtfString(Const.PARAM_ID_SNAKE));
                    Debug.Log("Snake " + Const.USER_ID + " id:" + para.GetUtfString(Const.PARAM_ID_SNAKE));
                //}
                break;
            case Const.SRV_CMD_GETUSER:
                /*if (PreferencyHelper.getIdUserDataBase().Length < Const.MIN_LEN_ID_STR)
                {*/
                    PreferencyHelper.setIdUserDataBase(para.GetUtfString(Const.PARAM_ID_USER));
                    Debug.Log("User id:" + para.GetUtfString(Const.PARAM_ID_USER));
                //}
                break;
            case Const.SRV_CMD_OPPONENT:
                Debug.Log("Snake id: " + para.GetUtfString(Const.PARAM_ID_OPPONENT));
                Debug.Log("Rating: " + para.GetInt(Const.PARAM_RATING_OPPONENT));
                Debug.Log("Used Cards: " + para.GetInt(Const.PARAM_UC_OPPONENT));
                Debug.Log("Snake Name: " + para.GetUtfString(Const.PARAM_NAME_OPPONENT));
                Debug.Log("Keys: " + para.GetKeys().ToString());
                //int[] tmp = para.GetIntArray(Const.PARAM_PACK_OPPONENT);
                //Debug.Log("tmp: " + tmp.Length.ToString());
                Debug.Log("Pack: " + para.GetIntArray(Const.PARAM_PACK_OPPONENT).Length.ToString());
                //save in peferency 
                try
                {
                    PreferencyHelper.setUsedCard(Const.OPPONENT_ID, para.GetInt(Const.PARAM_UC_OPPONENT));
                    PreferencyHelper.setRaiting(Const.OPPONENT_ID, para.GetInt(Const.PARAM_RATING_OPPONENT));
                    PreferencyHelper.setIdSnakeDataBase(Const.OPPONENT_ID, para.GetUtfString(Const.PARAM_ID_OPPONENT));
                    PreferencyHelper.setNameSnake(Const.OPPONENT_ID, para.GetUtfString(Const.PARAM_NAME_OPPONENT));
                    PreferencyHelper.setPackArray(Const.OPPONENT_ID, para.GetIntArray(Const.PARAM_PACK_OPPONENT));
                }
                catch (Exception e)
                {
                    
                }
                //Const.need_new_opponent = false;
                //disable from server
                //Const.ServerCMD.AddLast((String)evt.Params[Const.SRV_CMD_DISABLE]);
                //Const.sfs_state = Const.CONNECT_STATE.DISCONNECT;
                break;
            case Const.REQUEST_RAITING_SNAKES:
                ISFSArray raiting = null;
                ISFSObject tmp = null;
                try
                {
                    raiting = para.GetSFSArray(Const.PARAM_RAITING_SNAKES);
                }
                catch (Exception e)
                {
                    Debug.Log("Excep. get raiting array from request: " + e.ToString());
                    break;
                }
                Const.need_raiting = false;

                int size = raiting.Size();
                for (int i=0;i<size; i++)
                {
                    tmp = raiting.GetSFSObject(i);
                    if(tmp!= null)
                    {
                        Const.SnakeName[i] = tmp.GetUtfString(Const.PARAM_NAME_SNAKE);
                        Const.SnakeRaiting[i] = tmp.GetInt(Const.PARAM_RATING_SNAKE);
                        Debug.Log("Snake name: " + Const.SnakeName[i]+ " raiting" + Const.SnakeRaiting[i]);
                    }
                }
                break;
            default:
                break;
        }

        /*
        if (Const.need_new_opponent)
        {
            Const.ServerCMD.AddLast(Const.SRV_CMD_OPPONENT);
            Const.need_new_opponent = false;
        }
        */
        if (((String)evt.Params[Const.PARAM_CMD]).Equals(Const.SRV_CMD_OPPONENT))
        {

        }
        else { 
            Const.ServerCMD.AddLast((String)evt.Params[Const.PARAM_CMD]);
        }
    }

    private void onRoomRemoved(BaseEvent evt){
		Debug.Log("removed room");
	}

    private void OnLogin(BaseEvent evt)
    {
        Debug.Log("OnLogin");
        //if Login is successfully finished
        Const.sfs_state = Const.CONNECT_STATE.CREATE_ROOM;
    }

    private void OnLoginError(BaseEvent evt)
    {
        Const.sfs_state = Const.CONNECT_STATE.ERROOR_LOGIN;
        Debug.Log("OnLoginError: " + evt.ToString());
    }

        //----------------------------------------------------------
        // SmartFoxServer log event listeners
        //----------------------------------------------------------

        public void OnInfoMessage(BaseEvent evt) {
		string message = (string)evt.Params["message"];
		ShowLogMessage("INFO", message);
	}
	
	public void OnWarnMessage(BaseEvent evt) {
		string message = (string)evt.Params["message"];
		ShowLogMessage("WARN", message);
	}
	
	public void OnErrorMessage(BaseEvent evt) {
		string message = (string)evt.Params["message"];
		ShowLogMessage("ERROR", message);
	}
	
	public void ShowLogMessage(string level, string message) {
		message = "[SFS > " + level + "] " + message;
		//trace(message);
		Debug.Log(message);
	}
}
