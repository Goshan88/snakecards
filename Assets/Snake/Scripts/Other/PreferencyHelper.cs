﻿using System;
using UnityEngine;

public static class PreferencyHelper
{
   static  int numb_card = 0;

    public static int getCntSnake(){
		return PlayerPrefs.GetInt(Const.PRF_CNT_SNAKE, Const.DEF_CNT_SNAKE);
	}
	
	public static void setCntSnake(int _cnt){
		PlayerPrefs.SetInt(Const.PRF_CNT_SNAKE, _cnt);
	}

    public static int getActiveSnake()
    {
        return PlayerPrefs.GetInt(Const.PRF_ACTIVE_SNAKE, Const.DEF_ACTIVE_SNAKE);
    }

    public static void setActiveSnake(int _cnt)
    {
        PlayerPrefs.SetInt(Const.PRF_ACTIVE_SNAKE, _cnt);
    }

    public static string getNameSnake(int _id){
		string con_str = Const.PRF_NAME_SNAKE + _id.ToString();
		return PlayerPrefs.GetString(con_str, Const.DEF_NAME_SNAKE);
	}
	
	public static void setNameSnake(int _id, string _name){
		string con_str = Const.PRF_NAME_SNAKE + _id.ToString();
		PlayerPrefs.SetString(con_str, _name);
	}
	
	public static int getField(int _id_snake, int _id_field){
		string con_str = Const.PRF_ID_FIELD + _id_snake.ToString()+ _id_field.ToString();
		return PlayerPrefs.GetInt(con_str, Const.DEF_ID_FIELD);
	}
	
	public static void setField(int _id_snake, int _id_field, int _value){
		string con_str = Const.PRF_ID_FIELD + _id_snake.ToString() + _id_field.ToString();
		PlayerPrefs.SetInt(con_str, _value);
	}
	
	public static string getIdSnakeDataBase(int _id){
		string con_str = Const.PRF_ID_SNAKE_DB + _id.ToString();
		return PlayerPrefs.GetString(con_str, Const.DEF_ID_SNAKE_DB);
	}
	
	public static void setIdSnakeDataBase(int _id, string _val){
		string con_str = Const.PRF_ID_SNAKE_DB + _id.ToString();
		PlayerPrefs.SetString(con_str, _val);
	}
	
	public static string getIdUserDataBase(){
	    string con_str = Const.PRF_ID_USER_DB;
		return PlayerPrefs.GetString(con_str, Const.DEF_ID_USER_DB);
	}
	
	public static void setIdUserDataBase(string _val){
		string con_str = Const.PRF_ID_USER_DB;
		PlayerPrefs.SetString(con_str, _val);
	}
	
	public static string getNameUser(){
		string con_str = Const.PRF_NAME_USER;
		return PlayerPrefs.GetString(con_str, Const.DEF_NAME_USER);
	}
	
	public static void setNameUser(string _name){
		string con_str = Const.PRF_NAME_USER;
		PlayerPrefs.SetString(con_str, _name);
	}

    public static int getUsedCard(int snake_id)
    {
        string con_str = Const.PRF_USED_CARD + snake_id.ToString();
        return PlayerPrefs.GetInt(con_str, Const.DEF_USED_CARD);
    }

    public static void setUsedCard(int snake_id, int _value)
    {
        string con_str = Const.PRF_USED_CARD + snake_id.ToString();
        PlayerPrefs.SetInt(con_str, _value);
    }

    public static int getRaiting(int snake_id)
    {
        string con_str = Const.PRF_RATING + snake_id.ToString();
        return PlayerPrefs.GetInt(con_str, Const.DEF_RATING);
    }

    public static void setRaiting(int snake_id, int _value)
    {
        string con_str = Const.PRF_RATING + snake_id.ToString();
        PlayerPrefs.SetInt(con_str, _value);
    }

    public static int[] getCardNumb(int _id_card)//return begin and end numb in id card [0:8]
    {
        int tmp = (Const.CardSize * Const.CardSize);
        int begin = _id_card * tmp;
        int end = begin + tmp;
        if (_id_card == 0)  end -= 1;
        int[] ar_tmp = {begin,end};
        return ar_tmp;
    }

    public static int[] getCardValue(int _id_snake,int _id_card)//return array field [0;48] in id snake[1...] id card [0;8]
    {
        int size = (Const.CardSize * Const.CardSize);
        int[] field = new int[size];
        int[] arr = getCardNumb(_id_card);
        //Debug.Log("getCardValue: "+ _id_snake.ToString() + arr[0].ToString());
        for (int i = 0; i < size; i++) { field[i] = getField(_id_snake, arr[0]++); }
        return field;
    }

    public static void setCardValue(int _id_snake, int _id_card, int[] value)//return array field [0;48] in id snake[1...] id card [0;8]
    {
        int size = (Const.CardSize * Const.CardSize);
        if (value.Length < size)
        {
            Debug.Log("Eror PreferencyHelper setCardVakue value.Length < size");
            return;
        }
        int[] arr = getCardNumb(_id_card);
        //Debug.Log("setCardValue: " + _id_snake.ToString() + arr[0].ToString());
        for (int i = 0; i < size; i++) {setField(_id_snake, arr[0]++, value[i]); }
    }

    public static int[] getPackValue(int _id_snake)//return array field [0;9*48] in id snake[1...]
    {
        int size = (Const.CardSize * Const.CardSize)*Const.PackSize;
        int[] field = new int[size];
        int cnt = 0;
        //Debug.Log("getPack size: " + size.ToString());
        for (int i = 0; i < size; i++) { field[i] = getField(_id_snake, cnt++); }
        return field;
    }

    public static void setPackArray(int _id_snake, int[] arr)//return array field [0;9*48] in id snake[1...]
    {
        for (int i = 0; i < arr.Length; i++) { setField(_id_snake, i, arr[i]); }
    }

    public static string getLang()
    {
        return PlayerPrefs.GetString(Const.PRF_LANG, Const.DEF_LANG);
    }

    public static void saveLang(string _lang)
    {
        PlayerPrefs.SetString(Const.PRF_LANG, _lang);
    }

    public static string getMail()
    {
        return PlayerPrefs.GetString(Const.PRF_MAIL, Const.DEF_MAIL);
    }

    public static void saveMail(string _mail)
    {
        PlayerPrefs.SetString(Const.PRF_MAIL, _mail);
    }

    //Time functions
    public static int[] getArrFromDate(System.DateTime _date)
    {
        int[] tmp = new int[Const.SIZE_ARR_DATE];
        tmp[0] = _date.Second;
        tmp[1] = _date.Minute;
        tmp[2] = _date.Hour;
        tmp[3] = _date.Day;
        tmp[4] = _date.Month;
        tmp[5] = _date.Year;
        return tmp;
    }

    public static void saveInt(string _name, int _cnt, int _value)
    {
        string con_str = _name + _cnt.ToString();
        PlayerPrefs.SetInt(con_str, _value);
    }

    public static int loadInt(string _name, int _cnt)
    {
        string con_str = _name + _cnt.ToString();
        return PlayerPrefs.GetInt(con_str, Const.PRF_DT_DEF);
    }

    //save time change
    public static void saveChangeDT(String _name)//return array field [0;9*48] in id snake[1...]
    {
        int[] arr_int = getArrFromDate(System.DateTime.Now);
        for (int i = 0; i < arr_int.Length; i++) saveInt(_name,i,arr_int[i]);
    }

    //get time change
    public static int[] loadChangeDT(String _name)//return array field [0;9*48] in id snake[1...]
    {
        int[] arr_int = getArrFromDate(System.DateTime.Now);
        for (int i = 0; i < arr_int.Length; i++) arr_int[i] = loadInt(_name, i);
        return arr_int;
    }

    public static string getChangeDT(String _name)//return array field [0;9*48] in id snake[1...]
    {
        String arr_str = "";
        int[] arr_int = getArrFromDate(System.DateTime.Now);
        for (int i = 0; i < arr_int.Length; i++)
        {
            arr_int[i] = loadInt(_name, i);
            arr_str += arr_int[i].ToString();
        }
        return arr_str;
    }

    //save time change
    public static bool getUserTrening()
    {
        int res = PlayerPrefs.GetInt(Const.PRF_TRENING, Const.PRF_TRENING_DEF);
        if(res== Const.PRF_TRENING_DEF)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public static void setUserTrening(bool val)//return array field [0;9*48] in id snake[1...]
    {
        if (val)
        {
            PlayerPrefs.SetInt(Const.PRF_TRENING, Const.PRF_TRENING_OK);
        }
        else
        {
            PlayerPrefs.SetInt(Const.PRF_TRENING, Const.PRF_TRENING_DEF);
        }
    }
    /*
    //get time change
    public static int[] loadChangeDT(String _name)//return array field [0;9*48] in id snake[1...]
    {
        int[] arr_int = getArrFromDate(System.DateTime.Now);
        for (int i = 0; i < arr_int.Length; i++) arr_int[i] = loadInt(_name, i);
        return arr_int;
    }*/
}
