﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneScreneHelper : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void pullScene(GameObject _object)
    {
        int screen_width = Screen.width;
        int screen_height = Screen.height;
        _object.transform.localScale = new Vector3(screen_width, screen_height, Const.Z_SCALE_IMAGE_SCREEN);
        _object.transform.localPosition = new Vector3(0, 0, Const.Z_COORD_IMAGE_SCREEN);
    }

    public static void setTransperent(GameObject _object, float _value, Color _color)
    {
        var renderer = _object.GetComponent<Renderer>();
        renderer.material.shader = Shader.Find("Transparent/Diffuse");
        //renderer.material.color = _color * _value;
        renderer.material.SetColor("_Color",_color);
    }
}
