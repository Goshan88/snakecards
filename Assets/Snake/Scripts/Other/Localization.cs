﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SimpleLocalization;

public class Localization : MonoBehaviour {

	// Use this for initialization
	void Start () {
        LocalizationManager.Read();
        string Language = PreferencyHelper.getLang();
        //Debug.Log("Lang. load " + Language);
        if (Language == Const.DEF_LANG)
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.English:
                    LocalizationManager.Language = "English";
                    break;
                case SystemLanguage.Russian:
                    LocalizationManager.Language = "Russian";
                    break;
                default:
                    LocalizationManager.Language = "English";
                    break;
            }
            //Debug.Log("Lang. save " + LocalizationManager.Language);
            PreferencyHelper.saveLang(LocalizationManager.Language);
        }
        else
        {
            LocalizationManager.Language = Language;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
