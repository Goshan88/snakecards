using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class EditTextControl : MonoBehaviour
{
   public Button Ok, Clean, Close;
   public Text TeachText;
   public GameObject teach_panel;
   private float _nextEditTextUpdate;

    void Start()
    {
        offBorder(Ok.gameObject);
        offBorder(teach_panel);
        //check teach mode
        if (!PreferencyHelper.getUserTrening())
        {
            teach_panel.SetActive(true);
            TeachText.gameObject.SetActive(true);
            InreactAllBut(false);
            Const.StateTeachOut = Const.TeachState.ClickCard_6;
        }
        else
        {
            teach_panel.SetActive(false);
            TeachText.gameObject.SetActive(false);
            InreactAllBut(true);
        }
    }

    void Update()
    {
        if (_nextEditTextUpdate > Time.time)
        {
            return;
        }

        _nextEditTextUpdate = Time.time + Const.game_update;

        if (!PreferencyHelper.getUserTrening())
        {

            if (Const.StateTeach != Const.StateTeachOut)
            {
                Const.StateTeachOut = Const.StateTeach;
                ShowTeach();
            }
            showBorder();
        }

    }

    private void showBorder()
    {
        switch (Const.StateTeach)
        {
            case Const.TeachState.ClickCard_7:
            case Const.TeachState.ClickCard_10:
            case Const.TeachState.ClickCard_14:
            case Const.TeachState.ClickCard_15:
                if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                {
                    Const.CntUpdateBorder = 0;
                    toggleBorder(teach_panel);
                }
                break;
            case Const.TeachState.ClickCard_8:
                break;
            case Const.TeachState.ClickCard_9:
                break;
            case Const.TeachState.ClickCard_18:
            case Const.TeachState.ClickCard_11:
                if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                {
                    Const.CntUpdateBorder = 0;
                    toggleBorder(Ok.gameObject);
                }
                break;
            case Const.TeachState.ClickCard_16:
                
                break;
            case Const.TeachState.ClickCard_17:
                break;

            default:
                break;
        }
    }

    

        private void ShowTeach()
    {
        switch (Const.StateTeachOut)
        {
            case Const.TeachState.ClickCard_7:
                TeachText.text = LocalizationManager.Localize("teach_8");
                break;
            case Const.TeachState.ClickCard_8:
                TeachText.text = LocalizationManager.Localize("teach_9");
                offBorder(teach_panel);
                offBorder(Ok.gameObject);
                break;
            case Const.TeachState.ClickCard_9:
                TeachText.text = LocalizationManager.Localize("teach_10");
                break;
            case Const.TeachState.ClickCard_10:
                TeachText.text = LocalizationManager.Localize("teach_11");
                break;
            case Const.TeachState.ClickCard_11:
                TeachText.text = LocalizationManager.Localize("teach_12");
                Ok.interactable = true;
                break;
            case Const.TeachState.ClickCard_14:
                TeachText.text = LocalizationManager.Localize("teach_15");
                break;
            case Const.TeachState.ClickCard_15:
                TeachText.text = LocalizationManager.Localize("teach_16");
                break;
            case Const.TeachState.ClickCard_16:
                TeachText.text = LocalizationManager.Localize("teach_17");
                offBorder(teach_panel);
                offBorder(Ok.gameObject);
                break;
            case Const.TeachState.ClickCard_17:
                TeachText.text = LocalizationManager.Localize("teach_18");
                break;
            case Const.TeachState.ClickCard_18:
                TeachText.text = LocalizationManager.Localize("teach_19");
                Ok.interactable = true;
                break;
            default:
                break;
        }
    }


    private void InreactAllBut(bool val)
    {
        Ok.interactable = val;
        Clean.interactable = val;
        Close.interactable = val;
    }

    private void toggleBorder(GameObject _object)
    {
        Outline[] outLine;
        outLine = _object.GetComponentsInChildren<Outline>();
        foreach (Outline o in outLine)
        {
            if (o.isActiveAndEnabled)
            {
                o.enabled = false;
                Const.BorderState = false;
            }
            else
            {
                o.enabled = true;
                Const.BorderState = true;
            }
        }
    }


    private void offBorder(GameObject _object)
    {
        Outline[] outLine;
        outLine = _object.GetComponentsInChildren<Outline>();
        foreach (Outline o in outLine)
        {
            o.enabled = false;
            Const.BorderState = false;
        }
    }
}
