using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class MainScTouch : MonoBehaviour {
	float _nextCardsUpdate = Const.TimeBegin;
    static Coords Touch = new Coords();
    private Vector2 startPos, touch_direction;
    float coord_x = 0f, coord_y = 0f;
    public Text Snake_Name;
    private float _nextTouchUpdate = 0;
    private float _nextMessageTime = 0;
    public Text Editor_Status, Connect_Status;
    private bool Settingpanel = false;


    void Start () {
        
    }

    private static bool initShowCards = false;
    void Update() {
        //Vector2 test = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                _nextTouchUpdate = Time.time + Const.TIME_LONG_CLICK;
                if (Const.panel != Const.Type_panel.NOTHING) Settingpanel = true;
            }

            //Debug.Log("Touch: " + touch.position.x.ToString() + "x" + touch.position.y.ToString());
            if (touch.phase == TouchPhase.Ended) { 
                var pos = Camera.main.ScreenToWorldPoint(touch.position);
                //Debug.Log("pos " + pos.x.ToString() + "x" + pos.y.ToString() + "x" + pos.z.ToString());
                Const.touch_pos.x = pos.x;
                Const.touch_pos.y = pos.y;
                //Debug.Log("Const.touch_pos " + Const.touch_pos.x.ToString() + "x" + Const.touch_pos.y.ToString());
                //check if open setting panel than touch no
                
                if(!Settingpanel)
                {
                    Const.touch_pos.processed = false;
                }
                else
                {
                    Settingpanel = false;
                }
                Const.long_click = (Time.time > _nextTouchUpdate) ? true : false;
                Debug.Log("Long click: " + Const.long_click.ToString());
            }
        }

        //code for editor card scene
        if(Editor_Status != null)
        {
            if (Const.delete_head)
            {
                Editor_Status.text = LocalizationManager.Localize("DeleteHead");
                _nextMessageTime = Time.time + Const.TIME_SHOW_MESSAGE;
                Const.delete_head = false;
                Const.show_wrn_msg = true;
                Const.color_font = Editor_Status.color;
                Editor_Status.color = Color.red;
            }
            else
            {
                if (Const.show_wrn_msg)
                {
                    if(Time.time > _nextMessageTime)
                    {
                        Const.show_wrn_msg = false;
                        Editor_Status.color= Const.color_font;
                    }
                }
                else
                {
                    Editor_Status.text = LocalizationManager.Localize(Const.change_state.ToString());
                }
            }
        }
        if (Connect_Status != null)
        {
            if (Connect_Status.text.Equals(Const.sfs_state.ToString()))
            {

            }
            else
            {
                Connect_Status.text = Const.sfs_state.ToString();
            }
        }
    }

    //UI button processing
    public void click_edit_OK()
    {
        Debug.Log("Click on button OK on EditCard scene");
        Const.EDIT_SAVE_CARD = true;
    }

    public void click_edit_Cancel()
    {
        Debug.Log("Click on button Cancel on EditCard scene");
        Const.EDIT_CANCEL_CARD = true;  
    }

    public void clear_edit()
    {
        Debug.Log("Click on button Clear");
        Const.clear_edit_card = true;
    }

    public void click_battle()
    {
        Debug.Log("Click on button Battle");
        Const.battle_type = Const.BATTLE_TYPE.BATTLE;
        Const.initGame = false;
        Const.cnt_move = Const.max_move;
        Const.finishGame = false;
        Const.beginGame = true;
        Const.game_step_mode = true;
    }

    public void click_debug()
    {
        Debug.Log("Click on button Debug");
        Const.battle_type = Const.BATTLE_TYPE.DEBUG;
        Const.cnt_move = Const.max_move;
        Const.finishGame = false;
        Const.game_step_mode = true;
    }

    public void game_play()
    {
        Debug.Log("Click on button game play");
        Const.click_but = true;
        Const.game_step_mode = false;
        Const.finishGame = false;
        if (Const.cnt_move > Const.min_move) Const.playGame = true;
    }

    public void game_next_step()
    {
       // Debug.Log("Click on button next step");
        Const.click_but = true;
        Const.game_step_mode = true;
        Const.game_next_step = true;
        if (Const.cnt_move > Const.min_move) Const.playGame = true;
    }

    public void game_exite()
    {
        Debug.Log("Click on button exite game");
        Const.initShowCards = false;
        Const.touch_pos.processed = true;
        Const.battle_type = Const.BATTLE_TYPE.NOTHING;
        SceneManager.LoadScene("Main");
        Const.need_new_opponent = true;
        Const.sfs_state = Const.CONNECT_STATE.NOTHING;
    }

    public void loadKeyBoard()
    {
        Debug.Log("loadKeyBoard");
       //string st = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
    }

    public void setLabelNameSnake(string _str)
    {
        Snake_Name.text = _str;
    }

    public void loadScores()
    {
        //SceneManager.LoadScene("Scores");
        Const.View_panel = true;
        Debug.Log("Click on Score");
        Const.panel = Const.Type_panel.SCORE;
    }

    public void clickSnakeName()
    {
        Const.View_panel = true;
        Debug.Log("Click on SnakeName");
        Const.panel = Const.Type_panel.NAME;
    }

    public void clickSetting()
    {
        Const.View_panel = true;
        Debug.Log("Click on Setting");
        Const.panel = Const.Type_panel.SETTINGS;
        //Const.send_request = true;
     }

    public void closepanel()
    {
        Const.Close_panel = true;
        Debug.Log("Click on Closepanel");
        Const.panel = Const.Type_panel.NOTHING;
    }

    public void enterpanel()
    {
        Const.Enter_panel = true;
        Debug.Log("Click on Enterpanel");
    }

    public void choiseRussia()
    {
        Const.Close_panel = true;
        Debug.Log("Click on Russia");
        LocalizationManager.Language = "Russian";
        PreferencyHelper.saveLang(LocalizationManager.Language);
    }

    public void choiseEnglish()
    {
        Const.Close_panel = true;
        Debug.Log("Click on English");
        LocalizationManager.Language = "English";
        PreferencyHelper.saveLang(LocalizationManager.Language);
    }

    public void changeMail()
    {
        Const.View_panel = true;
        Debug.Log("Click change Mail");
        Const.panel = Const.Type_panel.EMAIL;
    }
}