﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
    public Camera _MainCamera;
    float orthographicSize = 0;
    bool init = false;
    // Use this for initialization
    void Start () {
        orthographicSize = Screen.height / 2;
        
        //MainCamera. = orthographicSize;
    }
	
	// Update is called once per frame
	void Update () {
        if (!init)
        {
             init = true;
            _MainCamera.enabled = true; 
            _MainCamera.orthographicSize = orthographicSize;
        }
    }
}
