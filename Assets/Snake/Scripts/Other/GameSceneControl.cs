﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;
using UnityEngine.SceneManagement;

public class GameSceneControl : MonoBehaviour {
    public ProgressBar Moves;
    public ProgressBar User, Opponent;
    public GameObject SceneImage;//close scene wen game end
    public Text GameStatus, vs;
    public Text UserName,UserScore,UserWinner;
    public Text OpponentName, OpponentScore, OpponentWinner;
    public Button Play, Step, Exit;
    private float[] Z_coord_but= new float[3];

    public GameObject teach_panel;
    public Text teach_text;

    private static string st_User, st_Opponent;
    private static float cnt_moves=0, user_health = 0, opponent_health = 0;

    float _nextGameUpdate = Const.TimeBegin;
    float _nextGameOver = Const.TimeBegin;

    
    private int cnt_body_user = 0, cnt_body_opponent = 0;

    // Use this for initialization
    void Start () {
        Moves.BarValue = 25;
        User.BarValue = 50;
        Opponent.BarValue = 75;

        st_User = User.Title;
        st_Opponent = Opponent.Title;

        cnt_moves = Moves.BarValue;
        user_health = User.BarValue;
        opponent_health = Opponent.BarValue;

        Moves.Title = LocalizationManager.Localize("Steps");
        Moves.BarValue = Moves.BarValue;

        SceneScreneHelper.pullScene(SceneImage);
        //SceneScreneHelper.setTransperent(SceneImage,0.8f, Color.black);
        
        //if only one game player
        if (Const.battle_type == Const.BATTLE_TYPE.DEBUG)
        {
            Opponent.gameObject.SetActive(false);
            User.gameObject.SetActive(false);
            showUsedCard(false);
            Const.beginGame = false;
        }
        Const.cnt_scene = Const.GameOverScene.NOTHING;
        Const.finishGame = false;
        hideFinishScreen();

        //save coord button
        saveZCoord();
        Const.cnt_welcome = Const.WelcomeScene.NOTHING;

        if (!PreferencyHelper.getUserTrening())
        {
            Const.StateTeachOut = Const.TeachState.NOTHING;
            teach_panel.SetActive(true);
            teach_text.gameObject.SetActive(true);
            Const.StateTeach = Const.TeachState.ClickCard_21;//just for debug
        }
        else
        {
            teach_panel.SetActive(false);
            teach_text.gameObject.SetActive(false);
        }

        offBorder(Play.gameObject);
        offBorder(Step.gameObject);
    }
	
    private void saveZCoord()
    {
        Z_coord_but[0] = Play.gameObject.transform.localPosition.z;
        Z_coord_but[1] = Step.gameObject.transform.localPosition.z;
        Z_coord_but[2] = Exit.gameObject.transform.localPosition.z;
    }

    private void loadZCoord()
    {
        Play.gameObject.transform.localPosition = new Vector3(Play.gameObject.transform.localPosition.x, Play.gameObject.transform.localPosition.y, Z_coord_but[0]);
        Step.gameObject.transform.localPosition = new Vector3(Step.gameObject.transform.localPosition.x, Step.gameObject.transform.localPosition.y, Z_coord_but[1]);
        Exit.gameObject.transform.localPosition = new Vector3(Exit.gameObject.transform.localPosition.x, Exit.gameObject.transform.localPosition.y, Z_coord_but[2]);
    }

    private void setZCoord(float _Zcoord)
    {
        Play.gameObject.transform.localPosition = new Vector3(Play.gameObject.transform.localPosition.x, Play.gameObject.transform.localPosition.y, _Zcoord);
        Step.gameObject.transform.localPosition = new Vector3(Step.gameObject.transform.localPosition.x, Step.gameObject.transform.localPosition.y, _Zcoord);
        Exit.gameObject.transform.localPosition = new Vector3(Exit.gameObject.transform.localPosition.x, Exit.gameObject.transform.localPosition.y, _Zcoord);
    }

    private void setZ_Exit(float _Zcoord)
    {
        Exit.gameObject.transform.localPosition = new Vector3(Exit.gameObject.transform.localPosition.x, Exit.gameObject.transform.localPosition.y, _Zcoord);
    }

        private void hideFinishScreen()
    {
        SceneImage.gameObject.SetActive(false);
        GameStatus.gameObject.SetActive(false);

        UserName.gameObject.SetActive(false);
        UserScore.gameObject.SetActive(false);
        UserWinner.gameObject.SetActive(false);

        OpponentName.gameObject.SetActive(false);
        OpponentScore.gameObject.SetActive(false);
        OpponentWinner.gameObject.SetActive(false);

        vs.gameObject.SetActive(false);
    }

    private void showUsedCard(bool _show)
    {
        for (int i = 0; i < Const.User_usedCard.Count; i++)
        {
            Const.User_usedCard[i].Object.SetActive(_show);
        }
        for (int i = 0; i < Const.Opponent_usedCard.Count; i++)
        {
            Const.Opponent_usedCard[i].Object.SetActive(_show);
        }
    }

    private void viewFinishScene()
    {
        switch (Const.cnt_scene)
        {
            case Const.GameOverScene.NOTHING:
                Const.cnt_dark = Const.DARK_BEGIN;
                //SceneScreneHelper.setTransperent(SceneImage, Const.cnt_dark, Const.BACKGROUND_COLOR);
                SceneImage.gameObject.SetActive(true);
                GameStatus.text = LocalizationManager.Localize("GameOver");
                GameStatus.gameObject.SetActive(true);
                setZ_Exit(Const.Z_COORD_BUTTON);
                Const.cnt_scene = Const.GameOverScene.DARK_SCENE;
                Const.cnt_welcome = Const.WelcomeScene.NOTHING;

                if (Const.battle_type == Const.BATTLE_TYPE.BATTLE)
                {
                    Const.opponent_id = PreferencyHelper.getIdSnakeDataBase(Const.OPPONENT_ID);
                    Const.need_new_opponent = true;
                    Const.need_send_res_bat = true;
                    int tmp = PreferencyHelper.getRaiting(Const.USER_ID);
                    tmp += Const.user_result;
                    PreferencyHelper.setRaiting(Const.USER_ID,tmp);
                    //Const.ServerCMD.AddLast(Const.SRV_CMD_RESULT);
                }
                break;
            case Const.GameOverScene.DARK_SCENE:
                if (Const.cnt_dark< Const.DARK_END)
                {
                    Const.cnt_dark += Const.DARK_DELTA;
                    //SceneScreneHelper.setTransperent(SceneImage, Const.cnt_dark, Const.BACKGROUND_COLOR);
                }
                else
                {
                    Const.cnt_scene = Const.GameOverScene.VIEW_STATUS;
                    _nextGameOver = Time.time + Const.TIME_STATUS_TEXT;
                }
                break;
            case Const.GameOverScene.VIEW_STATUS:
                if (_nextGameOver < Time.time)
                {
                    _nextGameOver = Time.time + Const.TIME_NAME_VIEW;

                    if (Const.battle_type == Const.BATTLE_TYPE.DEBUG) {
                        Const.cnt_scene = Const.GameOverScene.VIEW_OK_BUTTON;
                    } else {
                        GameStatus.gameObject.SetActive(false);
                        Const.cnt_scene = Const.GameOverScene.VIEW_NAME;
                    }
                }
                break;
            case Const.GameOverScene.VIEW_NAME:
                UserName.gameObject.SetActive(true);
                OpponentName.gameObject.SetActive(true);
                if (_nextGameOver < Time.time)
                {
                    Const.cnt_scene = Const.GameOverScene.VIEW_SCORE;
                    _nextGameOver = Time.time + Const.TIME_EMTY_SCORE_VIEW;
                }
                break;
            case Const.GameOverScene.VIEW_SCORE:
                UserScore.gameObject.SetActive(true);
                UserScore.text = "0";
                OpponentScore.gameObject.SetActive(true);
                OpponentScore.text = "0";
                if (_nextGameOver < Time.time)
                {
                    Const.cnt_scene = Const.GameOverScene.VIEW_SCORE_RES;
                    cnt_body_user = 0;
                    cnt_body_opponent = 0;
                    _nextGameOver = Time.time + Const.TIME_DELTA_SCORE_VIEW;
                }
                 break;
            case Const.GameOverScene.VIEW_SCORE_RES:
                if (_nextGameOver < Time.time)
                {
                    //Debug.Log("Const.user_result: " + Const.user_result.ToString() + " Const.opponent_result:" + Const.opponent_result.ToString());
                    if (cnt_body_user < Const.user_result)
                    {
                        cnt_body_user++;
                        UserScore.text = cnt_body_user.ToString();
                    }
                    if (cnt_body_opponent < Const.opponent_result)
                    {
                        cnt_body_opponent++;
                        OpponentScore.text = cnt_body_opponent.ToString();
                    }
                    _nextGameOver = Time.time + Const.TIME_DELTA_SCORE_VIEW;
                    if ((cnt_body_user >= Const.user_result) && (cnt_body_opponent >= Const.opponent_result))
                    {
                        _nextGameOver = Time.time + Const.TIME_RESULT_VIEW;
                        Const.cnt_scene = Const.GameOverScene.VIEW_RESULT;
                    }
                }
                break;
            case Const.GameOverScene.VIEW_RESULT:
                if (_nextGameOver < Time.time)
                {
                    if(Const.user_result== Const.opponent_result)
                    {
                        GameStatus.text = LocalizationManager.Localize("Draw");
                        GameStatus.gameObject.SetActive(true);
                        Const.cnt_scene = Const.GameOverScene.VIEW_OK_BUTTON;
                        break;
                    }
                    if (Const.user_result > Const.opponent_result)
                    {
                        UserWinner.gameObject.SetActive(true);
                    }
                    else
                    {
                        OpponentWinner.gameObject.SetActive(true);
                    }
                    Const.cnt_scene = Const.GameOverScene.VIEW_OK_BUTTON;
                }
                break;

            case Const.GameOverScene.VIEW_OK_BUTTON:
                setZ_Exit(Const.Z_COORD_BUTTON);
                Const.cnt_scene = Const.GameOverScene.WAIT_PRESS_OK;
                break;
            case Const.GameOverScene.WAIT_PRESS_OK:
                //auto exite
                if (Const.cnt_auto_exite++>Const.MAX_AUTO_EXITE)
                {
                    Const.cnt_auto_exite = 0;
                    Const.initShowCards = false;
                    Const.touch_pos.processed = true;
                    Const.battle_type = Const.BATTLE_TYPE.NOTHING;
                    SceneManager.LoadScene("Main");
                }
                break;
            default:
                break;
        }

    }


    public static void setNameUser(string _str)
    {
        st_User = _str;
    }

    public static void setNameOpponent(string _str)
    {
        st_Opponent = _str;
    }

    public static void setUserProgr(float _val)
    {
        if((_val<Const.MIN_PROGRESS)|| (_val > Const.MAX_PROGRESS))
        {
            Debug.Log("Error set progress bar val:" + _val.ToString());
        }
        user_health = _val;
    }

    public static void setOpponentProgr(float _val)
    {
        if ((_val < Const.MIN_PROGRESS) || (_val > Const.MAX_PROGRESS))
        {
            Debug.Log("Error set progress bar val:" + _val.ToString());
        }
        opponent_health = _val;
    }

    public static void setMovesProgr(float _val)
    {
        if ((_val < Const.MIN_PROGRESS) || (_val > Const.MAX_PROGRESS))
        {
            Debug.Log("Error set progress bar val:" + _val.ToString());
        }
        cnt_moves = _val;
    }

    private void viewBeginScene()
    {
        switch (Const.cnt_welcome) {

            case Const.WelcomeScene.NOTHING:
                vs.gameObject.SetActive(true);
                SceneImage.gameObject.SetActive(true);
                //SceneScreneHelper.setTransperent(SceneImage, Const.DARK_WELCOME, Const.BACKGROUND_COLOR);
                UserScore.gameObject.SetActive(true);
                UserName.gameObject.SetActive(true);
                UserScore.text = PreferencyHelper.getRaiting(Const.USER_ID).ToString();
                OpponentScore.gameObject.SetActive(true);
                OpponentName.gameObject.SetActive(true);
                OpponentScore.text = PreferencyHelper.getRaiting(Const.OPPONENT_ID).ToString();
                setZCoord(Const.Z_COORD_BUTTON);
                Const.cnt_welcome = Const.WelcomeScene.WELCOME_SCENE;
                showUsedCard(true);
                GameStatus.gameObject.SetActive(true);
                GameStatus.text = LocalizationManager.Localize("START");
                break;
            case Const.WelcomeScene.WELCOME_SCENE:
                Const.click_but = false;
                Const.cnt_welcome = Const.WelcomeScene.WAIT_PRESS_BUT;
               /* Const.Game_Fields[84].Object.transform.localPosition = new Vector3(Const.Game_Fields[84].Object.transform.localPosition.x,
                    Const.Game_Fields[84].Object.transform.localPosition.y, Const.Z_COORD_BUTTON);*/
                break;
            case Const.WelcomeScene.WAIT_PRESS_BUT:
                if (Const.click_but)
                {
                    Const.cnt_welcome = Const.WelcomeScene.WAIT_PRESS_BUT;
                    Const.beginGame = false;
                    vs.gameObject.SetActive(false);
                    SceneImage.gameObject.SetActive(false);
                    UserScore.gameObject.SetActive(false);
                    UserName.gameObject.SetActive(false);
                    OpponentScore.gameObject.SetActive(false);
                    OpponentName.gameObject.SetActive(false);
                    GameStatus.gameObject.SetActive(false);
                    loadZCoord();
                    showUsedCard(false);
                    Const.need_new_opponent = true;
                    Const.sfs_state = Const.CONNECT_STATE.NOTHING;
                }
                break;
            default:
                break;
        }
        //setZ_Exit(Const.Z_COORD_BUTTON);
    }


    // Update is called once per frame
    void Update()
    {

        if ((_nextGameUpdate > Time.time) || (!Const.initGame))
        {
            return;
        }
        else
        {
            _nextGameUpdate = Time.time + Const.game_update;


            if (!Equals(st_Opponent, Opponent.Title))
            {
                OpponentName.text = st_Opponent;
                Opponent.Title = st_Opponent;
                Opponent.BarValue = Opponent.BarValue;
            }
            if (!Equals(st_User, User.Title))
            {
                UserName.text = st_User;
                User.Title = st_User;
                User.BarValue = User.BarValue;
            }
            if (cnt_moves != Moves.BarValue)
            {
                Moves.BarValue = cnt_moves;
            }
            if (user_health != User.BarValue)
            {
                User.BarValue = user_health;
            }
            if (opponent_health != Opponent.BarValue)
            {
                Opponent.BarValue = opponent_health;
            }
            if (Const.finishGame)
            {
                viewFinishScene();
            }
            if (Const.beginGame)
            {
                viewBeginScene();
            }

            if (!PreferencyHelper.getUserTrening())
            {
                if (Const.StateTeach != Const.StateTeachOut)
                {
                    Const.StateTeachOut = Const.StateTeach;
                    ShowTeach();
                }
                showBorder();
                if (!Const.touch_pos.processed)
                {
                    Const.touch_pos.processed = true;
                    checkTouch();
                }
                checkStep();
            }
        }
    }

    private void checkStep()
    {
        switch (Const.StateTeach)
        {
            case Const.TeachState.ClickCard_23:
                if ((Const.max_move- Const.cnt_move)>=Const.teach_first_step) {
                    Const.StateTeach = Const.TeachState.ClickCard_24;
                }
                break;
            case Const.TeachState.ClickCard_25:
                if ((Const.max_move - Const.cnt_move) >= Const.teach_second_step)
                {
                    Const.StateTeach = Const.TeachState.ClickCard_26;
                }
                break;
            case Const.TeachState.ClickCard_30:
                if ((Const.max_move - Const.cnt_move) >= Const.teach_third_step)
                {
                    //teach finish
                    offBorder(Step.gameObject);
                    offBorder(Play.gameObject);
                    Step.interactable = true;
                    Exit.interactable = true;
                    teach_panel.SetActive(false);
                    PreferencyHelper.setUserTrening(true);
                }
                break;
            default:
                break;
        }
    }

        private void checkTouch()
    {
        switch (Const.StateTeachOut)
        {
            case Const.TeachState.ClickCard_21:
                Const.StateTeach = Const.TeachState.ClickCard_22;
                break;
            case Const.TeachState.ClickCard_22:
                Const.StateTeach = Const.TeachState.ClickCard_23;
                break;
            case Const.TeachState.ClickCard_24:
                Const.StateTeach = Const.TeachState.ClickCard_25;
                break;
            case Const.TeachState.ClickCard_26:
                Const.StateTeach = Const.TeachState.ClickCard_27;
                break;
            case Const.TeachState.ClickCard_27:
                Const.StateTeach = Const.TeachState.ClickCard_28;
                break;
            case Const.TeachState.ClickCard_28:
                Const.StateTeach = Const.TeachState.ClickCard_29;
                break;
            case Const.TeachState.ClickCard_29:
                Const.StateTeach = Const.TeachState.ClickCard_30;
                break;
            default:
                break;
        }
    }

    private void ShowTeach()
    {
        switch (Const.StateTeachOut)
        {
            case Const.TeachState.ClickCard_21:
                teach_text.text = LocalizationManager.Localize("teach_22");
                Step.interactable = false;
                Play.interactable = false;
                Exit.interactable = false;
                break;
            case Const.TeachState.ClickCard_22:
                teach_text.text = LocalizationManager.Localize("teach_23");
                break;
            case Const.TeachState.ClickCard_23:
                teach_text.text = LocalizationManager.Localize("teach_24");
                Step.interactable = true;
                offBorder(teach_panel);
                teach_panel.SetActive(false);
                break;
            case Const.TeachState.ClickCard_24:
                teach_text.text = LocalizationManager.Localize("teach_25");
                Step.interactable = false;
                offBorder(Step.gameObject);
                teach_panel.SetActive(true);
                break;
            case Const.TeachState.ClickCard_25:
                teach_text.text = LocalizationManager.Localize("teach_26");
                Step.interactable = true;
                offBorder(teach_panel);
                teach_panel.SetActive(false);
                break;
            case Const.TeachState.ClickCard_26:
                teach_text.text = LocalizationManager.Localize("teach_27");
                Step.interactable = false;
                offBorder(Step.gameObject);
                teach_panel.SetActive(true);
                break;
            case Const.TeachState.ClickCard_27:
                teach_text.text = LocalizationManager.Localize("teach_28");
                break;
            case Const.TeachState.ClickCard_28:
                teach_text.text = LocalizationManager.Localize("teach_29");
                offBorder(teach_panel);
                break;
            case Const.TeachState.ClickCard_29:
                teach_text.text = LocalizationManager.Localize("teach_30");
                Play.interactable = true;
                break;
            default:
                break;
        }
    }

    private void showBorder()
    {
        if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
        {
            Const.CntUpdateBorder = 0;
        }
        else
        {
            return;
        }

        switch (Const.StateTeach)
        {
            case Const.TeachState.ClickCard_21:
                toggleBorder(teach_panel);
                break;
            case Const.TeachState.ClickCard_22:
                toggleBorder(teach_panel);
                break;
            case Const.TeachState.ClickCard_23:
                toggleBorder(Step.gameObject);
                break;
            case Const.TeachState.ClickCard_24:
                toggleBorder(teach_panel);
                break;
            case Const.TeachState.ClickCard_25:
                toggleBorder(Step.gameObject);
                break;
            case Const.TeachState.ClickCard_26:
                toggleBorder(teach_panel);
                break;
            case Const.TeachState.ClickCard_27:
                toggleBorder(teach_panel);
                break;
            case Const.TeachState.ClickCard_28:
                toggleBorder(teach_panel);
                break;
            case Const.TeachState.ClickCard_29:
                toggleBorder(Play.gameObject);
                break;
            case Const.TeachState.ClickCard_30:
                
                break;
            default:
                break;
        }
    }


    private void toggleBorder(GameObject _object)
    {
        Outline[] outLine;
        outLine = _object.GetComponentsInChildren<Outline>();
        foreach (Outline o in outLine)
        {
            if (o.isActiveAndEnabled)
            {
                o.enabled = false;
                Const.BorderState = false;
            }
            else
            {
                o.enabled = true;
                Const.BorderState = true;
            }
        }
    }

    private void offBorder(GameObject _object)
    {
        Outline[] outLine;
        outLine = _object.GetComponentsInChildren<Outline>();
        foreach (Outline o in outLine)
        {
            o.enabled = false;
            Const.BorderState = false;
        }
    }

}
