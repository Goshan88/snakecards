﻿//#define ON_COMPUTER
//#define ON_WIFI
#define ON_SERVER
#define SHOW_RED

using System;
using UnityEngine;
using System.Collections.Generic;
using Sfs2X.Entities.Data;


public static class Const
{
#if SHOW_RED
    public const bool showOpponentCard = false;
#else
    public const bool showOpponentCard = true;
#endif

    public static int Xmin = 7;
    public static int Xmax = 17;
    public static int Ymin = 0;
    public static int Ymax = 15;

    public static string PAUSE_GAME = "Пауза";
    public static string GAME_OVER = "Конец игры";

    public static string WIN_CONDITION_STR = "WIN_CONDITION";
    public static string LANGUAGE = "LANGUAGE";
    public static string ALL_COINS = "ALL_COINS";
    public static string CUR_LEV = "CURRENT_LEVEL";
    public static string MAX_LEV = "MAX_LEVEL"; 

    public static int Speed = 1;
    public static int maxSpeed = 5;
    public static int minSpeed = 0;
    public const string str_Speed = "Speed";//

    public static float TimeBegin = 0.4f;
    public static int FIELD_SIZE = 13;
    public static int PACK_SIZE = 25;
    public static int EDIT_SIZE = 10;
    public static float PACK_OFF_Y = 2.5f;
    public static float PACK_OFF_X = 1.5f;
    public static float EDIT_OFF_X = 2.5f;

    public static int FIELD_BORDER = 0;
    public static int FIELD_HAVE_MOVE = 2;
    public static float Z_ZERO = 0;
    public static float Z_COORD = -1;
    public static float Z_SMB_COORD = -2;
    public static float Z_CLOSE = -3;
    public static float Z_BORDER = -4;
    public static float Z_HIDE_CLOSE = 3;
    public  enum FIELD_STATE
    {
		VOID,        //it is empty empty
        HEAD,
        BODY,
        TAIL,
        ENEMY_HEAD,
        ENEMY_BODY,
        ENEMY_TAIL,
        EMPTY_SYMBOL,
        WALL,
        DELETE_SMB,
        EMPTY,
        ENEMY_CURVE,
        CURVE,
        NOTHING,		//behind wall
        BUTTON_EMP,
        BUTTON_SLC,
        USED_CARD,
        ANUSED_CARD,
        EXPLOSION,
        CLOSE_CARD_NOTINTERACT,
    }
    
    public enum SNAKE_COLOR
    {
        RED,
        BLUE
    }

    //game scene
    public static SNAKE_MOVE snake_move= SNAKE_MOVE.NOTHING;
    public static int screen_width = 0;
    public static int screen_height = 0;
    public static float scale = 0f;
    public enum BATTLE_TYPE
    {
           NOTHING,
           DEBUG,//without opponent
           BATTLE,
           VS_3
    }
    public static BATTLE_TYPE battle_type;
    public static bool initGame = false;
    public static bool initCardsControl = true;
    public static bool playGame = false;
    public static bool finishGame = false;
    public static bool showFinaleScene = false;
    public static bool beginGame = true;
    public static int cnt_move = 0;//count move snake
    public static int max_move = 200;//max count move snake
    public static int min_move = 1;
    public static bool make_move = true;
    public static bool init_snake = false;
    public static bool game_exite = false;
    public static bool game_step_mode = false;
    public static bool game_next_step = false;
    public static bool click_but = false;
    public static int MAX_PROGRESS = 100;
    public static int MIN_PROGRESS = 0;
    public static int MIN_MATCHES_FOR_RND = 1;
    public static int FREE_MOVE_COORD = 3;

    public static int Z_SCALE_IMAGE_SCREEN = 1;
    public static int Z_COORD_UI = -1;
    public static int Z_COORD_GAME_STATUS = -3;
    public static int Z_COORD_BUTTON = -4;
    public static int Z_COORD_IMAGE_SCREEN = -2;

    public static float DARK_BEGIN = 0.1f;
    public static float DARK_END = 0.1f;
    public static float DARK_WELCOME = 0.9f;
    public static float DARK_DELTA = 0.1f;
    public static float cnt_dark = DARK_BEGIN;
    public static Color BACKGROUND_COLOR = Color.black;
    public static float TIME_STATUS_TEXT = 1f;
    public static float TIME_NAME_VIEW = 0.5f;
    public static float TIME_EMTY_SCORE_VIEW = 0.5f;
    public static float TIME_DELTA_SCORE_VIEW = 0.1f;
    public static float TIME_RESULT_VIEW = 0.5f;
    public static int user_result = 0;
    public static int opponent_result = 0;
    public static string opponent_id = "";
    //public static int user_rating = 256;
    //public static int opponent_rating = 986;
    public static bool need_send_res_bat = false;
    public static bool need_update_snake = true;
    public static bool need_load_raiting = true;
    public static bool need_new_opponent = true;
    public static int MAX_CNT_NEED = 4;

    public static int cnt_auto_exite = 0;
    public static int MAX_AUTO_EXITE = 10;

    public static GameOverScene cnt_scene = GameOverScene.NOTHING;
    public enum GameOverScene
    {
        NOTHING,
        DARK_SCENE,
        VIEW_STATUS,
        VIEW_NAME,
        VIEW_SCORE,
        VIEW_SCORE_RES,
        VIEW_RESULT,
        VIEW_OK_BUTTON,
        WAIT_PRESS_OK
    }

    public static WelcomeScene cnt_welcome = WelcomeScene.NOTHING;
    public enum WelcomeScene
    {
        NOTHING,
        WELCOME_SCENE,
        WAIT_PRESS_BUT
    }

    //main scene
    public static readonly List<Field> Game_Fields = new List<Field>();
    public static readonly List<Field> User_usedCard = new List<Field>();
    public static readonly List<Field> Opponent_usedCard = new List<Field>();
    public static int OFF_FIELD_INPACK = 1;
    public static bool initShowCards = false;

    public static bool long_click = false;
    public static float TIME_LONG_CLICK = 1f;// for long click need push above 1s

    public static float game_update = 0.1f;
    public static int User_Snake = 0;
    public static int Head_X = 2;
	public static int Head_Y = 3;

    public static int Opponent_Snake = 1;
    public static int Op_Head_X = 11;
    public static int Op_Head_Y = 3;
    //just for test rate racing
    //public static int Op_Head_X = 3;
    //public static int Op_Head_Y = (Head_Y +8);


    //public static byte cntSnake = 1;
    public static byte LoseSizeSnake = 2;//when snake fail game
    public static byte SnakeSize = 9;
    public static byte CardSize = 7;
	public static byte PackSize = 9;
    public static byte MinUsedCard = 0;
    public static byte RotateVariant = 4;
    public static byte EditCardSize = 1;
    public static byte RowCards = 3;
	public struct Card{
		public static FIELD_STATE[] Fields = new FIELD_STATE[(CardSize*CardSize)];
	}
	public static int DEF_RET_COORD =50;//in cards return if can't found head
    public static int BEHIND_WALL = -1;

    public static int cnt_snake = 0;//коллич. змей в памяти
    public static string user_snake_name = "";
    public static int snake_used_card = 1;
    public static string opponent_snake_name = "";
    public static int MAX_ADD_SNAKE = 2;
    public static int MIN_CHANGE_SNAKE = 1;

    public enum CARD_TYPE{
		NOTHING,
		ACTIVE,//when snake choice for move that cards
		VIEWED,//card don't used in move, but view current card
        CLOSED//field for close unused card
	}

    public enum VIEW_TYPE
    {
        STATE,
        NEW_STATE,
        COORD
    }

    public static int CARD_OFFSET_Y = 2;//shift above main field
    public static int CARD_OFFSET_X = 1;

    //used card view
    public static int USER_USED_CARD_SHIFT_X = -5;
    public static int USER_USED_CARD_SHIFT_Y = -2;
    public static int OPPONENT_USED_CARD_SHIFT_Y = 6;

    //preferences helper
    public static string PRF_CNT_SNAKE="count_snake";
	public static int DEF_CNT_SNAKE =0;
    public static string PRF_ACTIVE_SNAKE = "active_snake";
    public static int DEF_ACTIVE_SNAKE = 0;
    public static string PRF_NAME_SNAKE="name_snake";
	public static string DEF_NAME_SNAKE ="snake";
	public static string PRF_STATE_FIELD="id_field";
	public static byte DEF_STATE_FIELD =0;
	public static string PRF_ID_SNAKE_DB="id_snake_database";
	public static string DEF_ID_SNAKE_DB ="0";
    public static int MIN_SIZE_ROOM_NAME = 7;
    public static float MaxIdRnd = 100000;
    public static string PRF_ID_USER_DB="id_user_database";
	public static string DEF_ID_USER_DB ="0";
	public static string PRF_NAME_USER="name_user";
    public static string DEF_NAME_USER = "User";
    public static string PRF_USED_CARD = "used_card";
    public static int DEF_USED_CARD = 1;
    public static string PRF_RATING = "raiting";
    public static int DEF_RATING = 1;
    public static string PRF_ID_FIELD = "id_field";
    public static int DEF_ID_FIELD = 0;
    public static int snake_active = 0;//current selected snake [0...]
    public static int card_active = 0;//current selected card [0..8]
    public static string PRF_LANG = "Language";
    public static string DEF_LANG = "";
    public static string PRF_MAIL = "Mail";
    public static string DEF_MAIL = "";
    public static string PRF_DT_USER = "dt_user";
    public static string PRF_DT_SNAKE = "dt_snake";
    public static int PRF_DT_DEF = 0;
    public static int USER_ID = 0;
    public static int OPPONENT_ID = 1;
    public static string PRF_TRENING = "trening";
    public static int PRF_TRENING_DEF = 0;
    public static int PRF_TRENING_OK = 1;

    //edit card scene
    public static bool initEditCards = false;
    public static int CNT_TYPE_SYMB = 12;
    public static int OFF_CARD_EDIT = 1;
    public static int OFF_TYPE_SYMB = 3;
    public static int EDIT_OFF_SYMB_X = 2;
    public static float EDIT_BTW_SYMB_Y = 1.9f;
    public static float EDIT_OFF_SYMB_Y = 0.4f;
    public static float edit_but_scale = 1.7f;
    public static bool EDIT_SAVE_CARD = false;
    public static bool EDIT_CANCEL_CARD = false;
    public static bool clear_edit_card = false;
    public static Const.FIELD_STATE change_state = FIELD_STATE.NOTHING;
    public static bool delete_head = false;
    public static float TIME_SHOW_MESSAGE = 2f;
    public static bool show_wrn_msg = false;
    public static Color color_font;

    public static class touch_pos { 
		public static float x=0f;
        public static  float y =0f;
        public static  bool processed =true;//if true means not have new data
    }


    //sfs server
#if ON_COMPUTER
                public static string defaultHost = "127.0.0.1";   // Default host
                public static int defaultTcpPort = 9933;          // Default TCP port
                public static int defaultWsPort = 8053;         // Default WebSocket port
#endif

#if ON_WIFI
            public static string defaultHost = "192.168.0.14";   
            public static int defaultTcpPort = 9933;          
            public static int defaultWsPort = 8053;			
#endif

#if ON_SERVER
    public static string defaultHost = "77.94.105.252";   
            public static int defaultTcpPort = 9933;          
            public static int defaultWsPort = 8080;			
#endif

    public static string ZoneName = "SnakeBattle";   //"BasicExamples"
    public static string RoomID = "__lib__";
    public static string RoomPaket = "kalinka.game.SB_Extension";

    public static bool send_request = false;
    public static bool prepare_request = false;

    public static REQUEST_TYPE request_state = REQUEST_TYPE.NOTHING;
    public enum REQUEST_TYPE
    {
        NOTHING,
        CHECK_UPDATE,
        SET_USER,
        SET_SNAKE,
        SET_PACK,
        GET_OPPONENT,
        SET_RESULT_BATTLE,
        GET_RAITING,
        SET_DISABLE
    }
    public static string REQUEST_SNAKE_NAME = "SnakeName";
    public static string REQUEST_SNAKE_ID = "SnakeId";
    public static string REQUEST_ID_OPPONENT = "opponent_id";
    public static string REQUEST_SNAKE_RATING_IN_BAT = "UserRatingBattle";
    public static string REQUEST_OPPONENT_RATING_IN_BAT = "OpponentRatingBattle";
    public static string REQUEST_SNAKE_USEDCARD = "SnakeUsedCard";
    public static string REQUEST_USER_ID = "UserId";
    public static string REQUEST_USER_LANG = "UserLang";
    public static string REQUEST_USER_MAIL = "UserMail";
    public static string REQUEST_SNAKE_CHANGE = "SnakeChange";
    public static string REQUEST_SNAKE_RAITING = "SnakeRaiting";
    public static string REQUEST_SNAKE_CARDSPACK = "CardsPack";
    public static string REQUEST_USER_CHANGE = "UserChange";

    public static string SRV_CHECK_UPDATE = "checkUpdate";
    public static string SRV_PARAM_USER = "paramUser";
    public static string SRV_PARAM_SNAKE = "paramSnake";
    public static string SRV_GET_OPPONENT = "getOpponent";
    public const string REQUEST_RAITING_SNAKES = "raiting_snakes";
    public static string PARAM_RAITING_SNAKES = "raiting_snakes";
    public static string SRV_RESULT_BATTLE = "setResultBattle";
    public static string SRV_RAITING = "getRaiting";
    public static string cmd_request = SRV_CHECK_UPDATE;

    //stack for command from server
    public static readonly LinkedList<String> ServerCMD = new LinkedList<String>();
    //command from server
    public const string SRV_CMD_GETSNAKE = "getSnake";
    public const string SRV_CMD_GETUSER = "getUser";
    public const string SRV_CMD_OPPONENT = "getOpponent";
    public const string SRV_CMD_RESULT = "getResult";
    public const string SRV_CMD_DISABLE = "cmd_disable";
    public const string SRV_CMD_RAITING = "getRaiting";
    public const string SRV_CMD_UPDATE = "updateSnake";

    //params answer form server
    public const string PARAM_CMD = "cmd";
    public const string PARAM_PARAMS = "params";
    public const string PARAM_ID_SNAKE = "snake_id";
    public const string PARAM_NAME_SNAKE = "name";
    public const string PARAM_RATING_SNAKE = "rating";
    public const string PARAM_ID_USER = "user_id";
    public const string PARAM_USED_CARDS = "used_cards";
    public static string PARAM_ID_OPPONENT = "opponent_id";
    public static string PARAM_UC_OPPONENT = "opponent_used_card";
    public static string PARAM_NAME_OPPONENT = "opponent_name";
    public static string PARAM_RATING_OPPONENT = "opponent_rating";
    public static string PARAM_PACK_OPPONENT = "opponent_pack";

    //preferency save result battle
    public const string BATTLE_ID_USER = "battle_user_id";
    public const string BATTLE_ID_OPPONENT = "battle_opponent_id";
    public const string BATTLE_RES_USER = "battle_user_res";
    public const string BATTLE_RES_OPPONENT = "battle_opponent_res";
    public const string BATTLE_NEED_OPPONENT = "battle_opponent_need";
    public const string BATTLE_RES_NEED_SEND = "result_battle_is_needed_send";

    public const int MIN_LEN_ID_STR = 3;
   // public const int OPPONENT_PREF_ID = 1;
   // public const int USER_PREF_ID = 0;

    public static System.DateTime CurrentTime = System.DateTime.Now; // Текущее время
    public static int SIZE_ARR_DATE = 6;

    public static CONNECT_STATE sfs_state = CONNECT_STATE.NOTHING;
    public enum CONNECT_STATE
    {
        NOTHING,
        RESET,
        CONNECT_SFS,
        LOGIN,
        CREATE_ROOM,
        REQUEST_EXCHANGE,
        ERROOR_TRY_CON,
        ERROOR_CON_FAILED,//error in parameters connection (example: ip,port)
                          //error in zone without name
        ERROOR_CON_LOST,
        ERROOR_LOGIN,
        DISCONNECT,//command for disconnect
        STANDBY,//waiting command from user
        CANT_CON_SFS//client can't connect to the server
    }
    public static int cnt_time_delay_error = 0;
    public static int MAX_TIME_DELAY = 17;
    public static int cnt_sfs_con_err = 0;
    public static int MAX_ATTEMPT_TO_SFS = 3;
    public static int cnt_attempt_con = 0;
    public static int MAX_ATTEMPT_CON = 5;
    public static float server_update = 0.2f;//another way i gett error in connect cnt
    public static float request_update = 0.1f;

    public static SFSObject object_request = new SFSObject();

    //raitning user
    public const int MAX_RAITING_USER_CNT = 10;
    public const int MAX_VIEW_RAITING_CNT = 6;
    public static string[] SnakeName= new string[MAX_RAITING_USER_CNT];
    public static int[] SnakeRaiting = new int[MAX_RAITING_USER_CNT];
    public static bool need_raiting = true;

    //teach_panel load
    public static bool View_panel = false;
    public static bool Close_panel = false;
    public static bool Enter_panel = false;
    public enum Type_panel
    {
        NOTHING,
        NAME,
        EMAIL,
        SETTINGS,
        LANG,
        SCORE
    }
    public static Type_panel panel = Type_panel.NOTHING;

    //teach var
    public enum TeachState
    {
        NOTHING,
        BEGIN,
        ClickCard_1,
        ClickCard_2,
        ClickCard_3,
        ClickCard_4,
        ClickCard_5,
        ClickCard_6,
        ClickCard_7,
        ClickCard_8,
        ClickCard_9,
        ClickCard_10,
        ClickCard_11,
        ClickCard_12,
        ClickCard_13,
        ClickCard_14,
        ClickCard_15,
        ClickCard_16,
        ClickCard_17,
        ClickCard_18,
        ClickCard_19,
        ClickCard_20,
        ClickCard_21,
        ClickCard_22,
        ClickCard_23,
        ClickCard_24,
        ClickCard_25,
        ClickCard_26,
        ClickCard_27,
        ClickCard_28,
        ClickCard_29,
        ClickCard_30
    }
    public static TeachState StateTeach = TeachState.BEGIN;     //
    public static TeachState StateTeachOut = TeachState.NOTHING;  //
    public static int needClickCard = 0;
    public static int needClickCardOut = 0;

    public static int defArrow = 6;//value change coord y every sample
    public static int MaxDefArrow = 64;//max value change coord
    public static int MinDefArrow = 0;//max value change coord
    public static Vector3 firstCard;//coordinate first card
    public static Vector3 firstCardTop;
    public static Vector3 secondCard;
    public static Vector3 secondCardTop;
    public static Vector3 CoordTailSymbol;
    public static Vector3 CoordEmptySymbol;
    public static Vector3 CoordField_1;
    public static Vector3 CoordField_2;
    public static bool showArrow = false;
    public const int numbEnemyTail = 5;
    public const int numbEmpty = 6;
    public const int size_field_ind = 4;
    public static int[] teach_feild_ind = new int[4]{18,10,11,12};
    public static int CountArrowMove = 0;
    public static bool dirUp = true;
    public static bool rotateArrow = false;

    public static int UpdateBorderMax = 4;
    public static int CntUpdateBorder = 0;
    public static bool BorderState = false;
    public static float toggle_size_multi = 1.1f;
    public static float toggle_offset = 1.07f;
    public static int teach_first_step = 2;
    public static int teach_second_step = 6;
    public static int teach_third_step = 12;
}