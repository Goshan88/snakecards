﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Assets.SimpleLocalization;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class MainTextControl : MonoBehaviour
{
    public Text snake_name, user_score, teach_state, connect_state, title, lang, tChangeMail, teach_text;
    public GameObject panel, teach_panel, NoConnect;
    public InputField textInput;
    public Button Score, Setting, butDebug, Enter, Lang_1, Lang_2, changeEmail, battle_but, close_panel;
    public Text Score_1, Score_2, Score_3, Score_4, Score_5, Score_6;
    //GameController controller;
    // Use this for initialization
    private static Vector3 coordArrow;
    private float _nextMainTextUpdate = 0f;

    void Start()
    {
        if (user_score != null)
        {
            user_score.text = PreferencyHelper.getRaiting(Const.USER_ID).ToString();
            snake_name.text = PreferencyHelper.getNameSnake(Const.USER_ID);
            if (textInput != null)
            {
                textInput.text = snake_name.text;
            }
        }

//#if Realise
        if(connect_state!=null) connect_state.gameObject.SetActive(false);
        if(teach_state!=null) teach_state.gameObject.SetActive(false);
        NoConnect.SetActive(false);
//#endif
        panel.SetActive(false);
        hideLangButton();
        hideScoreDesc();

        if (!PreferencyHelper.getUserTrening())
        {
            Const.StateTeachOut = Const.TeachState.NOTHING;
            notInterracteble();
            teach_panel.SetActive(true);
        }
        else
        {
            teach_panel.SetActive(false);
        }
        offBorder(battle_but.gameObject);
        offBorder(teach_panel);

    }

    
    private void changeColor(Text _text)
    {
        if (_text.color == Color.yellow)
        {
            _text.color = Color.red;
        }
        else
        {
            _text.color = Color.yellow;
        }
    }

    private void setRedColor(Text _text)
    {
        _text.color = Color.red;
    }

        private void toggleBorder(GameObject _object)
    {
        Outline[] outLine;
        outLine = _object.GetComponentsInChildren<Outline>();
        foreach (Outline o in outLine)
        {
            if (o.isActiveAndEnabled)
            {
                o.enabled = false;
                Const.BorderState = false;
            }
            else
            {
                o.enabled = true;
                Const.BorderState = true;
            }
        }
    }


    private void offBorder(GameObject _object)
    {
        Outline[] outLine;
        outLine = _object.GetComponentsInChildren<Outline>();
        foreach (Outline o in outLine)
        {
            o.enabled = false;
            Const.BorderState = false;
        }
    }

    private void setBorder()
    {

        switch (Const.StateTeachOut)
        {
            case Const.TeachState.BEGIN:
            case Const.TeachState.ClickCard_2:
            case Const.TeachState.ClickCard_3:
            case Const.TeachState.ClickCard_4:
            case Const.TeachState.ClickCard_5:
            if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                {
                    Const.CntUpdateBorder = 0;
                    toggleBorder(teach_panel);
                }
                break;
            case Const.TeachState.ClickCard_13:
            case Const.TeachState.ClickCard_12:
            case Const.TeachState.ClickCard_1:
            case Const.TeachState.ClickCard_6:
                if (Const.BorderState) offBorder(teach_panel);
                break;
            case Const.TeachState.ClickCard_19:
                if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                {
                    Const.CntUpdateBorder = 0;
                    changeColor(snake_name);
                }
                break;
            case Const.TeachState.ClickCard_20:
                if (Const.CntUpdateBorder++ > Const.UpdateBorderMax)
                {
                    Const.CntUpdateBorder = 0;
                    toggleBorder(battle_but.gameObject);
                }
                break;
            default:
                break;
        }
    }


    void hideLangButton()
    {
        Lang_1.gameObject.SetActive(false);
        Lang_2.gameObject.SetActive(false);
    }

    void showLangButton()
    {
        Lang_1.gameObject.SetActive(true);
        Lang_2.gameObject.SetActive(true);
    }

    void hideScoreDesc()
    {
        Score_1.gameObject.SetActive(false);
        Score_2.gameObject.SetActive(false);
        Score_3.gameObject.SetActive(false);
        Score_4.gameObject.SetActive(false);
        Score_5.gameObject.SetActive(false);
        Score_6.gameObject.SetActive(false);
    }

    void showScoreDesc()
    {
        Score_1.gameObject.SetActive(true);
        Score_2.gameObject.SetActive(true);
        Score_3.gameObject.SetActive(true);
        Score_4.gameObject.SetActive(true);
        Score_5.gameObject.SetActive(true);
        Score_6.gameObject.SetActive(true);

        for (int i = 0; i < Const.MAX_VIEW_RAITING_CNT; i++)
        {
            if (Const.SnakeRaiting[i] == 0) break;
            switch (i)
            {
                case 0:
                    Score_1.text = "1." + Const.SnakeName[i] + "  " + Const.SnakeRaiting[i];
                    break;
                case 1:
                    Score_2.text = "2." + Const.SnakeName[i] + "  " + Const.SnakeRaiting[i];
                    break;
                case 2:
                    Score_3.text = "3." + Const.SnakeName[i] + "  " + Const.SnakeRaiting[i];
                    break;
                case 3:
                    Score_4.text = "4." + Const.SnakeName[i] + "  " + Const.SnakeRaiting[i];
                    break;
                case 4:
                    Score_5.text = "5." + Const.SnakeName[i] + "  " + Const.SnakeRaiting[i];
                    break;
                case 5:
                    Score_6.text = "6." + Const.SnakeName[i] + "  " + Const.SnakeRaiting[i];
                    break;
            }
        }
    }

    private void ShowTeach()
    {
        switch (Const.StateTeachOut)
        {
            case Const.TeachState.NOTHING:
                break;
            case Const.TeachState.BEGIN:
                PreferencyHelper.setUsedCard(Const.USER_ID, 0);
                if (!teach_state.gameObject.activeSelf) teach_state.gameObject.SetActive(true);
                teach_text.text = LocalizationManager.Localize("teach_1");
                break;

            case Const.TeachState.ClickCard_1:
                teach_text.text = LocalizationManager.Localize("teach_2");
                break;
            case Const.TeachState.ClickCard_2:
                teach_text.text = LocalizationManager.Localize("teach_3");
                break;
            case Const.TeachState.ClickCard_3:
                teach_text.text = LocalizationManager.Localize("teach_4");
                break;
            case Const.TeachState.ClickCard_4:
                teach_text.text = LocalizationManager.Localize("teach_5");
                break;
            case Const.TeachState.ClickCard_5:
                teach_text.text = LocalizationManager.Localize("teach_6");
                break;
            case Const.TeachState.ClickCard_6:
                teach_text.text = LocalizationManager.Localize("teach_7");
                break;
            case Const.TeachState.ClickCard_12:
                teach_text.text = LocalizationManager.Localize("teach_13");
                break;
            case Const.TeachState.ClickCard_13:
                teach_text.text = LocalizationManager.Localize("teach_14");
                break;
            case Const.TeachState.ClickCard_19:
                teach_text.text = LocalizationManager.Localize("teach_20");
                snake_name.GetComponent<Button>().interactable = true;
                Enter.interactable = true;
                title.color = Color.black;
                break;
            case Const.TeachState.ClickCard_20:
                teach_text.text = LocalizationManager.Localize("teach_21");
                snake_name.GetComponent<Button>().interactable = false;
                setRedColor(snake_name);
                battle_but.interactable = true;
                break;
            default:
                break;
        }
    }

    private void checkConStatus()
    {
        if (PreferencyHelper.getUserTrening())
        {
            if (Const.sfs_state.Equals(Const.CONNECT_STATE.STANDBY))
            {
                battle_but.interactable = true;
                NoConnect.SetActive(false);
            }
            else
            {
                battle_but.interactable = false;
                if (Const.sfs_state.Equals(Const.CONNECT_STATE.CANT_CON_SFS))
                {
                    //if not have connect
                    NoConnect.SetActive(true);
                }
                else
                {
                    NoConnect.SetActive(false);
                }
            }
        }
        else
        {
            if (Const.StateTeach == Const.TeachState.ClickCard_20)
            {
                battle_but.GetComponent<Text>().color = Color.black;
                battle_but.interactable = true;
            }
            else
            {
                battle_but.interactable = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (_nextMainTextUpdate > Time.time)
        {
            return;
        }

        _nextMainTextUpdate = Time.time + Const.game_update;


        //trening check
        if (!PreferencyHelper.getUserTrening())
        {

            if (!teach_state.text.Equals(Const.StateTeach.ToString()))
            {
                teach_state.text = Const.StateTeach.ToString();
            }

            if (Const.StateTeach != Const.StateTeachOut)
            {
                Const.StateTeachOut = Const.StateTeach;
                ShowTeach();
            }
            setBorder();
        }
        else
        {
            if (teach_state.gameObject.activeSelf) teach_state.gameObject.SetActive(false);
        }


        if (Const.Enter_panel)
        {
            Const.Enter_panel = false;
            switch (Const.panel)
            {
                case Const.Type_panel.NAME:
                    PreferencyHelper.setNameSnake(Const.USER_ID, textInput.text);
                    snake_name.text = textInput.text;
                    Const.Close_panel = true;
                    Const.sfs_state = Const.CONNECT_STATE.CONNECT_SFS;
                    Const.ServerCMD.AddLast(Const.SRV_CMD_GETSNAKE);
                    PreferencyHelper.saveChangeDT(Const.PRF_DT_SNAKE);
                    Const.StateTeach = Const.TeachState.ClickCard_20;
                    break;
                case Const.Type_panel.EMAIL:
                    PreferencyHelper.saveMail(textInput.text);
                    Const.sfs_state = Const.CONNECT_STATE.CONNECT_SFS;
                    Const.ServerCMD.AddLast(Const.SRV_CMD_GETUSER);
                    Const.Close_panel = true;
                    PreferencyHelper.saveChangeDT(Const.PRF_DT_USER);
                    break;
                case Const.Type_panel.SETTINGS:
                    Const.panel = Const.Type_panel.LANG;
                    Const.View_panel = true;
                    PreferencyHelper.saveChangeDT(Const.PRF_DT_USER);
                    Const.sfs_state = Const.CONNECT_STATE.CONNECT_SFS;
                    Const.ServerCMD.AddLast(Const.SRV_CMD_GETUSER);
                    break;
                default:
                    break;
            }
            Const.sfs_state = Const.CONNECT_STATE.NOTHING;
        }


        if (Const.View_panel)
        {
            Const.View_panel = false;
            panel.SetActive(true);
            switch (Const.panel)
            {
                case Const.Type_panel.NAME:
                    Enter.gameObject.SetActive(true);
                    Enter.GetComponentInChildren<Text>().text = LocalizationManager.Localize("Save");
                    textInput.contentType = InputField.ContentType.Name;
                    textInput.text = PreferencyHelper.getNameSnake(Const.USER_ID);
                    textInput.gameObject.SetActive(true);
                    textInput.ActivateInputField();
                    textInput.Select();
                    //textInput.OnPointerClick(null);
                    title.gameObject.SetActive(true);
                    lang.gameObject.SetActive(false);
                    title.text = LocalizationManager.Localize("EnterName");
                    hideScoreDesc();
                    hideLangButton();
                    changeEmail.gameObject.SetActive(false);
                    tChangeMail.gameObject.SetActive(false);
                    break;
                case Const.Type_panel.EMAIL:
                    Enter.gameObject.SetActive(true);
                    textInput.gameObject.SetActive(true);
                    textInput.contentType = InputField.ContentType.EmailAddress;
                    textInput.text = PreferencyHelper.getMail();
                    textInput.Select();
                    textInput.ActivateInputField();
                    //textInput.OnPointerClick(null);
                    title.gameObject.SetActive(true);
                    lang.gameObject.SetActive(false);
                    title.text = LocalizationManager.Localize("EMAIL");
                    Enter.GetComponentInChildren<Text>().text = LocalizationManager.Localize("Save");
                    hideScoreDesc();
                    hideLangButton();
                    changeEmail.gameObject.SetActive(false);
                    tChangeMail.gameObject.SetActive(false);
                    break;
                case Const.Type_panel.SETTINGS:
                    Enter.gameObject.SetActive(true);
                    Enter.GetComponentInChildren<Text>().text = PreferencyHelper.getLang();
                    textInput.gameObject.SetActive(false);
                    title.gameObject.SetActive(true);
                    lang.gameObject.SetActive(true);
                    title.text = LocalizationManager.Localize("Settings");
                    hideScoreDesc();
                    hideLangButton();
                    //changeEmail.GetComponentInChildren<Text>().text = PreferencyHelper.getMail();
                    tChangeMail.GetComponentInChildren<Text>().text = PreferencyHelper.getMail();
                    changeEmail.gameObject.SetActive(true);
                    tChangeMail.gameObject.SetActive(true);
                    break;
                case Const.Type_panel.SCORE:
                    Enter.gameObject.SetActive(false);
                    textInput.gameObject.SetActive(false);
                    title.gameObject.SetActive(true);
                    lang.gameObject.SetActive(false);
                    title.text = LocalizationManager.Localize("Rating");
                    showScoreDesc();
                    hideLangButton();
                    changeEmail.gameObject.SetActive(false);
                    tChangeMail.gameObject.SetActive(false);
                    break;
                case Const.Type_panel.LANG:
                    title.gameObject.SetActive(true);
                    title.text = LocalizationManager.Localize("ChoiseLang");
                    lang.gameObject.SetActive(false);
                    Enter.gameObject.SetActive(false);
                    showLangButton();
                    hideScoreDesc();
                    changeEmail.gameObject.SetActive(false);
                    tChangeMail.gameObject.SetActive(false);
                    break;
                default:
                    Const.Close_panel = true;
                    break;
            }
        }

        if (Const.Close_panel)
        {
            Const.Close_panel = false;
            Const.panel = Const.Type_panel.NOTHING;
            panel.SetActive(false);
        }

        checkConStatus();
    }

    private void notInterracteble()
    {
        title.color = Color.grey;
        lang.color = Color.grey;
        tChangeMail.color = Color.grey;

        close_panel.interactable = false;
        Enter.interactable = false;
        butDebug.interactable = false;
        Setting.interactable = false;
        user_score.GetComponent<Button>().interactable = false;
        snake_name.GetComponent<Button>().interactable = false;
        Score.interactable = false;
        Score.image.sprite = Resources.Load("score_medal_grey", typeof(Sprite)) as Sprite;
    }
}
