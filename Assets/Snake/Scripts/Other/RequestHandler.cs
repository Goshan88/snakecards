﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RequestHandler : MonoBehaviour {
    float _nextRequestUpdate = Const.TimeBegin;
    static int cnt_disc = 0;
    const int MAX_CNT_DISCON = 3;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        
        if (_nextRequestUpdate > Time.time)
        {
            return;
        }

        _nextRequestUpdate = Time.time + Const.request_update;

        //just for debug
        if (Const.send_request)
        {
            Const.sfs_state = Const.CONNECT_STATE.RESET;
            //Const.request_state = Const.REQUEST_TYPE.CHECK_UPDATE;
            Const.ServerCMD.AddLast(Const.SRV_CMD_UPDATE);
            Const.send_request = false;
        }

        //if get command from server
        if (Const.sfs_state == Const.CONNECT_STATE.REQUEST_EXCHANGE)
        {
            //check need command send to the server
            if ((Const.ServerCMD.Count==0)&&(Const.request_state== Const.REQUEST_TYPE.NOTHING)){
            if (Const.need_send_res_bat)
            {
            //Const.request_state = Const.REQUEST_TYPE.SET_RESULT_BATTLE;
            Const.ServerCMD.AddLast(Const.SRV_CMD_RESULT);
            }
            else
            {
                if (Const.need_update_snake)
                {
                //Const.request_state = Const.REQUEST_TYPE.CHECK_UPDATE;
                Const.ServerCMD.AddLast(Const.SRV_CMD_UPDATE);
            }
                else
                {
                    if (Const.need_raiting)
                    {
                    //Const.request_state = Const.REQUEST_TYPE.GET_RAITING;
                    Const.ServerCMD.AddLast(Const.SRV_CMD_RAITING);
                    }
                    else
                    {
                        if (Const.need_new_opponent)
                        {
                            //Const.request_state = Const.REQUEST_TYPE.GET_OPPONENT;
                            Const.ServerCMD.AddLast(Const.SRV_CMD_OPPONENT);
                        }
                        else
                            {
                                //all data send
                                //Const.request_state = Const.REQUEST_TYPE.SET_DISABLE;
                                /*if (cnt_disc++ > MAX_CNT_DISCON)
                                {
                                    cnt_disc = 0;*/
                                    Const.ServerCMD.AddLast(Const.SRV_CMD_DISABLE);
                                //}
                            }
                        }
                     }
                }
            }

        
            if (Const.request_state == Const.REQUEST_TYPE.NOTHING)
            {
                if (Const.ServerCMD.Count > 0)
                {
                    Debug.Log("Request from server: " + Const.ServerCMD.First.Value);
                    switch (Const.ServerCMD.First.Value)
                    {
                        case Const.SRV_CMD_GETSNAKE:
                            Const.request_state = Const.REQUEST_TYPE.SET_SNAKE;
                            Debug.Log("SRV_CMD_GETSNAKE");
                            break;
                        case Const.SRV_CMD_GETUSER:
                            Const.request_state = Const.REQUEST_TYPE.SET_USER;
                            Debug.Log("SRV_CMD_SETUSER");
                            break;
                        case Const.SRV_CMD_OPPONENT:
                            Const.request_state = Const.REQUEST_TYPE.GET_OPPONENT;
                            Debug.Log("SRV_CMD_OPPONENT");
                            break;
                        case Const.SRV_CMD_RESULT:
                            Const.request_state = Const.REQUEST_TYPE.SET_RESULT_BATTLE;
                            Debug.Log("SRV_CMD_RESULT");
                            break;
                        case Const.SRV_CMD_DISABLE:
                            Const.request_state = Const.REQUEST_TYPE.SET_DISABLE;
                            Debug.Log("SRV_CMD_DISABLE");
                            break;
                        case Const.SRV_CMD_RAITING:
                            Const.request_state = Const.REQUEST_TYPE.GET_RAITING;
                            Debug.Log("SRV_CMD_RAITING");
                            break;
                        case Const.SRV_CMD_UPDATE:
                            Const.request_state = Const.REQUEST_TYPE.CHECK_UPDATE;
                            Debug.Log("SRV_CMD_UPDATE");
                            break;
                        default:
                            break;
                    }
                    //delete first element
                    Const.ServerCMD.RemoveFirst();
                }
            }
        }

        if (Const.sfs_state == Const.CONNECT_STATE.REQUEST_EXCHANGE)
        {
            switch (Const.request_state)
            {
                case Const.REQUEST_TYPE.NOTHING:
                    break;
                case Const.REQUEST_TYPE.CHECK_UPDATE:
                    Const.cmd_request = Const.SRV_CHECK_UPDATE;
                    Const.object_request = new Sfs2X.Entities.Data.SFSObject();
                    Const.object_request.PutText(Const.REQUEST_SNAKE_NAME, PreferencyHelper.getNameSnake(Const.USER_ID));
                    Const.object_request.PutText(Const.REQUEST_SNAKE_ID, PreferencyHelper.getIdSnakeDataBase(Const.USER_ID));
                    Const.object_request.PutText(Const.REQUEST_USER_ID, PreferencyHelper.getIdUserDataBase());
                    Const.object_request.PutInt(Const.REQUEST_SNAKE_RAITING, PreferencyHelper.getRaiting(Const.USER_ID));
                    //Debug.Log("Snake " + Const.USER_ID + " id:" + PreferencyHelper.getIdSnakeDataBase(Const.USER_ID));
                    int[] tmp_snake = PreferencyHelper.loadChangeDT(Const.PRF_DT_SNAKE);
                    if ((tmp_snake[0] == 0) && (tmp_snake[(tmp_snake.Length - 1)] == 0)) PreferencyHelper.saveChangeDT(Const.PRF_DT_SNAKE);
                    //Debug.Log("Time snake " + PreferencyHelper.getChangeDT(Const.PRF_DT_SNAKE));
                    Const.object_request.PutIntArray(Const.REQUEST_SNAKE_CHANGE, PreferencyHelper.loadChangeDT(Const.PRF_DT_SNAKE));
                    int[] tmp_user = PreferencyHelper.loadChangeDT(Const.PRF_DT_USER);
                    if ((tmp_user[0] == 0) && (tmp_user[(tmp_user.Length-1)] == 0)) PreferencyHelper.saveChangeDT(Const.PRF_DT_USER);
                    Const.object_request.PutIntArray(Const.REQUEST_USER_CHANGE, PreferencyHelper.loadChangeDT(Const.PRF_DT_USER));
                    //Debug.Log("Time user " + PreferencyHelper.getChangeDT(Const.PRF_DT_USER));
                    Const.need_update_snake = false;
                    break;
                case Const.REQUEST_TYPE.SET_USER:
                    Const.cmd_request = Const.SRV_PARAM_USER;
                    Const.object_request.PutText(Const.REQUEST_USER_ID, PreferencyHelper.getIdUserDataBase());
                    Const.object_request.PutText(Const.REQUEST_USER_LANG, PreferencyHelper.getLang());
                    Const.object_request.PutText(Const.REQUEST_USER_MAIL, PreferencyHelper.getMail());
                    Const.object_request.PutIntArray(Const.REQUEST_USER_CHANGE, PreferencyHelper.loadChangeDT(Const.PRF_DT_USER));
                    break;
                case Const.REQUEST_TYPE.SET_SNAKE:
                    Const.cmd_request = Const.SRV_PARAM_SNAKE;
                    Const.object_request.PutText(Const.REQUEST_SNAKE_NAME, PreferencyHelper.getNameSnake(Const.USER_ID));
                    Const.object_request.PutText(Const.REQUEST_SNAKE_ID, PreferencyHelper.getIdSnakeDataBase(Const.USER_ID));
                    Const.object_request.PutInt(Const.REQUEST_SNAKE_USEDCARD, PreferencyHelper.getUsedCard(Const.USER_ID));
                    Const.object_request.PutIntArray(Const.REQUEST_SNAKE_CHANGE, PreferencyHelper.loadChangeDT(Const.PRF_DT_SNAKE));
                    Const.object_request.PutIntArray(Const.REQUEST_SNAKE_CARDSPACK, PreferencyHelper.getPackValue(Const.USER_ID));
                    break;
                case Const.REQUEST_TYPE.SET_PACK:
                    break;
                case Const.REQUEST_TYPE.GET_OPPONENT:
                    Const.cmd_request = Const.SRV_GET_OPPONENT;
                    Const.object_request.PutText(Const.REQUEST_SNAKE_ID, PreferencyHelper.getIdSnakeDataBase(Const.USER_ID));
                    Const.need_new_opponent = false;
                    break;
                case Const.REQUEST_TYPE.SET_RESULT_BATTLE:
                    Const.cmd_request = Const.SRV_RESULT_BATTLE;
                    Const.object_request.PutText(Const.REQUEST_SNAKE_ID, PreferencyHelper.getIdSnakeDataBase(Const.USER_ID));
                    //Const.object_request.PutText(Const.REQUEST_ID_OPPONENT, PreferencyHelper.getIdSnakeDataBase(Const.OPPONENT_PREF_ID));
                    Const.object_request.PutText(Const.REQUEST_ID_OPPONENT, Const.opponent_id);
                    Const.object_request.PutInt(Const.REQUEST_SNAKE_RATING_IN_BAT, Const.user_result);
                    Const.object_request.PutInt(Const.REQUEST_OPPONENT_RATING_IN_BAT, Const.opponent_result);
                    Const.need_send_res_bat = false;
                    break;
                case Const.REQUEST_TYPE.GET_RAITING:
                    Const.cmd_request = Const.SRV_RAITING;
                    Const.object_request.PutText(Const.REQUEST_SNAKE_ID, PreferencyHelper.getIdSnakeDataBase(Const.USER_ID));
                    Const.need_raiting = false;
                    break;
                case Const.REQUEST_TYPE.SET_DISABLE:
                    Const.sfs_state = Const.CONNECT_STATE.DISCONNECT;
                    Const.request_state = Const.REQUEST_TYPE.NOTHING;
                    break;
                default:
                    break;
            }

            if (Const.request_state != Const.REQUEST_TYPE.NOTHING)
            {
                Const.prepare_request = true;
            }
            
        }
	}

}
